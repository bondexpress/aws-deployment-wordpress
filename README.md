Wordpress Staging
-----------------
```
apt-get update
apt-get upgrade -y
apt-get dist-upgrade -y
apt-get autoremove -y
sudo apt-get install nginx -y
sudo apt-get install php-fpm php-mysql -y
sudo apt-get install php7.0-curl -y

sudo vim /etc/php/7.0/fpm/php.ini

#cgi.fix_pathinfo=1 > cgi.fix_pathinfo=0

sudo systemctl restart php7.0-fpm

sudo vim /etc/nginx/sites-available/default

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    root /var/www/html;
    index index.php index.html index.htm index.nginx-debian.html;

    server_name server_domain_or_IP;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }

    location ~ /\.ht {
        deny all;
    }
}

sudo systemctl reload nginx

cd /var/www/html
rm -rf *
cd ../
mkdir staging
cd staging
git clone https://garygoodger@bitbucket.org/bondexpress/aws-deployment-wordpress.git .
chmod -R 744 .
chown -R www-data:www-data .

sudo service nginx restart
```
After Wordpress Is Pulled
-----------------------------
```
define('WP_REDIS_HOST', '');
```
