<?php
	/* Define THEME */
	if (!defined('TB_URI_PATH')) define('TB_URI_PATH', get_template_directory_uri());
	if (!defined('TB_ABS_PATH')) define('TB_ABS_PATH', get_template_directory());
	if (!defined('TB_URI_PATH_FR')) define('TB_URI_PATH_FR', TB_URI_PATH.'/framework');
	if (!defined('TB_ABS_PATH_FR')) define('TB_ABS_PATH_FR', TB_ABS_PATH.'/framework');
	if (!defined('TB_URI_PATH_ADMIN')) define('TB_URI_PATH_ADMIN', TB_URI_PATH_FR.'/admin');
	if (!defined('TB_ABS_PATH_ADMIN')) define('TB_ABS_PATH_ADMIN', TB_ABS_PATH_FR.'/admin');
	function cayto_enqueue_styles() {
		$parent_style = 'style';
		wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
		wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style )
		);
	}
	add_action( 'wp_enqueue_scripts', 'cayto_enqueue_styles' );

?>
