		<?php $tb_options = $GLOBALS['tb_options'];
			$post_id = isset( $tb_options['tb_error404_page_id'] ) ? intval( $tb_options['tb_error404_page_id'] ) : 0;
		 ?>
		<?php
			$tb_display_footer = isset( $post_id ) && get_post_meta( $post_id, 'tb_display_footer', true )!==false ? get_post_meta( $post_id, 'tb_display_footer', true ) : $tb_options['tb_error404_display_footer'];
			if( $tb_display_footer ){
				$tb_footer_layout = $tb_options['tb_footer_layout'];
				if( isset( $post_id ) && get_post_meta( $post_id, 'tb_footer_layout', true ) ){
					$tb_footer_layout = get_post_meta( $post_id, 'tb_footer_layout', true ) === 'v1' ? 'v1' : 'v2';
				}
				$is_v1 = $tb_footer_layout === 'v1';
		 ?>
		<div class="tb_footer <?php echo (get_post_meta(get_the_ID(), 'tb_footer', true)) ? ' '.esc_attr(get_post_meta(get_the_ID(), 'tb_footer', true)) : ''; ?>">
			<?php if( $is_v1 && ( is_active_sidebar('tbtheme-before-footer-1') || is_active_sidebar('tbtheme-before-footer-2') ) ): ?>
			<div class="footer-header">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 tb_before_footer_1">
							<?php if(is_active_sidebar('tbtheme-before-footer-1')){ dynamic_sidebar("tbtheme-before-footer-1"); } ?>
						</div>
						<div class="col-xs-12 col-sm-6 tb_before_footer_2 text-right">
							<?php if(is_active_sidebar('tbtheme-before-footer-2')){ dynamic_sidebar("tbtheme-before-footer-2"); } ?>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<!-- Start Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row same-height">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 tb_footer_top_one">
							<?php if(is_active_sidebar('tbtheme-footer-top-1')){ dynamic_sidebar("tbtheme-footer-top-1"); } ?>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 tb_footer_top_two">
							<?php if(is_active_sidebar('tbtheme-footer-top-2')){ dynamic_sidebar("tbtheme-footer-top-2"); }?>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 tb_footer_top_three">
							<?php if(is_active_sidebar('tbtheme-footer-top-3')){ dynamic_sidebar("tbtheme-footer-top-3"); }?>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 tb_footer_top_four">
							<?php if(is_active_sidebar('tbtheme-footer-top-4')){ dynamic_sidebar("tbtheme-footer-top-4"); }?>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4 col-lg-3 tb_footer_top_five">
							<?php if( $is_v1 && is_active_sidebar('tbtheme-footer-top-5')){ dynamic_sidebar("tbtheme-footer-top-5"); }else{ dynamic_sidebar("tbtheme-footer-top-5-v2");}?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Footer Top -->
			
			<!-- Start Footer Bottom -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tb_footer_bottom_left">
							<?php if(is_active_sidebar('tbtheme-bottom-left')){ dynamic_sidebar("tbtheme-bottom-left"); }?>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tb_footer_bottom_right">
							<?php if(is_active_sidebar('tbtheme-bottom-right')){ dynamic_sidebar("tbtheme-bottom-right"); }?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Footer Bottom -->
				
		</div>
		<?php }

		// template header 4
		if( isset( $tb_options['tb_header_layout'] ) && $tb_options['tb_header_layout'] == 'v4') echo '</div></div>';
		?>
	</div><!-- #wrap -->
	<a id="tb_back_to_top">
		<span class="go_up">
		<i class="fa fa-angle-up"></i> 
		</span>
	</a>
	<?php wp_footer(); ?>
</body>