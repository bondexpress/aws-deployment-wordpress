<!DOCTYPE html>
<?php $tb_options = $GLOBALS['tb_options']; ?>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ):?>
		<link rel="shortcut icon" href="<?php echo esc_url( $tb_options['tb_favicon_image']['url'] ); ?>" />
	<?php endif; ?>
	<?php wp_head(); ?>
</head>
<body <?php body_class() ?>>
<?php
	$post_id = isset( $tb_options['tb_error404_page_id'] ) ? intval( $tb_options['tb_error404_page_id'] ) : 0;
?>

	<div id="tb_wrapper">
		<?php
			$tb_display_top_sidebar = get_post_meta( $post_id, 'tb_display_top_sidebar', true ) ? get_post_meta( $post_id, 'tb_display_top_sidebar', true ) : ( isset( $tb_options['tb_error404_display_top_sidebar'] ) ? intval( $tb_options['tb_error404_display_top_sidebar'] ) : 0 );
			$tb_display_header = isset( $tb_options['tb_error404_display_header'] ) ? intval( $tb_options['tb_error404_display_header'] ) : 0;
			$tb_display_title = isset( $tb_options['tb_error404_display_title'] ) ? intval( $tb_options['tb_error404_display_title'] ) : 0;
			if( $tb_display_top_sidebar && is_active_sidebar( 'tbtheme-top-sidebar' )){
			?>
				<div class="tb_top_sidebar_wrap">
					<?php dynamic_sidebar("Top Sidebar"); ?>
				</div>
			<?php
			}
		?>
		<?php if( $tb_display_header ) tb_header(); ?>
		<?php if( $tb_display_title ) tb_page_title();?>
		