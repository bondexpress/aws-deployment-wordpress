=== JWSThemes ===
Author: jwsthemes
Requires at least: WordPress 4.6.0
Tested up to: WordPress 4.8.1
Version: 3.0.0
Description: Cayto is a super clean and modern WooCommerce WordPress Theme created specifically for fashion and style bloggers around the globe. A modern blog coupled with a fully styled WooCommerce store means you can write about what you love, and sell the key pieces you mention. 
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html
Tags: one-column, two-columns, right-sidebar, custom-background, custom-colors, editor-style, featured-images, microformats, post-formats, sticky-post, threaded-comments, translation-ready
== Installation ==
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's cayto.zip file. Click Install Now.
3. Click Activate to use your new theme right away. 
