<?php
	/* Define THEME */
	if (!defined('TB_URI_PATH')) define('TB_URI_PATH', get_template_directory_uri());
	if (!defined('TB_ABS_PATH')) define('TB_ABS_PATH', get_template_directory());
	if (!defined('TB_URI_PATH_FR')) define('TB_URI_PATH_FR', TB_URI_PATH.'/framework');
	if (!defined('TB_ABS_PATH_FR')) define('TB_ABS_PATH_FR', TB_ABS_PATH.'/framework');
	if (!defined('TB_URI_PATH_ADMIN')) define('TB_URI_PATH_ADMIN', TB_URI_PATH_FR.'/admin');
	if (!defined('TB_ABS_PATH_ADMIN')) define('TB_ABS_PATH_ADMIN', TB_ABS_PATH_FR.'/admin');
	/* Theme Options */
	if ( !class_exists( 'ReduxFramework' ) ) {
		require_once( TB_ABS_PATH . '/redux-framework/ReduxCore/framework.php' );
	}
	require_once (TB_ABS_PATH_ADMIN.'/theme-options.php');
	require_once (TB_ABS_PATH_ADMIN.'/index.php');
	global $tb_options;
	/* Template Functions */
	require_once TB_ABS_PATH_FR . '/template-functions.php';
	/* Template Functions */
	require_once TB_ABS_PATH_FR . '/templates/post-favorite.php';
	require_once TB_ABS_PATH_FR . '/templates/post-functions.php';
	/* Lib resize images */
	require_once TB_ABS_PATH_FR.'/includes/resize.php';
	/* Post Type */
	require_once TB_ABS_PATH_FR.'/post-type/testimonial.php';
	require_once TB_ABS_PATH_FR.'/post-type/portfolio.php';
	require_once TB_ABS_PATH_FR.'/post-type/team.php';
	/* Function for Framework */
	require_once TB_ABS_PATH_FR . '/includes.php';
		/* Function for OCDI */
	function _cayto_filter_fw_ext_backups_demos($demos)
	{
		$demos_array = array(
			'cayto' => array(
				'title' => esc_html__('Cayto Demo', 'cayto'),
				'screenshot' => 'http://gavencreative.com/import_demo/cayto/screenshot.jpg',
				'preview_link' => 'http://cayto.jwsuperthemes.com',
			),
		);
        $download_url = 'http://gavencreative.com/import_demo/cayto/download-script/';
		foreach ($demos_array as $id => $data) {
			$demo = new FW_Ext_Backups_Demo($id, 'piecemeal', array(
				'url' => $download_url,
				'file_id' => $id,
			));
			$demo->set_title($data['title']);
			$demo->set_screenshot($data['screenshot']);
			$demo->set_preview_link($data['preview_link']);

			$demos[$demo->get_id()] = $demo;

			unset($demo);
		}

		return $demos;
	}
add_filter('fw:ext:backups-demo:demos', '_cayto_filter_fw_ext_backups_demos');
	/* Register Sidebar */
	if (!function_exists('tb_RegisterSidebar')) {
		function tb_RegisterSidebar(){
			global $tb_options;
			register_sidebar(array(
			'name' => __('Right Sidebar', 'cayto'),
			'id' => 'tbtheme-right-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Left Sidebar', 'cayto'),
			'id' => 'tbtheme-left-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Header Top Widget 1', 'cayto'),
			'id' => 'tbtheme-header-top-widget-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Header Top Widget 2', 'cayto'),
			'id' => 'tbtheme-header-top-widget-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Header Top Widget 3 for Header 5', 'cayto'),
			'id' => 'tbtheme-header-top-widget-3',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Header Top Widget 4 For Header 7', 'cayto'),
			'id' => 'tbtheme-header-top-widget-4',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Before Footer Top 1', 'cayto'),
			'id' => 'tbtheme-before-footer-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Before Footer Top 2', 'cayto'),
			'id' => 'tbtheme-before-footer-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Top 1', 'cayto'),
			'id' => 'tbtheme-footer-top-1',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Top 2', 'cayto'),
			'id' => 'tbtheme-footer-top-2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Top 3', 'cayto'),
			'id' => 'tbtheme-footer-top-3',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Top 4', 'cayto'),
			'id' => 'tbtheme-footer-top-4',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Top 5', 'cayto'),
			'id' => 'tbtheme-footer-top-5',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Top 5 V2', 'cayto'),
			'id' => 'tbtheme-footer-top-5-v2',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Sidebar', 'cayto'),
			'id' => 'tbtheme-footer-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Bottom Left', 'cayto'),
			'id' => 'tbtheme-bottom-left',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Footer Bottom Right', 'cayto'),
			'id' => 'tbtheme-bottom-right',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebars(4,array(
			'name' => __('Custom Widget %d', 'cayto'),
			'id' => 'tbtheme-custom-widget',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '<div style="clear:both;"></div></div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			register_sidebar(array(
			'name' => __('Popup Newsletter Sidebar', 'cayto'),
			'id' => 'tbtheme-popup-newsletter-sidebar',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h3 class="wg-title">',
			'after_title' => '</h3>',
			));
			if (class_exists ( 'Woocommerce' )) {
				register_sidebar(array(
				'name' => __('Woocommerce Canvas Sidebar', 'cayto'),
				'id' => 'tbtheme-woo-canvas-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="wg-title"><span>',
				'after_title' => '</span></h3>',
				));
				register_sidebar(array(
				'name' => __('Woocommerce Account Sidebar', 'cayto'),
				'id' => 'tbtheme-woo-account-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="wg-title"><span>',
				'after_title' => '</span></h3>',
				));				
				register_sidebar(array(
				'name' => __('Woocommerce Sidebar', 'cayto'),
				'id' => 'tbtheme-woo-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="wg-title"><span>',
				'after_title' => '</span></h3>',
				));
				register_sidebar(array(
				'name' => __('Woocommerce Shop Sidebar', 'cayto'),
				'id' => 'tbtheme-woo-shop-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="wg-title">',
				'after_title' => '</h3>',
				));
				register_sidebar(array(
				'name' => __('Woocommerce Shop Full Width Sidebar', 'cayto'),
				'id' => 'tbtheme-woo-archive-attr-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="wg-title">',
				'after_title' => '</h3>',
				));
				register_sidebar(array(
				'name' => __('Woocommerce Shop Single Sidebar', 'cayto'),
				'id' => 'tbtheme-woo-single-sidebar',
				'before_widget' => '<div id="%1$s" class="widget %2$s">',
				'after_widget' => '</div>',
				'before_title' => '<h3 class="wg-title">',
				'after_title' => '</h3>',
				));
			}
		}
	}
	add_action( 'init', 'tb_RegisterSidebar' );
	/* Add Stylesheet And Script */
	if (!function_exists('tb_theme_enqueue_style')) {
		function tb_theme_enqueue_style() {
			global $tb_options;
			wp_enqueue_style( 'bootstrap.min', TB_URI_PATH.'/assets/css/bootstrap.min.css', false );
			wp_enqueue_style('flexslider', TB_URI_PATH . "/assets/vendors/flexslider/flexslider.css",array(),"");
			wp_enqueue_style('owl-carousel', TB_URI_PATH . "/assets/vendors/owl-carousel/owl.carousel.css",array(),"");
			wp_enqueue_style('jquery.mCustomScrollbar', TB_URI_PATH . "/assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css",array(),"");
			wp_enqueue_style('jquery.fancybox', TB_URI_PATH . "/assets/vendors/FancyBox/jquery.fancybox.css",array(),"");
			wp_enqueue_style('colorbox', TB_URI_PATH . "/assets/css/colorbox.css",array(),"");
			wp_enqueue_style('font-awesome', TB_URI_PATH.'/assets/css/font-awesome.min.css', array(), '4.1.0');
			wp_enqueue_style( 'tb.core.min', TB_URI_PATH.'/assets/css/tb.core.min.css', false );
			if(class_exists('WooCommerce')){
				wp_enqueue_style( 'woocommerce', TB_URI_PATH . '/assets/css/woocommerce.css', array(), '1.0.0');
			}
			wp_enqueue_style( 'shortcodes', TB_URI_PATH_FR.'/shortcodes/shortcodes.css', false );
			wp_enqueue_style( 'main-style', TB_URI_PATH.'/assets/css/main-style.css', false );
			wp_enqueue_style( 'style', TB_URI_PATH.'/style.css', false );
			if( isset( $tb_options['tb_box_style'] ) && $tb_options['tb_box_style'] ){
				wp_enqueue_style( 'box-style', TB_URI_PATH.'/assets/css/box-style.css', false );
			}
		}
		add_action( 'wp_enqueue_scripts', 'tb_theme_enqueue_style' );
	}
	if (!function_exists('tb_theme_enqueue_script')) {
		function tb_theme_enqueue_script() {
			global $tb_options;
			wp_enqueue_script("jquery");
			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
				wp_enqueue_script( 'comment-reply' );
			}
			wp_enqueue_script( 'jquery.flexslider-min', TB_URI_PATH.'/assets/vendors/flexslider/jquery.flexslider-min.js', array('jquery') );
			wp_enqueue_script( 'owl.carousel.min', TB_URI_PATH.'/assets/vendors/owl-carousel/owl.carousel.min.js', array('jquery') );
			wp_enqueue_script( 'jquery.mCustomScrollbar', TB_URI_PATH.'/assets/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js', array('jquery') );
			wp_enqueue_script( 'jquery.fancybox', TB_URI_PATH.'/assets/vendors/FancyBox/jquery.fancybox.js', array('jquery') );
			wp_enqueue_script( 'jquery.elevatezoom', TB_URI_PATH.'/assets/vendors/elevatezoom-master/jquery.elevatezoom.js', array('jquery') );
			wp_enqueue_script( 'bootstrap.min', TB_URI_PATH.'/assets/js/bootstrap.min.js', array('jquery') );
			wp_enqueue_script('jquery.colorbox', TB_URI_PATH . "/assets/js/jquery.colorbox.js", array('jquery'),"1.5.5");
			wp_enqueue_script( 'tb.shortcodes', TB_URI_PATH_FR.'/shortcodes/shortcodes.js', array('jquery') );
			wp_enqueue_script( 'parallax', TB_URI_PATH.'/assets/js/parallax.js', array('jquery') );
			wp_enqueue_script( 'easytabs', TB_URI_PATH.'/assets/js/jquery.easytabs.min.js', array('jquery') );
			wp_register_script( 'countUP', TB_URI_PATH.'/assets/js/jquery.incremental-counter.min.js', array('jquery') );
			wp_register_script( 'instafeed-custom', TB_URI_PATH.'/assets/js/instafeed.min.js', array('jquery') );
			wp_register_script( 'cycle2', TB_URI_PATH.'/assets/vendors/cycle2/jquery.cycle2.min.js', array('jquery') );
			wp_register_script( 'cycle2-carousel', TB_URI_PATH.'/assets/vendors/cycle2/jquery.cycle2.carousel.min.js', array('cycle2') );
			wp_enqueue_script( 'main', TB_URI_PATH.'/assets/js/main.js', array('jquery') );
			if( isset( $tb_options['tb_box_style'] ) && $tb_options['tb_box_style'] ){
				wp_enqueue_script( 'box-style', TB_URI_PATH.'/assets/js/box-style.js', array('jquery','main') );
			}
			wp_localize_script(
			'main',
			'the_ajax_script',
			array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'assets_img' => TB_URI_PATH.'/assets/images/',
			'primary_color' => $tb_options['tb_primary_color']
			)
			);
		}
		add_action( 'wp_enqueue_scripts', 'tb_theme_enqueue_script' );
	}
	function tb_load_footer_css() {
		global $tb_options;
		if( isset( $tb_options['tb_box_style'] ) && $tb_options['tb_box_style'] ){
		?>
		<style id='box-style-inline-css' type='text/css'>
			<?php
				$response = wp_remote_get( TB_URI_PATH.'/assets/css/custom-style.css' );
				$custom_style = '';
				if( is_array($response) ) {
					$custom_style = $response['body'];
				}
				echo str_replace('#19c4aa', $tb_options['tb_primary_color'], $custom_style );
			?>
		</style>
		<?php
		}
	}
	add_action( 'get_footer', 'tb_load_footer_css' );
	/*Style Inline*/
	require TB_ABS_PATH_FR.'/style-inline.php';
	/* Less */
	if(isset($tb_options['tb_less'])&&$tb_options['tb_less']){
		require_once TB_ABS_PATH_FR.'/presets.php';
	}
	/* Widgets */
	require_once TB_ABS_PATH_FR.'/widgets/widgets.php';
	/* Woo commerce function */
	if (class_exists('Woocommerce')) {
		require_once TB_ABS_PATH . '/woocommerce/wc-template-function.php';
		require_once TB_ABS_PATH . '/woocommerce/wc-template-hooks.php';
	}	