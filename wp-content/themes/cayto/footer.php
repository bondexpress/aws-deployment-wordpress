		<?php global $post;
		$tb_options = $GLOBALS['tb_options'];
		$is_header_v4 = isset( $tb_options['tb_header_layout'] ) && $tb_options['tb_header_layout'] == 'v4';
		 ?>
		<?php

			$tb_display_footer = (isset( $post->ID ) && get_post_meta( $post->ID, 'tb_display_footer', true )!=='') ? get_post_meta( $post->ID, 'tb_display_footer', true ) : $tb_options['tb_display_footer'];
			if( $tb_display_footer ){
				$tb_footer_layout = $tb_options['tb_footer_layout'];
				if( isset( $post->ID ) && get_post_meta( $post->ID, 'tb_footer_layout', true ) ){
					$tb_ft = get_post_meta( $post->ID, 'tb_footer_layout', true );
					$tb_footer_layout = in_array( $tb_ft, array('v1','v2','v3') ) ? $tb_ft : 'v2';
				}
				$is_v1 = $tb_footer_layout !== 'v2';
		 ?>
		<?php if( $is_header_v4 ){echo '<div class="footer-layout-4">';} ?>
		<div class="tb_footer tb_footer_<?php echo esc_attr($tb_footer_layout); ?>">
			<?php if( $is_v1 && ( is_active_sidebar('tbtheme-before-footer-1') || is_active_sidebar('tbtheme-before-footer-2') ) ): ?>
			<div class="footer-header">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 tb_before_footer_1">
							<?php if(is_active_sidebar('tbtheme-before-footer-1')){ dynamic_sidebar("tbtheme-before-footer-1"); } ?>
						</div>
						<?php
							if( $is_header_v4 ):
						?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tb_footer_bottom_right">
							<?php if(is_active_sidebar('tbtheme-bottom-right')){ dynamic_sidebar("tbtheme-bottom-right"); } ?>
						</div>
						<?php else: ?>
						<div class="col-xs-12 col-sm-6 tb_before_footer_2 text-right">
							<?php if(is_active_sidebar('tbtheme-before-footer-2')){ dynamic_sidebar("tbtheme-before-footer-2");} ?>
						</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<!-- Start Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row same-height">
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 tb_footer_top_one">
							<?php if(is_active_sidebar('tbtheme-footer-top-1')){ dynamic_sidebar("tbtheme-footer-top-1"); } ?>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 tb_footer_top_two">
							<?php if(is_active_sidebar('tbtheme-footer-top-2')){ dynamic_sidebar("tbtheme-footer-top-2"); }?>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 tb_footer_top_three">
							<?php if(is_active_sidebar('tbtheme-footer-top-3')){ dynamic_sidebar("tbtheme-footer-top-3"); }?>
						</div>
						<div class="col-xs-12 col-sm-4 col-md-4 col-lg-2 tb_footer_top_four">
							<?php if(is_active_sidebar('tbtheme-footer-top-4')){ dynamic_sidebar("tbtheme-footer-top-4"); }?>
						</div>
						<div class="col-xs-12 col-sm-8 col-md-4 col-lg-3 tb_footer_top_five">
							<?php if( $is_v1 && is_active_sidebar('tbtheme-footer-top-5')){ dynamic_sidebar("tbtheme-footer-top-5"); }else{ dynamic_sidebar("tbtheme-footer-top-5-v2");}?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Footer Top -->
			<?php if( !$is_header_v4 ): ?>
			
			<!-- Start Footer Bottom -->
			<div class="footer-bottom">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tb_footer_bottom_left">
							<?php if(is_active_sidebar('tbtheme-bottom-left')){ dynamic_sidebar("tbtheme-bottom-left"); }?>
						</div>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 tb_footer_bottom_right">
							<?php if(is_active_sidebar('tbtheme-bottom-right')){ dynamic_sidebar("tbtheme-bottom-right"); }?>
						</div>
					</div>
				</div>
			</div>
			<!-- End Footer Bottom -->
			<?php endif; ?>
				
		</div>
		<?php if( $is_header_v4 ){echo '</div>';} ?>
		<?php }

		// template header 4
		if( $is_header_v4 ) echo '</div></div>';
		?>
	</div><!-- #wrap -->
	<div style="display: none;">
		<div id="tb_send_mail" class="tb-send-mail-wrap">
			<?php if(is_active_sidebar('tbtheme-popup-newsletter-sidebar')){ dynamic_sidebar("tbtheme-popup-newsletter-sidebar"); }?>
		</div>
	</div>
	<a id="tb_back_to_top">
		<span class="go_up">
		<i class="fa fa-angle-up"></i> 
		</span>
	</a>
	<?php
	if( isset( $tb_options['tb_box_style'] ) && $tb_options['tb_box_style'] ){
		require_once TB_ABS_PATH_FR.'/includes/box-style.php';
	}
	?>
	<?php wp_footer(); ?>
</body>
</html>