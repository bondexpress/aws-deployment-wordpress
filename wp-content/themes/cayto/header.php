<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<?php $tb_options = $GLOBALS['tb_options']; $tb_body_layout= isset( $tb_options['tb_body_layout'] ) ? esc_attr( $tb_options['tb_body_layout'] ) : ''; ?>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?php if ( ! function_exists( 'has_site_icon' ) || ! has_site_icon() ):?>
		<link rel="shortcut icon" href="<?php echo esc_url( $tb_options['tb_favicon_image']['url'] ); ?>" />
		<?php endif; ?>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class( $tb_body_layout ) ?>>	
		<div id="tb_wrapper">
			<?php
				$tb_display_top_sidebar = get_post_meta(get_the_ID(), 'tb_display_top_sidebar', true)?get_post_meta(get_the_ID(), 'tb_display_top_sidebar', true):0;
				if(is_active_sidebar( 'tbtheme-top-sidebar' ) && $tb_display_top_sidebar){
				?>
				<div class="tb_top_sidebar_wrap">
					<?php dynamic_sidebar("Top Sidebar"); ?>
				</div>
				<?php
				}
			?>
			<?php tb_header(); ?>
		<?php tb_page_title();?>		