<?php
function tb_title_func($atts) {
    $title = $color = $el_align =$animation = $el_class = '';
    extract(shortcode_atts(array(
        'title' => '',
		'sub_title' => '',
		'title_background' => '',
        'title_tpl' => 'tpl3',
		'font_size' => '',
        'color' => '',
        'el_align' => 'text-center',
        'animation' => '',
        'el_class' => ''
    ), $atts));

    $class = $style = $parent_class = array();
    $class[] = 'cayto-title';
    if( $title_tpl == 'tpl3' ){
        $parent_class[] = $el_align;
        $parent_class[] = getCSSAnimation($animation);
    }else{
        $class[] = $el_align;
        $class[] = getCSSAnimation($animation);
    }
	if($font_size){
        $style[] = "font-size: {$font_size}px";
    }
    if($color){
        $style[] = "color: $color";
    }
    $class[] = $el_class;
	$layout = "";

    $title = wp_kses_post( $title );
        if ($title_tpl == 'tpl1'){
            ob_start();
            $class[] = "cayto-title-underline cayto-title-underline-1";
            ?>
                <h3 class="<?php echo esc_attr(implode(' ', $class)); ?>" <?php if($style)echo 'style="'.esc_attr(implode('; ', $style)).'"'; ?>>
                    <?php echo $title;?>
                </h3>
            <?php
            $layout = ob_get_clean();
        } elseif ($title_tpl == 'tpl2'){
            $bg_title = wp_get_attachment_image_src($title_background, 'full');
            ob_start();
            $class[] = "cayto-title-underline cayto-title-underline-2";
            ?>
                <h3 class="<?php echo esc_attr(implode(' ', $class)); ?>" <?php if($style)echo 'style="'.esc_attr(implode('; ', $style)).'"'; ?>>
                    <?php echo $title;?>
                </h3>
            <?php
            $layout = ob_get_clean();
        }else if($title_tpl == 'tpl3'){
            ob_start();
            ?>
            <div class="cayto-title-separator-wrap <?php echo esc_attr( implode(' ', $parent_class) );?>">
                <h3 class="cayto-title-separator <?php echo esc_attr(implode(' ', $class)); ?>" <?php if($style) echo 'style="'. esc_attr(implode('; ', $style))  .'"'; ?>>
                    <span>
                    <?php echo $title;?>
                    </span>
                </h3>
            </div>
            <?php
            $layout = ob_get_clean();
        }else{
            ob_start();
            ?>
            <div class="cayto-title-default">
                <h3 class="<?php echo esc_attr(implode(' ', $class)); ?>" <?php if($style) echo 'style="'. esc_attr(implode('; ', $style))  .'"'; ?>>
                    <?php echo $title;?>
                </h3>
            </div>
            <?php
            $layout = ob_get_clean();
        }
		
	
	return $layout;
}

if(function_exists('insert_shortcode')) { insert_shortcode('title', 'tb_title_func'); }
