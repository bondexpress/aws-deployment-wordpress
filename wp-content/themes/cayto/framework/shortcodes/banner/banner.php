<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
$el_class=$title='';
function tb_banner_widget_func($atts, $content = null) {
    extract(shortcode_atts(array(
		'title' => '',
		'img_src' => '',
		'heading' => '',
		'sub_head' => '',
		'over_color' => 'f7f7f7',
		'text_color' => '',
        'link' => '',
        'el_class' => ''
    ), $atts));

	$content = wpb_js_remove_wpautop($content, true);
	
    $class = array();
	$class[] = 'tb-woo-banner';
	$class[] = $el_class;
	$img_src = wp_get_attachment_image_src( $img_src, 'full' );
    ob_start();
    ?>
		<div class="<?php echo esc_attr(implode(' ', $class)); ?>">
			<?php if( ! empty( $link ) ){ ?>
				<a href="<?php echo esc_url($link);?>">
			<?php } ?>
				<?php if( ! empty( $img_src ) ){ ?>
					<img class="img-responsive" src="<?php echo esc_url( $img_src[0] );?>" alt="<?php echo esc_attr( $sub_head );?>">
				<?php } ?>
			<?php if( !empty( $link )){ ?>
				</a>
			<?php } ?>
			<hgroup style="background-color: <?php echo esc_attr( $over_color );?>">
			<?php if( ! empty( $heading ) ){ ?>
				<h2 class="font-cayto-1" style="color:<?php echo esc_attr( $text_color );?>">
					<?php echo esc_attr( $heading ); ?>
				</h2>
			<?php } ?>
			<?php if( ! empty( $sub_head ) ): ?>
				<h3 class="font-cayto-2"  style="color:<?php echo esc_attr( $text_color );?>">
					<?php echo esc_attr( $sub_head ); ?>
				</h3>
			<?php endif; ?>
			</hgroup>
		</div>
		
    <?php
    return ob_get_clean();
}


if(function_exists('insert_shortcode')) { insert_shortcode('banner', 'tb_banner_widget_func'); }
