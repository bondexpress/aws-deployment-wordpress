<?php
function tb_info_box_func($atts, $content = null) {
    extract(shortcode_atts(array(
        'tpl' => 'tpl1',
        'image' => '',
        'el_class' => ''
    ), $atts));
	
	$content = wpb_js_remove_wpautop($content, true);
	
    $class = array();
	$class[] = 'ct-info-box-wrap';
	$class[] = $tpl;
	$class[] = $el_class;
    ob_start();
    ?>
		<div class="<?php if( $tpl == 'tpl6' ){ echo 'tpl3 ';}elseif( $tpl=='tpl7' ){echo 'tpl5 ';}; echo esc_attr(implode(' ', $class)); ?>">
			<?php include $tpl.".php"; ?>
		</div>
		
    <?php
    return ob_get_clean();
}
if(function_exists('insert_shortcode')) { insert_shortcode('info_box', 'tb_info_box_func');}
