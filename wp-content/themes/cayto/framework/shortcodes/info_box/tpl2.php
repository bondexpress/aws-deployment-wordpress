<div class="tb-info-box">
	<?php 
		echo '<div class="tb-content">'. apply_filters( 'the_content', $content )  .'</div>';	
		echo '<div class="tb-image tb-right">'.wp_get_attachment_image( $image, 'full', false, array('class'=>'img-responsive') ).'</div>';
	?>
</div>