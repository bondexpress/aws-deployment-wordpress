<div id="tb-product-metabox" class='tb_metabox'>
	<div id="tb-tab-product" class='categorydiv'>
		<ul class='category-tabs'>
		   <li class='tb-tab'><a href="#tabs-header"><i class="dashicons dashicons-menu"></i> <?php echo _e('Video','raymond');?></a></li>
		</ul>
		<div class='tb-tabs-panel'>
			<div id="tabs-header">
				<?php
					$this->text('product_video_youtube',
							'Video Youtube',
							''
					);
				?>
			</div>
		</div>
	</div>
</div>