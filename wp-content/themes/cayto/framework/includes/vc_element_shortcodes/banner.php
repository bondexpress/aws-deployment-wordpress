<?php
vc_map ( array (
	"name" => 'Banner Widget',
	"base" => "banner",
	"icon" => "tb-icon-for-vc",
	"category" => __( 'Cayto', 'cayto' ), 
	'admin_enqueue_js' => array(TB_URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
		array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title", 'cayto'),
            "param_name" => "title",
            "value" => "",
            "description" => __("Content.", 'cayto')
        ),
        array(

			"type" => "attach_image",
			"class" => "",
			"heading" => __("Image src", 'cayto'),
			"param_name" => "img_src",
			"value" => "",
			"description" => __("Banner image", 'cayto')
		),
		array (
            "type" => "colorpicker",
            "heading" => __( 'Overlay Color', 'cayto' ),
            "param_name" => "over_color",
            "value" => '',
           	"std" => '#f7f7f7',
            "description" => __( 'Overlay Color', 'cayto' ),
        ),
        array (
            "type" => "colorpicker",
            "heading" => __( 'Overlay Color', 'cayto' ),
            "param_name" => "text_color",
            "value" => '',
           	"std" => '#393939',
            "description" => __( 'Overlay Color', 'cayto' ),
        ),
		array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Heading", 'cayto'),
            "param_name" => "heading",
            "value" => "",
            "description" => __("Heading.", 'cayto')
        ),
		array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Sub Heading", 'cayto'),
            "param_name" => "sub_head",
            "value" => "",
            "description" => __("Sub Heading.", 'cayto')
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Link", 'cayto'),
            "param_name" => "link",
            "value" => "",
            "description" => __("Link.", 'cayto')
        ),
	)
));