<?php
if(class_exists('Woocommerce')){
    vc_map(array(
        "name" => __("Product Grid", 'cayto'),
        "base" => "products_grid",
        "class" => "tb-products-grid",
        "category" => __('Cayto', 'cayto'),
        'admin_enqueue_js' => array(TB_URI_PATH_ADMIN.'/assets/js/customvc.js'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Template", 'cayto'),
                "param_name" => "tpl",
                "value" => array(
					__("Template 1 (Small thumb)",'cayto') => "tpl1",
					__("Template 2 (Medium thumb)",'cayto') => "tpl2"
                ),
                "description" => __('Select template in this elment.', 'cayto')
            ),
			array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Columns", 'cayto'),
                "param_name" => "columns",
                "value" => array(
                    __("4 Columns",'cayto') => "4",
                    __("5 Columns",'cayto') => "5",
                    __("3 Columns",'cayto') => "3",
                    __("2 Columns",'cayto') => "2",
                    __("1 Column",'cayto') => "1",
                ),
				"description" => __('Select columns in this elment.', 'cayto')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'cayto'),
                "param_name" => "el_class",
                "value" => "",
				"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
            ),
			array (
                "type" => "tb_taxonomy",
                "taxonomy" => "product_cat",
                "heading" => __( "Categories", 'cayto' ),
                "param_name" => "product_cat",
                "class" => "",
				"group" => __("Build Query", 'cayto'),
                "description" => __( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'cayto' )
            ),
			array (
					"type" => "dropdown",
					"class" => "",
					"heading" => __( "Show", 'cayto' ),
					"param_name" => "show",
					"value" => array (
							__("All Products",'cayto') => "all_products",
							__("Featured Products",'cayto') => "featured",
							__("On-sale Products",'cayto') => "onsale",
                            __("Variable Products",'cayto') => "variable",
					),
					"group" => __("Build Query", 'cayto'),
					"description" => __( "Select show product type in this elment", 'cayto' )
			),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Product Count", 'cayto'),
                "param_name" => "number",
                "value" => "",
				"group" => __("Build Query", 'cayto'),
				"description" => __('Please, enter number of post per page. Show all: -1.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Hide Free', 'cayto'),
                "param_name" => "hide_free",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
				"group" => __("Build Query", 'cayto'),
                "description" => __('Hide free product in this element.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Hidden', 'cayto'),
                "param_name" => "show_hidden",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
				"group" => __("Build Query", 'cayto'),
                "description" => __('Show Hidden product in this element.', 'cayto')
            ),
            array (
				"type" => "dropdown",
				"heading" => __( 'Order by', 'cayto' ),
				"param_name" => "orderby",
				"value" => array (
						__("None",'cayto') => "none",
						__("Date",'cayto') => "date",
						__("Price",'cayto') => "price",
						__("Random",'cayto') => "rand",
						__("Selling",'cayto') => "selling",
						__("Rated",'cayto') => "rated",
				),
				"group" => __("Build Query", 'cayto'),
				"description" => __( 'Order by ("none", "date", "price", "rand", "selling", "rated") in this element.', 'cayto' )
			),
            array(
                "type" => "dropdown",
                "heading" => __('Order', 'cayto'),
                "param_name" => "order",
                "value" => Array(
                    __("None",'cayto') => "none",
                    __("ASC",'cayto') => "ASC",
                    __("DESC",'cayto') => "DESC"
                ),
				"group" => __("Build Query", 'cayto'),
                "description" => __('Order ("None", "Asc", "Desc") in this element.', 'cayto')
            ),
            array(
                "type" => "checkbox",
                "heading" => __('Show Sale Flash', 'cayto'),
                "param_name" => "show_sale_flash",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide sale flash of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Title', 'cayto'),
                "param_name" => "show_title",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide title of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Price', 'cayto'),
                "param_name" => "show_price",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide price of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Rating', 'cayto'),
                "param_name" => "show_rating",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide rating of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Add To Cart', 'cayto'),
                "param_name" => "show_add_to_cart",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide add to cart of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Quick View', 'cayto'),
                "param_name" => "show_quick_view",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide quick view of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Whishlist', 'cayto'),
                "param_name" => "show_whishlist",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide whishlish of product.', 'cayto')
            ),
			array(
                "type" => "checkbox",
                "heading" => __('Show Compare', 'cayto'),
                "param_name" => "show_compare",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "std"=>1,
				"group" => __("Template", 'cayto'),
                "description" => __('Show or hide compare of product.', 'cayto')
            ),
                array(
                "type" => "checkbox",
                "heading" => __('Show Pagination', 'cayto'),
                "param_name" => "show_pagination",
                "value" => array(
                    __("Yes, please", 'cayto') => 1
                ),
                "group" => __("Template", 'cayto'),
                "description" => __('Show or hide pagination in this element.', 'cayto')
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Align Pagination", 'cayto'),
                "param_name" => "el_align",
                "value" => array(
                    __("Left",'cayto') => "text-left",
                    __("Right",'cayto') => "text-right",
                    __("Center",'cayto') => "text-center"
                ),
                "std" => "text-center",
                "group" => __("Template", 'cayto'),
                "description" => __("Align", 'cayto')
            )
        )
    ));
}
