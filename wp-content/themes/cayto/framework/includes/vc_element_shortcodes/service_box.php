<?php
vc_map(array(
	"name" => __("Service Box", 'cayto'),
	"base" => "service_box",
	"category" => __('Cayto', 'cayto'),
	"icon" => "tb-icon-for-vc",
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Template", 'cayto'),
			"param_name" => "tpl",
			"value" => array(
				__("Default",'cayto') => "tpl",
				__("Template 1 (Border top)",'cayto') => "tpl1",
				__("Template 2 ( Icon on the left )",'cayto') => "tpl3",
				__("Template 3 ( Count Up )",'cayto') => "tpl2"
			),
			"description" => __('Select template in this element.', 'cayto')
		),
		array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Align", 'cayto'),
            "param_name" => "el_align",
            "value" => array(
                __("Left",'cayto') => "text-left",
                __("Right",'cayto') => "text-right",
                __("Center",'cayto') => "text-center"
            ),
            "std" => "text-center",
            "description" => __("Align", 'cayto')
        ),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Icon", 'cayto'),
			"param_name" => "icon",
			"value" => "",
			"std" => "fa fa-",
			"description" => __("Please, enter class icon in this element.", 'cayto')
		),
		array(
			"type" => "textfield",
			"holder" => "div",
			"class" => "",
			"heading" => __("Title", 'cayto'),
			"param_name" => "title",
			"value" => "",
			"description" => __("Please, enter title in this element.", 'cayto')
		),
		array(
			"type" => "textarea_html",
			"class" => "",
			"heading" => __("Description", 'cayto'),
			"param_name" => "content",
			"value" => "",
			"description" => __("Please, enter description in this element.", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Link", 'cayto'),
			"param_name" => "ex_link",
			"value" => "",
			"description" => __("Please, enter extra link in this element.", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
	)
));
