<?php
vc_map ( array (
	"name" => 'Testimonial Slider',
	"base" => "testimonial_slider",
	"icon" => "tb-icon-for-vc",
	"category" => __( 'Cayto', 'cayto' ), 
	'admin_enqueue_js' => array(TB_URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Template", 'cayto'),
            "param_name" => "tpl",
            "value" => array(
                "Template 1 ( Thumbnail on the top )" => "tpl",
                "Template 2( Thumbnail on the left )" => "tpl1"
            ),
            "std"=>"tpl",
            "description" => __('Select template for title.', 'cayto')
        ),
		array (
				"type" => "textfield",
				"heading" => __( 'Count', 'cayto' ),
				"param_name" => "posts_per_page",
				'value' => '',
				"description" => __( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'cayto' )
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Order by', 'cayto' ),
				"param_name" => "orderby",
				"value" => array (
					__("None",'cayto') => "none",
					__("Title",'cayto') => "title",
					__("Date",'cayto') => "date",
					__("ID",'cayto') => "ID"
				),
				"description" => __( 'Order by ("none", "title", "date", "ID").', 'cayto' )
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Order', 'cayto' ),
				"param_name" => "order",
				"value" => Array (
					__("None",'cayto') => "none",
					__("ASC",'cayto') => "ASC",
					__("DESC",'cayto') => "DESC"
				),
				"description" => __( 'Order ("None", "Asc", "Desc").', 'cayto' )
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Image", 'cayto'),
			"param_name" => "show_image",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"group" => __("Template", 'cayto'),
			"std" => true,
			"description" => __("Show or not image of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Ttile", 'cayto'),
			"param_name" => "show_title",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"group" => __("Template", 'cayto'),
			"std" => true,
			"description" => __("Show or not title of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Excerpt", 'cayto'),
			"param_name" => "show_excerpt",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"group" => __("Template", 'cayto'),
			"std" => true,
			"description" => __("Show or not excerpt of post in this element.", 'cayto')
		),
	)
));