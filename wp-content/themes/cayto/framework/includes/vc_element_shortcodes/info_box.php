<?php
vc_map(array(
	"name" => __("Info Box", 'cayto'),
	"base" => "info_box",
	"category" => __('Cayto', 'cayto'),
	"icon" => "tb-icon-for-vc",
	"params" => array(
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Template", 'cayto'),
			"param_name" => "tpl",
			"value" => array(
				__("Template 1 (Horizontal - image on the right)",'cayto') => "tpl1",
				__("Template 2 (Horizontal - image on the right)",'cayto') => "tpl2",
				__("Template 3 (Vertical align)",'cayto') => "tpl3",
				__("Template 4 (One column ~ Wathches)",'cayto') => "tpl4",
				__("Template 5 (Big column ~ Special today)",'cayto') => "tpl5",
				__("Template 6 (Vertical align style as home page 1)",'cayto') => "tpl6",
				__("Template 7 (Big column ~ new trend)",'cayto') => "tpl7",
			),
			"description" => __('Select template in this element.', 'cayto')
		),
		array(
                "type" => "attach_image",
                "class" => "",
                "heading" => __("Image", 'cayto'),
                "param_name" => "image",
                "value" => "",
                "description" => __("Select image in this element", 'cayto')
            ),
		array(
			"type" => "textarea_html",
			"class" => "",
			"heading" => __("Description", 'cayto'),
			"param_name" => "content",
			"value" => "",
			"description" => __("Please, enter description in this element.", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
	)
));
