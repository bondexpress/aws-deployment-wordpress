<?php
vc_map ( array (
	"name" => 'Team',
	"base" => "list_team",
	"icon" => "tb-icon-for-vc",
	"category" => __( 'Cayto', 'cayto' ), 
	'admin_enqueue_js' => array(TB_URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
            "type" => "dropdown",
            "class" => "",
            "heading" => __("Align", 'cayto'),
            "param_name" => "el_align",
            "value" => array(
                __("Left",'cayto') => "text-left",
                __("Right",'cayto')=> "text-right",
                __("Center",'cayto')=> "text-center"
            ),
            "std" => "text-center",
            "description" => __("Align", 'cayto')
        ),
		array (
				"type" => "textfield",
				"heading" => __( 'Count', 'cayto' ),
				"param_name" => "posts_per_page",
				'value' => '',
				"description" => __( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'cayto' )
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Order by', 'cayto' ),
				"param_name" => "orderby",
				"value" => array (
						__("None",'cayto') => "none",
						__("Title",'cayto') => "title",
						__("Date",'cayto') => "date",
						__("ID",'cayto') => "ID"
				),
				"description" => __( 'Order by ("none", "title", "date", "ID").', 'cayto' )
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Order', 'cayto' ),
				"param_name" => "order",
				"value" => Array (
						__("None",'cayto') => "none",
						__("ASC",'cayto') => "ASC",
						__("DESC",'cayto') => "DESC"
				),
				"description" => __( 'Order ("None", "Asc", "Desc").', 'cayto' )
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Image", 'cayto'),
			"param_name" => "show_image",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std"=>true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not image of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Title", 'cayto'),
			"param_name" => "show_title",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std"=>true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not title of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Experience", 'cayto'),
			"param_name" => "show_experience",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std" => true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not title of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Social", 'cayto'),
			"param_name" => "show_social",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std" => true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not excerpt of post in this element.", 'cayto')
		),
	)
));