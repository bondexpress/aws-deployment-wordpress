<?php

vc_map ( array (

		"name" => 'Portfolio Grid',

		"base" => "portfolio_grid",

		"icon" => "tb-icon-for-vc",

		"category" => __( 'Cayto', 'cayto' ), 

		'admin_enqueue_js' => array(TB_URI_PATH_FR .'/admin/assets/js/customvc.js'),

		"params" => array (

					array (

							"type" => "tb_taxonomy",

							"taxonomy" => "portfolio_category",

							"heading" => __( "Portfolio Categories", 'cayto' ),

							"param_name" => "category",

							"description" => __( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'cayto' )

					),

					array (

							"type" => "textfield",

							"heading" => __( 'Count', 'cayto' ),

							"param_name" => "posts_per_page",

							'value' => '',

							"description" => __( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'cayto' )

					),

					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Template", 'cayto'),
						"param_name" => "tpl",
						"value" => array(
							__("Template 1 ( Overlay effect )",'cayto') => "tpl1",
							__("Template 2 ( Overlay effect With Icon )",'cayto') => "tpl2",
							__("Default",'cayto') => "tpl"
						),
						"std" => 'tpl',
						"group" => __("Template", 'cayto'),
						"description" => __('Select template in this element.', 'cayto')
					),

					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("Show Filter", 'cayto'),

						"param_name" => "show_filter",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),
						"std" => true,


						"description" => __("Show or not show filter in this element.", 'cayto')

					),

					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("Show Pagination", 'cayto'),

						"param_name" => "show_pagination",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),

						"description" => __("Show or not show pagination in this element.", 'cayto')

					),

					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("No Padding", 'cayto'),

						"param_name" => "no_pading",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),
						"std" => false,

						"description" => __("No padding in each items", 'cayto')

					),

					array(

							"type" => "dropdown",

							"class" => "",

							"heading" => __("Columns", 'cayto'),

							"param_name" => "columns",

							"value" => array(
								__("4 Columns",'cayto') => "4",
								__("3 Columns",'cayto') => "3",

								__("2 Columns",'cayto') => "2",

								__("1 Column",'cayto') => "1",

							),

							"description" => __('Select columns display in this element.', 'cayto')

					),

					array (

							"type" => "dropdown",

							"heading" => __( 'Order by', 'cayto' ),

							"param_name" => "orderby",

							"value" => array (

									__("None",'cayto') => "none",

									__("Title",'cayto') => "title",

									__("Date",'cayto') => "date",

									__("ID",'cayto') => "ID"

							),

							"description" => __( 'Order by ("none", "title", "date", "ID").', 'cayto' )

					),

					array (

							"type" => "dropdown",

							"heading" => __( 'Order', 'cayto' ),

							"param_name" => "order",

							"value" => Array (

									__("None",'cayto') => "none",

									__("ASC",'cayto') => "ASC",

									__("DESC",'cayto') => "DESC"

							),

							"description" => __( 'Order ("None", "Asc", "Desc").', 'cayto' )

					),

					array(

						"type" => "textfield",

						"class" => "",

						"heading" => __("Extra Class", 'cayto'),

						"param_name" => "el_class",

						"value" => "",

						"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )

					),

					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("Show Title", 'cayto'),

						"param_name" => "show_title",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),

						"std" => true,

						"group" => __("Template", 'cayto'),

						"description" => __("Show or not title of post in this element.", 'cayto')

					),

					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("Show Category", 'cayto'),

						"param_name" => "show_category",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),

						"std" => true,

						"group" => __("Template", 'cayto'),

						"description" => __("Show or not category of post in this element.", 'cayto')

					),

					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("Show View Now", 'cayto'),

						"param_name" => "show_readmore",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),


						"group" => __("Template", 'cayto'),

						"description" => __("Show or not View Now of post in this element.", 'cayto')

					),
					array(

						"type" => "checkbox",

						"class" => "",

						"heading" => __("Show View More", 'cayto'),

						"param_name" => "show_viewmore",

						"value" => array (

							__( "Yes, please", 'cayto' ) => true

						),
						"std"=>true,

						"group" => __("Template", 'cayto'),

						"description" => __("Show or not view more button.", 'cayto')

					)

		)

));