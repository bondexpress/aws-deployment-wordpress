<?php
vc_map(array(
    "name" => 'Google Maps V3',
    "base" => "maps",
    "category" => __('Cayto', 'cayto'),
	"icon" => "tb-icon-for-vc",
    "description" => __('Google Maps API V3', 'cayto'),
    "params" => array(
        array(
            "type" => "textfield",
            "heading" => __('API Key', 'cayto'),
            "param_name" => "api",
            "value" => '',
            "description" => __('Enter you api key of map, get key from (https://console.developers.google.com)', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Address', 'cayto'),
            "param_name" => "address",
            "value" => 'New York, United States',
            "description" => __('Enter address of Map', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Coordinate', 'cayto'),
            "param_name" => "coordinate",
            "value" => '',
            "description" => __('Enter coordinate of Map, format input (latitude, longitude)', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Click Show Info window', 'cayto'),
            "param_name" => "infoclick",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Marker", 'cayto'),
            "description" => __('Click a marker and show info window (Default Show).', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Marker Coordinate', 'cayto'),
            "param_name" => "markercoordinate",
            "value" => '',
            "group" => __("Marker", 'cayto'),
            "description" => __('Enter marker coordinate of Map, format input (latitude, longitude)', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Marker Title', 'cayto'),
            "param_name" => "markertitle",
            "value" => '',
            "group" => __("Marker", 'cayto'),
            "description" => __('Enter Title Info windows for marker', 'cayto')
        ),
        array(
            "type" => "textarea",
            "heading" => __('Marker Description', 'cayto'),
            "param_name" => "markerdesc",
            "value" => '',
            "group" => __("Marker", 'cayto'),
            "description" => __('Enter Description Info windows for marker', 'cayto')
        ),
        array(
            "type" => "attach_image",
            "heading" => __('Marker Icon', 'cayto'),
            "param_name" => "markericon",
            "value" => '',
            "group" => __("Marker", 'cayto'),
            "description" => __('Select image icon for marker', 'cayto')
        ),
        array(
            "type" => "textarea_raw_html",
            "heading" => __('Marker List', 'cayto'),
            "param_name" => "markerlist",
            "value" => '',
            "group" => __("Multiple Marker", 'cayto'),
            "description" => __('[{"coordinate":"41.058846,-73.539423","icon":"","title":"title demo 1","desc":"desc demo 1"},{"coordinate":"40.975699,-73.717636","icon":"","title":"title demo 2","desc":"desc demo 2"},{"coordinate":"41.082606,-73.469718","icon":"","title":"title demo 3","desc":"desc demo 3"}]', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Info Window Max Width', 'cayto'),
            "param_name" => "infowidth",
            "value" => '200',
            "group" => __("Marker", 'cayto'),
            "description" => __('Set max width for info window', 'cayto')
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Map Type", 'cayto'),
            "param_name" => "type",
            "value" => array(
                "ROADMAP" => "ROADMAP",
                "HYBRID" => "HYBRID",
                "SATELLITE" => "SATELLITE",
                "TERRAIN" => "TERRAIN"
            ),
            "description" => __('Select the map type.', 'cayto')
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Style Template", 'cayto'),
            "param_name" => "style",
            "value" => array(
                "Default" => "",
                "Subtle Grayscale" => "Subtle-Grayscale",
                "Shades of Grey" => "Shades-of-Grey",
                "Blue water" => "Blue-water",
                "Pale Dawn" => "Pale-Dawn",
                "Blue Essence" => "Blue-Essence",
                "Apple Maps-esque" => "Apple-Maps-esque",
            ),
            "group" => __("Map Style", 'cayto'),
            "description" => 'Select your heading size for title.'
        ),
        array(
            "type" => "textfield",
            "heading" => __('Zoom', 'cayto'),
            "param_name" => "zoom",
            "value" => '13',
            "description" => __('zoom level of map, default is 13', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Width', 'cayto'),
            "param_name" => "width",
            "value" => 'auto',
            "description" => __('Width of map without pixel, default is auto', 'cayto')
        ),
        array(
            "type" => "textfield",
            "heading" => __('Height', 'cayto'),
            "param_name" => "height",
            "value" => '350px',
            "description" => __('Height of map without pixel, default is 350px', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Scroll Wheel', 'cayto'),
            "param_name" => "scrollwheel",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('If false, disables scrollwheel zooming on the map. The scrollwheel is disable by default.', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Pan Control', 'cayto'),
            "param_name" => "pancontrol",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('Show or hide Pan control.', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Zoom Control', 'cayto'),
            "param_name" => "zoomcontrol",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('Show or hide Zoom Control.', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Scale Control', 'cayto'),
            "param_name" => "scalecontrol",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('Show or hide Scale Control.', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Map Type Control', 'cayto'),
            "param_name" => "maptypecontrol",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('Show or hide Map Type Control.', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Street View Control', 'cayto'),
            "param_name" => "streetviewcontrol",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('Show or hide Street View Control.', 'cayto')
        ),
        array(
            "type" => "checkbox",
            "heading" => __('Over View Map Control', 'cayto'),
            "param_name" => "overviewmapcontrol",
            "value" => array(
                __("Yes, please", 'cayto') => true
            ),
            "group" => __("Controls", 'cayto'),
            "description" => __('Show or hide Over View Map Control.', 'cayto')
        )
    )
));