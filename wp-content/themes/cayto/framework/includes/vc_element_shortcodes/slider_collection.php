<?php

vc_map(array(

	"name" => __("Slider Collection", 'cayto'),

	"base" => "slider_collection",

	"class" => "title",

	"category" => __('Cayto', 'cayto'),

	"icon" => "tb-icon-for-vc",

	"params" => array(

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Extra Class", 'cayto'),

			"param_name" => "el_class",

			"value" => "",

			"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto')

		),

		

		array(

			"type" => "attach_image",

			"class" => "",

			"heading" => __("Image", 'cayto'),

			"param_name" => "image_sl1",

			"value" => "",

			"group" => __("Slider 1", 'cayto'),

			"description" => __("Select image of slider.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Heading 1", 'cayto'),

			"param_name" => "heading1_sl1",

			"value" => "",

			"group" => __("Slider 1", 'cayto'),

			"description" => __("Please, Enter text of heading.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Heading 2", 'cayto'),

			"param_name" => "heading2_sl1",

			"value" => "",

			"group" => __("Slider 1", 'cayto'),

			"description" => __("Please, Enter text of heading.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Link", 'cayto'),

			"param_name" => "link_sl1",

			"value" => "",

			"group" => __("Slider 1", 'cayto'),

			"description" => __("Enter link of slider/ Remove blank field.", 'cayto')

		),

		

		array(

			"type" => "attach_image",

			"class" => "",

			"heading" => __("Image", 'cayto'),

			"param_name" => "image_sl2",

			"value" => "",

			"group" => __("Slider 2", 'cayto'),

			"description" => __("Select image of slider.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Heading 1", 'cayto'),

			"param_name" => "heading1_sl2",

			"value" => "",

			"group" => __("Slider 2", 'cayto'),

			"description" => __("Please, Enter text of heading.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Heading 2", 'cayto'),

			"param_name" => "heading2_sl2",

			"value" => "",

			"group" => __("Slider 2", 'cayto'),

			"description" => __("Please, Enter text of heading.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Link", 'cayto'),

			"param_name" => "link_sl2",

			"value" => "",

			"group" => __("Slider 2", 'cayto'),

			"description" => __("Enter link of slider/ Remove blank field.", 'cayto')

		),
		

		array(

			"type" => "attach_image",

			"class" => "",

			"heading" => __("Image", 'cayto'),

			"param_name" => "image_sl3",

			"value" => "",

			"group" => __("Slider 3", 'cayto'),

			"description" => __("Select image of slider.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Heading 1", 'cayto'),

			"param_name" => "heading1_sl3",

			"value" => "",

			"group" => __("Slider 3", 'cayto'),

			"description" => __("Please, Enter text of heading.", 'cayto')

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Heading 2", 'cayto'),

			"param_name" => "heading2_sl3",

			"value" => "",

			"group" => __("Slider 3", 'cayto'),

			"description" => __("Please, Enter text of heading.", 'cayto')

		),
		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Link", 'cayto'),

			"param_name" => "link_sl3",

			"value" => "",

			"group" => __("Slider 3", 'cayto'),

			"description" => __("Enter link of slider/ Remove blank field.", 'cayto')

		)
		

	)

));

