<?php
vc_map ( array (
	"name" => 'Blog Grid',
	"base" => "blog_grid",
	"icon" => "tb-icon-for-vc",
	"category" => __( 'Cayto', 'cayto' ), 
	'admin_enqueue_js' => array(TB_URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Template", 'cayto'),
			"param_name" => "tpl",
			"value" => array(
				__("Template 1",'cayto') => "tpl1",
				__("Template 2",'cayto') => "tpl2",
				__("Template 3 (No padding)",'cayto') => "ct-blog-small-grid"
			),
			"description" => __('Select template in this element.', 'cayto')
		),
		array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Columns", 'cayto'),
				"param_name" => "columns",
				"value" => array(
					__("4 Columns",'cayto') => "4",
					__("3 Columns",'cayto') => "3",
					__("2 Columns",'cayto') => "2",
					__("1 Column",'cayto') => "1",
				),
				"std"=>3,
				"description" => __('Select columns display in this element.', 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Pagination", 'cayto'),
			"param_name" => "show_pagination",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"description" => __("Show or not show pagination in this element.", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
		array (
				"type" => "tb_taxonomy",
				"taxonomy" => "category",
				"heading" => __( "Categories", 'cayto' ),
				"param_name" => "category",
				"group" => __("Build Query", 'cayto'),
				"description" => __( "Note: By default, all your projects will be displayed. <br>If you want to narrow output, select category(s) above. Only selected categories will be displayed.", 'cayto' )
		),
		
		array (
				"type" => "textfield",
				"heading" => __( 'Count', 'cayto' ),
				"param_name" => "posts_per_page",
				'value' => '',
				"group" => __("Build Query", 'cayto'),
				"description" => __( 'The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'cayto' )
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Order by', 'cayto' ),
				"param_name" => "orderby",
				"value" => array (
						__( "None",'cayto') => "none",
						__("Title",'cayto') => "title",
						__("Date",'cayto') => "date",
						__("ID" ,'cayto')=> "ID"
				),
				"group" => __("Build Query", 'cayto'),
				"description" => __( 'Order by ("none", "title", "date", "ID").', 'cayto' )
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Order', 'cayto' ),
				"param_name" => "order",
				"value" => Array (
						__("None",'cayto') => "none",
						__("ASC" ,'cayto')=> "ASC",
						__("DESC",'cayto') => "DESC"
				),
				"group" => __("Build Query", 'cayto'),
				"description" => __( 'Order ("None", "Asc", "Desc").', 'cayto' )
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Image", 'cayto'),
			"param_name" => "show_thumb",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std"=>true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not image of post in this element.", 'cayto')
		),
		array (
				"type" => "dropdown",
				"heading" => __( 'Image Size', 'cayto' ),
				"param_name" => "thumb_size",
				"value" => Array (
						__("Full",'cayto' ) => "full",
						__("Large",'cayto' ) => "large",
						__("Large Hard Crop",'cayto') => "cayto-blog-large-hard-crop",
						__("Medium",'cayto' ) => "medium",
						__("Medium Hard Crop",'cayto') => "cayto-blog-medium-hard-crop",
						__("Blog Grid",'cayto' ) => "cayto-blog-grid",
						__("Blog Grid Medium (270x200)",'cayto') => 'cayto-blog-grid-medium',
						__("Thumbnail",'cayto' ) => "thumbnail"
				),
				"group" => __("Template", 'cayto'),
				"description" => __( 'Select image size of post in this element.', 'cayto' )
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Title", 'cayto'),
			"param_name" => "show_title",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std"=>true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not title of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Info", 'cayto'),
			"param_name" => "show_info",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std"=>true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not info of post in this element.", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Show Excerpt", 'cayto'),
			"param_name" => "show_excerpt",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"std"=>true,
			"group" => __("Template", 'cayto'),
			"description" => __("Show or not excerpt of post in this element.", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Excerpt Lenght", 'cayto'),
			"param_name" => "excerpt_lenght",
			"value" => "",
			"group" => __("Template", 'cayto'),
			"description" => __("Please, Enter excerpt lenght in this element. EX: 20", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Excerpt More", 'cayto'),
			"param_name" => "excerpt_more",
			"value" => "",
			"std" => "...",
			"group" => __("Template", 'cayto'),
			"description" => __("Please, Enter excerpt more in this element. EX: ...", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Read More Text", 'cayto'),
			"param_name" => "readmore_text",
			"value" => "",
			"std" =>__("Read more",'cayto')	,
			"group" => __("Template", 'cayto'),
			"description" => __("Please, Enter read more text in this element. EX: Read more", 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("New line for read more text?", 'cayto'),
			"param_name" => "readmore_block",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"group" => __("Template", 'cayto'),
			"description" => __("Show read more text as new line", 'cayto')
		)
	)
));