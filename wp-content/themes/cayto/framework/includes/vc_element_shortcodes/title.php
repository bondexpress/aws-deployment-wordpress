<?php

add_action('init', 'title_integrateWithVC');

function title_integrateWithVC() {
    vc_map(array(
        "name" => __("Title", 'cayto'),
        "base" => "title",
        "class" => "title",
        "category" => __('Cayto', 'cayto'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
            array(
                "type" => "textfield",
                "holder" => "div",
                "class" => "",
                "heading" => __("Title", 'cayto'),
                "param_name" => "title",
                "value" => "",
                "description" => __("Content.", 'cayto')
            ),
			array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Font Size", 'cayto'),
                "param_name" => "font_size",
                "value" => "",
                "description" => __("Font Size.", 'cayto')
            ),
            array (
                "type" => "colorpicker",
                "heading" => __( 'Color', 'cayto' ),
                "param_name" => "color",
                "value" => '',
                "description" => __( 'Color', 'cayto' ),
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Template", 'cayto'),
                "param_name" => "title_tpl",
                "value" => array(
                    __("Title Separator ( position dependent by text-align)",'cayto') => "tpl3",
                    __("Title Underline style 1 (Single underline)",'cayto') => "tpl1",
                    __("Title Underline style 2",'cayto') => "tpl2",
                    __("None",'cayto') => "none",
                ),
                "std"=>"tpl3",
                "description" => __('Select template for title.', 'cayto')
            ),
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Align", 'cayto'),
                "param_name" => "el_align",
                "value" => array(
                    __("Left",'cayto') => "text-left",
                    __("Right",'cayto') => "text-right",
                    __("Center",'cayto') => "text-center"
                ),
                "std" => "text-center",
                "description" => __("Align", 'cayto')
            ),            
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", 'cayto'),
                "param_name" => "animation",
                "value" => array(
                    __("No",'cayto') => "",
                    __("Top to bottom",'cayto') => "top-to-bottom",
                    __("Bottom to top",'cayto') => "bottom-to-top",
                    __("Left to right",'cayto') => "left-to-right",
                    __("Right to left",'cayto') => "right-to-left",
                    __("Appear from center",'cayto') => "appear"
                ),
                "description" => __("Animation", 'cayto')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'cayto'),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", 'cayto')
            ),
        )
    ));
}
