<?php

add_action('init', 'tb_logo_integrateWithVC');

function tb_logo_integrateWithVC() {
    vc_map(array(
        "name" => __("Insert Logo", 'cayto'),
        "base" => "logo",
        "class" => "tb-logo",
        "category" => __('Cayto', 'cayto'),
        "icon" => "tb-icon-for-vc",
        "params" => array(
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Align", 'cayto'),
                "param_name" => "el_align",
                "value" => array(
                    __("Left",'cayto') => "text-left",
                    __("Right",'cayto') => "text-right",
                    __("Center",'cayto') => "text-center"
                ),
                "std" => "text-center",
                "description" => __("Align", 'cayto')
            ), 
            array (
                "type" => "colorpicker",
                "heading" => __( 'Color', 'cayto' ),
                "param_name" => "color",
                "value" => '',
                "std" => '#353535',
                "description" => __( 'Color', 'cayto' ),
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Font size", 'cayto'),
                "param_name" => "font_size",
                "value" => "",
                "std" => '55px',
                "description" => __("Font size.", 'cayto')
            ),          
            array(
                "type" => "dropdown",
                "class" => "",
                "heading" => __("Animation", 'cayto'),
                "param_name" => "animation",
                "value" => array(
                    __("No",'cayto') => "",
                    __("Top to bottom",'cayto') => "top-to-bottom",
                    __("Bottom to top",'cayto') => "bottom-to-top",
                    __("Left to right",'cayto') => "left-to-right",
                    __("Right to left",'cayto') => "right-to-left",
                    __("Appear from center",'cayto') => "appear"
                ),
                "description" => __("Animation", 'cayto')
            ),
            array(
                "type" => "textfield",
                "class" => "",
                "heading" => __("Extra Class", 'cayto'),
                "param_name" => "el_class",
                "value" => "",
                "description" => __("Extra Class.", 'cayto')
            ),
        )
    ));
}
