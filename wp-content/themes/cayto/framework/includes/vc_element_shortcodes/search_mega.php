<?php
vc_map ( array (
	"name" => 'Custom search mega',
	"base" => "search_mega",
	"icon" => "tb-icon-for-vc",
	"category" => __( 'Cayto', 'cayto' ), 
	'admin_enqueue_js' => array(TB_URI_PATH_FR.'/admin/assets/js/customvc.js'),
	"params" => array (
		array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Title", 'cayto'),
            "param_name" => "title",
            "value" => "",
            "description" => __("Option title.", 'cayto')
        ),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Type", 'cayto'),
			"param_name" => "type",
			"value" => array(
				__("Shop",'cayto') => "shop",
				__("Blog",'cayto') => "blog",
			),
			"description" => __('Select type in this element.', 'cayto')
		),
		array(
			"type" => "checkbox",
			"class" => "",
			"heading" => __("Hide Empty", 'cayto'),
			"param_name" => "hide_empty",
			"value" => array (
				__( "Yes, please", 'cayto' ) => true
			),
			"description" => __("Whether to hide terms not assigned to any posts.", 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		)
		
	)
));