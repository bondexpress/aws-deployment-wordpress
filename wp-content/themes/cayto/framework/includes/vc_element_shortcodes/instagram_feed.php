<?php
vc_map(array(
	"name" => __("Instagram Feed", 'cayto'),
	"base" => "instagram_feed",
	"category" => __('Cayto', 'cayto'),
	"icon" => "tb-icon-for-vc",
	"params" => array(
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Access Token", 'cayto'),
			"param_name" => "access_token",
			"value" => "",
			"description" => __("Please, enter <a href='http://instagram.pixelunion.net/' title'What is it?'>access token</a> in this element, demo: 2713900256.1677ed0.b25664f30f2c4ebe8af325eec77e9cf0.", 'cayto')
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Show Photos From", 'cayto'),
			"param_name" => "tpl",
			"value" => array(
				__("Popular", 'cayto')=> "popular",
				__("Hashtag", 'cayto')=> "tagged",
				__("Location", 'cayto') => "location",
				__("User ID", 'cayto')=> 'user'
			),
			"description" => __('Select template in this element.', 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("TagName/ LocationId/ UserId", 'cayto'),
			"param_name" => "item",
			"value" => "",
			"description" => __( "Please enter tagname/ locationId/ userId dependent of type 'Show Photos From'", 'cayto' )
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Limit", 'cayto'),
			"param_name" => "limit",
			"value" => "",
			"description" => __( "Maximum number of Images to add.", 'cayto' )
		),
		array(
			"type" => "dropdown",
			"class" => "",
			"heading" => __("Choose image size", 'cayto'),
			"param_name" => "thumb",
			"value" => array(
				__("Thumbnail(150x150)",'cayto') => "thumbnail",
				__("Low Resolution (320x320)",'cayto') => "low_resolution",
				__("Standar (612x612)",'cayto') => "standard_resolution"
			),
			"description" => __('Select thumbnail size in this element.', 'cayto')
		),
		array(
				"type" => "dropdown",
				"class" => "",
				"heading" => __("Columns", 'cayto'),
				"param_name" => "columns",
				"value" => array(
					__("6 Columns",'cayto') => "6",
					__("5 Columns",'cayto') => "5",
					__("4 Columns",'cayto') => "4",
					__("3 Columns",'cayto') => "3"
				),
				"description" => __('Select columns display in this element.', 'cayto')
		),
		array(
			"type" => "textfield",
			"class" => "",
			"heading" => __("Extra Class", 'cayto'),
			"param_name" => "el_class",
			"value" => "",
			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )
		),
	)
));
