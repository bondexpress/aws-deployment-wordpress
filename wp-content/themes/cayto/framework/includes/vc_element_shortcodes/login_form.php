<?php

vc_map(array(

	"name" => __("Login Form", 'cayto'),

	"base" => "login_form",

	"category" => __('Cayto', 'cayto'),

	"icon" => "tb-icon-for-vc",

	"params" => array(

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Link Facebook", 'cayto'),

			"param_name" => "link_facebook",

			"value" => "",

			"description" => __( "Enter Link Nextend Facebook Connect.", 'cayto' )

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Link Twitter", 'cayto'),

			"param_name" => "link_twitter",

			"value" => "",

			"description" => __( "Enter Link Nextend Twitter Connect.", 'cayto' )

		),

		array(

			"type" => "textfield",

			"class" => "",

			"heading" => __("Extra Class", 'cayto'),

			"param_name" => "el_class",

			"value" => "",

			"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", 'cayto' )

		),

	)

));

