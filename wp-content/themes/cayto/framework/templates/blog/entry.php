<?php
$tb_options = $GLOBALS['tb_options'];
$image_default = isset($tb_options['tb_blog_image_default']) ? $tb_options['tb_blog_image_default'] : '';
if(is_home()){
	$tb_show_post_title = 1;
	$tb_show_post_desc = 1;
	$tb_show_post_info = 1;
}elseif (is_single()) {
	$tb_blog_crop_image = isset($tb_options['tb_post_crop_image']) ? $tb_options['tb_post_crop_image'] : 0;
	$tb_blog_image_width = (int) isset($tb_options['tb_post_image_width']) ? $tb_options['tb_post_image_width'] : 800;
	$tb_blog_image_height = (int) isset($tb_options['tb_post_image_height']) ? $tb_options['tb_post_image_height'] : 400;
	$tb_show_post_title = (int) isset($tb_options['tb_post_show_post_title']) ? $tb_options['tb_post_show_post_title'] : 1;
	$tb_show_post_info = (int) isset($tb_options['tb_post_show_post_info']) ? $tb_options['tb_post_show_post_title'] : 1;
	$tb_post_show_social_share = (int) isset($tb_options['tb_post_show_social_share']) ? $tb_options['tb_post_show_social_share'] : 1;
	$tb_post_show_post_tags = (int) isset($tb_options['tb_post_show_post_tags']) ? $tb_options['tb_post_show_post_tags'] : 1;
	$tb_post_show_post_author = (int) isset($tb_options['tb_post_show_post_author']) ? $tb_options['tb_post_show_post_author'] : 1;
	$tb_show_post_desc = 1;
}else{
	$tb_blog_crop_image = isset($tb_options['tb_blog_crop_image']) ? $tb_options['tb_blog_crop_image'] : 0;
	$tb_blog_image_width = (int) isset($tb_options['tb_blog_image_width']) ? $tb_options['tb_blog_image_width'] : 600;
	$tb_blog_image_height = (int) isset($tb_options['tb_blog_image_height']) ? $tb_options['tb_blog_image_height'] : 400;
	$tb_show_post_title = (int) isset($tb_options['tb_blog_show_post_title']) ? $tb_options['tb_blog_show_post_title'] : 1;
	$tb_show_post_info = (int) isset($tb_options['tb_blog_show_post_info']) ? $tb_options['tb_blog_show_post_title'] : 1;
	$tb_show_post_desc = (int) isset($tb_options['tb_blog_show_post_desc']) ? $tb_options['tb_blog_show_post_desc'] : 1;
	$tb_blog_post_excerpt_leng = (int) isset($tb_options['tb_options[tb_blog_post_excerpt_leng]']) ? $tb_options['tb_options[tb_blog_post_excerpt_leng]'] : 100;
	$tb_blog_post_excerpt_more = isset($tb_options['tb_options[tb_blog_post_excerpt_more]']) ? $tb_options['tb_options[tb_blog_post_excerpt_more]'] : __('Read More','cayto');
	$tb_blog_post_excerpt_more = esc_attr( $tb_blog_post_excerpt_more );
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="tb-post-item">
	    <?php if (has_post_thumbnail()) { ?>
	        <div class="tb-thumb">
	            <!-- Get Thumb -->
	            <?php the_post_thumbnail('cayto-blog-large-hard-crop', array('class'=>'img-responsive')); ?>
				
	        </div>
	    <?php } ?>
		
		<div class="tb-content">
			<?php if($tb_show_post_info) { ?>
				<div class="tb-info">
					<?php echo tb_theme_info_bar_render(); ?>
				</div>
			<?php } ?>
			<?php if($tb_show_post_title) { ?>
				<a href="<?php the_permalink(); ?>"><h4 class="tb-title"><?php the_title(); ?></h4></a>
			<?php } ?>	
			<?php if($tb_show_post_desc) echo tb_theme_content_render(); ?>
		</div>
	
	</div>
</article>