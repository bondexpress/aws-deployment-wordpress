<?php
$tb_options = $GLOBALS['tb_options'];
$image_default = isset($tb_options['tb_portfolio_image_default']) ? $tb_options['tb_portfolio_image_default'] : '';
if(is_home()){
	$tb_show_post_title = 1;
	$tb_show_post_desc = 1;
	$tb_show_post_info = 1;
}elseif (is_single()) {
	$tb_portfolio_crop_image = isset($tb_options['tb_post_crop_image']) ? $tb_options['tb_post_crop_image'] : 0;
	$tb_portfolio_image_width = (int) isset($tb_options['tb_post_image_width']) ? $tb_options['tb_post_image_width'] : 800;
	$tb_portfolio_image_height = (int) isset($tb_options['tb_post_image_height']) ? $tb_options['tb_post_image_height'] : 400;
	$tb_show_post_title = (int) isset($tb_options['tb_post_show_post_title']) ? $tb_options['tb_post_show_post_title'] : 1;
	$tb_show_post_info = (int) isset($tb_options['tb_post_show_post_info']) ? $tb_options['tb_post_show_post_title'] : 1;
	$tb_post_show_social_share = (int) isset($tb_options['tb_post_show_social_share']) ? $tb_options['tb_post_show_social_share'] : 1;
	$tb_post_show_post_tags = (int) isset($tb_options['tb_post_show_post_tags']) ? $tb_options['tb_post_show_post_tags'] : 1;
	$tb_post_show_post_author = (int) isset($tb_options['tb_post_show_post_author']) ? $tb_options['tb_post_show_post_author'] : 1;
	$tb_show_post_desc = 1;
}else{
	$tb_portfolio_crop_image = isset($tb_options['tb_portfolio_crop_image']) ? $tb_options['tb_portfolio_crop_image'] : 0;
	$tb_portfolio_image_width = (int) isset($tb_options['tb_portfolio_image_width']) ? $tb_options['tb_portfolio_image_width'] : 600;
	$tb_portfolio_image_height = (int) isset($tb_options['tb_portfolio_image_height']) ? $tb_options['tb_portfolio_image_height'] : 400;
	$tb_show_post_title = (int) isset($tb_options['tb_portfolio_show_post_title']) ? $tb_options['tb_portfolio_show_post_title'] : 1;
	$tb_show_post_info = (int) isset($tb_options['tb_portfolio_show_post_info']) ? $tb_options['tb_portfolio_show_post_title'] : 1;
	$tb_show_post_desc = (int) isset($tb_options['tb_portfolio_show_post_desc']) ? $tb_options['tb_portfolio_show_post_desc'] : 1;
}
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	
    <?php if (has_post_thumbnail()) {
     ?>
        <div class="tb-blog-image text-center">
            <!-- Get Thumb -->
            <div class="tb-portfolio-flexslider flexslider">
            	<ul class="slides">
            		<li><?php the_post_thumbnail('full'); ?></li>
            		<?php $galleries = get_post_meta( get_the_ID(), 'tb_galleries', true);
            		if( $galleries ):
            			$galleries = explode( ',', $galleries );
            			foreach( $galleries as $src ):
            		?>
            			<li><img src="<?php echo esc_url( $src );?>" alt="<?php the_title();?>-gallery" class="img-responsive"></li>
            		<?php endforeach; endif; ?>
				</ul>
			</div>
        </div>
    <?php } ?>
	
	<div class="tb-content-block">
		<div class="row">
			<div class="col-sm-8 col-md-9">
				<?php if($tb_show_post_title) echo tb_theme_title_render(); ?>
				<?php if($tb_show_post_desc) echo tb_theme_content_render(); ?>
			</div>
			<div class="col-sm-4 col-md-3">
				<ul class="tb-portfolio-info list-unstyled">
					<?php
						$list_info = array('date'=>__('Date','cayto'),'client'=>__('Client','cayto'),'location'=>__('Location','cayto'));
						$k= 0;
						foreach( $list_info as $key=>$info ):
							$$key = get_post_meta( get_the_ID(), 'tb_'. $key, true );
							if( $k ===1 ):
							?>
							<li><span><?php _e('Categories','cayto'); ?>:</span> <?php the_terms( get_the_ID(), 'portfolio_category','', ', '); ?></li>
							<?php
							endif;
							if( !empty( $$key ) ):
						?>
							<li>
								<span><?php echo $info;?>:</span> <?php echo esc_attr( $$key ); ?>
							</li>
					<?php endif;$k++;endforeach; ?>
					<?php if($tb_show_post_info): ?>
						<li>
							<?php echo tb_theme_info_bar_render(); ?>
						</li>
					<?php endif;?>
					<?php if($tb_post_show_social_share): ?>
						<li>
						<?php echo tb_theme_social_share_post_render(); ?>
						</li>
					<?php endif; ?>
				</ul>
			</div>
		</div>
		
	</div>
	
</article>