<?php

class tb_Widget_Mini_Cart extends WC_Widget {

	/**
	 * Constructor
	 */
	function __construct() {
		$this->widget_cssclass    = 'woocommerce widget_mini_cart_wrap widget_mini_icon';
		$this->widget_description = __( "Display the user's Cart in the sidebar.", 'woocommerce' );
		$this->widget_id          = 'tb_widget_mini_cart';
		$this->widget_name        = __( 'TB Mini Search & Cart', 'woocommerce' );
		$this->settings           = array(
			'title'  => array(
				'type'  => 'text',
				'std'   => __( 'Cart', 'woocommerce' ),
				'label' => __( 'Title', 'woocommerce' )
			),
			'hide_search' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Hide search form', 'woocommerce' )
			),
			'hide_if_empty' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Hide if cart is empty', 'woocommerce' )
			),
			'full_search' => array(
				'type'  => 'checkbox',
				'std'   => 0,
				'label' => __( 'Full search form style, see header version 1', 'woocommerce' )
			)
		);

		parent::__construct();
	}

	/**
	 * widget function.
	 *
	 * @see WP_Widget
	 *
	 * @param array $args
	 * @param array $instance
	 */
	function widget( $args, $instance ) {
		global $post, $tb_options, $woocommerce;
		$tb_header = $tb_options["tb_header_layout"];
		$tb_custom_header = get_post_meta($post->ID, 'tb_header', true);
		if($post && isset($post->ID) && $tb_custom_header && $tb_custom_header  !='global' ){
			$tb_header = get_post_meta($post->ID, 'tb_header', true);
		}
		if ( apply_filters( 'woocommerce_widget_cart_is_hidden', is_cart() || is_checkout() ) ) {
			return;
		}

		$hide_if_empty = empty( $instance['hide_if_empty'] ) ? 0 : 1;
		$hide_search = empty( $instance['hide_search'] ) ? 0 : 1;
		$full_search = ( in_array( $tb_header, array("v1","v7") ) ) ? 1 : ( empty( $instance['full_search'] ) ? 0 : 1 );
		$hide_search = ( $tb_header == "v5" ) ? 1 : $hide_search;
		$hide_cart = empty( $instance['hide_cart'] ) ? 0 : 1;
		?>
		<div class="<?php if( $hide_search ) echo 'hidden ';?>widget_cart_search_wrap_item widget_searchform_content_wrap widget_mini_icon">
		<?php if( $tb_header == "v7" ): ?>
			<?php if( ! is_front_page() ): ?>
				<a href="javascript:void(0)" class="icon icon_search_wrap"><i class="fa fa-search search-icon"></i></a>
				<?php if( $full_search ){ ?>
				<div class="widget_searchform_content full_search">
					<div class="tb-container">
						<a href="#" id="tb-close-fullsearch" title="<?php _e('Close','cayto');?>">X</a>
				<?php }else{ ?>
				<div class="widget_searchform_content">
				<?php } ?>
					<form method="get" action="<?php echo esc_url( home_url( '/'  ) );?>">
						<input type="text" value="<?php echo get_search_query();?>" name="s" placeholder="<?php esc_html_e('Search...','cayto');?>" />
						<?php if( ! $full_search ): ?>
						<input type="submit" value="<?php echo esc_attr__( 'Search', 'woocommerce' )?>" />
						<?php endif; ?>
						<?php if(is_woocommerce()):?>
							<input type="hidden" name="post_type" value="product" />
						<?php endif;?>
					</form>
				<?php if( $full_search ){ ?>
					</div>
				<?php } ?>
				</div>
			<?php endif; ?>
			<!-- Menu Icon -->
			<a href="/my-account/" class="icon user_icon"><i class="fa fa-user"></i></a>
			<div class="tb-menu-account">
				<?php if(is_active_sidebar('tbtheme-woo-account-sidebar')) dynamic_sidebar('tbtheme-woo-account-sidebar');?>
			</div>
		<?php else: ?>
			<a href="javascript:void(0)" class="icon icon_search_wrap"><i class="fa fa-search search-icon"></i></a>
			<?php if( $full_search ){ ?>
			<div class="widget_searchform_content full_search">
				<div class="tb-container">
					<a href="#" id="tb-close-fullsearch" title="<?php _e('Close','cayto');?>">X</a>
			<?php }else{ ?>
			<div class="widget_searchform_content">
			<?php } ?>
				<form method="get" action="<?php echo esc_url( home_url( '/'  ) );?>">
					<input type="text" value="<?php echo get_search_query();?>" name="s" placeholder="<?php esc_html_e('Search','cayto');?>" />
					<?php if( ! $full_search ): ?>
					<input type="submit" value="<?php echo esc_attr__( 'Search', 'woocommerce' )?>" />
					<?php endif; ?>
					<?php if(is_woocommerce()):?>
						<input type="hidden" name="post_type" value="product" />
					<?php endif;?>
				</form>
			<?php if( $full_search ){ ?>
				</div>
			<?php } ?>
			</div>
		<?php endif; ?>
		</div>
		<div class="<?php if( $hide_cart ) echo 'hidden ';?>tb-menu-canvas-wrap">
			<?php
			if($tb_header == "v4" || $tb_header == "v6"){
				if($tb_header == "v4"){
					?>
						<!-- Menu Icon -->
						<div class="header-menu-item-icon">
							<a class="user_icon" href="/my-account/"><i class="fa fa-user"></i></a> <a class="tb-icon-setting" href="javascript:void();"><i class="fa fa-gear"></i></a> 
						</div>
						<div class="tb-menu-account">
							<?php if(is_active_sidebar('tbtheme-woo-account-sidebar')) dynamic_sidebar('tbtheme-woo-account-sidebar');?>
						</div>
						<div class="tb-menu-canvas">
							<?php if(is_active_sidebar('tbtheme-woo-canvas-sidebar')) dynamic_sidebar('tbtheme-woo-canvas-sidebar');?>
						</div>
					<?php 
				}
				if($tb_header == "v6"){
					?>
						<!-- Menu Icon -->
						<div class="header-menu-item-icon">
							<a class="user_icon" href="/my-account/"><i class="fa fa-user"></i></a>
						</div>
						<div class="tb-menu-account">
							<?php if(is_active_sidebar('tbtheme-woo-account-sidebar')) dynamic_sidebar('tbtheme-woo-account-sidebar');?>
						</div>

					<?php 
				}
				?>
				<?php
			}
			?>
		</div>
		<?php
		
		$this->widget_start( $args, $instance );
		if ( $hide_if_empty ) {

			echo '<div class="hide_cart_widget_if_empty">';
		}
		
		echo '<div class="header"><a class="icon icon_cart_wrap" href="'. $woocommerce->cart->get_cart_url() .'"><i class="fa fa-shopping-cart"></i><span class="cart_total" >0</span></a></div>';
		
		// Insert cart widget placeholder - code in woocommerce.js will update this on page load
		if( $tb_header == "v4") echo '<div class="wrap-shoping-libox"><div class="wrap-shoping-v4"><h3 class="tb-title-cart-h4">'. esc_html__("Mini Cart","cayto") .'<span>X</span></h3>';
		echo '<div class="shopping_cart_dropdown"><div class="widget_shopping_cart_content"></div></div>';
		if( $tb_header == "v4") echo '</div></div>';
		if ( $hide_if_empty ) {
			echo '</div>';
		}

		$this->widget_end( $args );
	}
}

add_filter('woocommerce_add_to_cart_fragments', 'woocommerce_icon_add_to_cart_fragment');
if(!function_exists('woocommerce_icon_add_to_cart_fragment')){
	function woocommerce_icon_add_to_cart_fragment( $fragments ) {
		global $woocommerce;
		ob_start();
		?>
		<div class="header"><a class="icon icon_cart_wrap" href="<?php echo $woocommerce->cart->get_cart_url(); ?>"><i class="fa fa-shopping-cart"></i><span class="cart_total" ><?php echo $woocommerce->cart->cart_contents_count; ?></span></a> <span class="total_cart">Cart: <?php echo $woocommerce->cart->get_cart_total(); ?></span></div>
		<?php
		$fragments['div.header'] = ob_get_clean();
		return $fragments;
	}
}

/**
 * Class tb_Widget_Mini_Cart
 */
function register_ct_widget_mini_cart() {
    register_widget('tb_Widget_Mini_Cart');
}
add_action('widgets_init', 'register_ct_widget_mini_cart');
