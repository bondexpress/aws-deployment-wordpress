<?php
require_once 'recent-posts-widget-with-thumbnails.php';
if (class_exists('Woocommerce')) {
	require_once 'mini-cart.php';
	require_once 'layered-nav-filters.php';
	require_once 'banner.php';
	require_once 'up-sells.php';
	//require_once 'price-filter.php';
	require_once 'news-letter.php';
}
