<?php
function tb_theme_autoCompileLess($inputFile, $outputFile) {
    require_once ( TB_ABS_PATH_FR . '/inc/lessc.inc.php' );
    $tb_options = $GLOBALS['tb_options'];
    $less = new lessc();
    $less->setFormatter("classic");
    $less->setPreserveComments(true);
	
	/*----------------------------VARIABLES THEME OPTIONS---------------------------*/
    $lists_extra_font = array();
	for( $i = 1; $i < 5; $i++ ){
        $font_name = 'google-font-'.$i;
        // var_dump( $tb_options );
        // var_dump( isset( $tb_options[ $font_name ] ) );
        if( isset( $tb_options[ $font_name ]['font-family'] ) ){
            $lists_extra_font[ 'tb_'. str_replace('-','_', $font_name ) ] = $tb_options[ $font_name ]['font-family'];
        }
    }
    if( isset( $tb_options['tb_body_font'] ) ){
        $lists_extra_font[ 'tb_body_font' ] = $tb_options[ 'tb_body_font' ]['font-family'];
    }
	/* Styling Options */
	$tb_primary_color = $tb_options['tb_primary_color'];
	/* Styling Options */
	
	/*----------------------------VARIABLES THEME OPTIONS---------------------------*/
	
	
    $variables = array(
		
		/* Primary Color */
		"tb_primary_color" => $tb_primary_color,
        // "tb_extra_font_1" => $tb_extra_font_1
    );
    $variables = wp_parse_args( $lists_extra_font, $variables );
	
	
    $less->setVariables($variables);
    $cacheFile = $inputFile.".cache";
    if (file_exists($cacheFile)) {
            $cache = unserialize(file_get_contents($cacheFile));
    } else {
            $cache = $inputFile;
    }
    $newCache = $less->cachedCompile($inputFile);
    if (!is_array($cache) || $newCache["updated"] > $cache["updated"]) {
            file_put_contents($cacheFile, serialize($newCache));
            file_put_contents($outputFile, $newCache['compiled']);
    }
}
function tb_addLessStyle() {
    try {
		$inputFile = TB_ABS_PATH.'/assets/css/less/style.less';
		$outputFile = TB_ABS_PATH.'/style.css';
		tb_theme_autoCompileLess($inputFile, $outputFile);
    } catch (Exception $e) {
        echo 'Caught exception: ', $e->getMessage(), "\n";
    }
}
add_action('wp_enqueue_scripts', 'tb_addLessStyle');
/* End less*/