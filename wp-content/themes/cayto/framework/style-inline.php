<?php
/*Header*/
if (!function_exists('tb_header_transparent_style_inline')) {
function tb_header_transparent_style_inline() {
	global $post;
	$postid = isset($post->ID)?$post->ID:0;
		
	/* Start Get value post meta */
		/* 1. Header menu v1 */
		$tb_header_menu_bg_color = get_post_meta($postid, 'tb_header_menu_bg_color', true);
		$tb_header_menu_color = get_post_meta($postid, 'tb_header_menu_color', true);
		$tb_header_menu_hover_color = get_post_meta($postid, 'tb_header_menu_hover_color', true);
		$tb_header_menu_bg_color_li_hover = get_post_meta($postid, 'tb_header_menu_bg_color_li_hover', true);
	/* End Get value post meta */
	

	
	/* Set value post meta on style inline */
	wp_enqueue_style('wp_custom_style', TB_URI_PATH . '/assets/css/wp_custom_style.css',array('style'));
	$custom_style = "
		.header-menu {
			background: {$tb_header_menu_bg_color};
		}
		.header-menu #nav > li > a, .header-menu .col-search-cart a.icon_search_wrap, .header-menu .col-search-cart a.icon_cart_wrap, .header-menu .col-search-cart .header-menu-item-icon a, .header-menu .col-search-cart  .tb-menu-control-mobi a{
			color: {$tb_header_menu_color};
		}
		.header-menu #nav > li:hover{
			background: {$tb_header_menu_bg_color_li_hover};
		}
		.header-menu #nav > li:hover a, .header-menu #nav li.current-menu-ancestor a, .header-menu #nav li.current-menu-parent a, .header-menu #nav li.current-menu-item a, .header-menu a.icon_search_wrap:hover, .header-menu a.icon_cart_wrap:hover, .header-menu .tb-menu-control-mobi a:hover {
			color: {$tb_header_menu_hover_color};
		}
	";
	wp_add_inline_style( 'wp_custom_style', $custom_style );
	
	
}
add_action( 'wp_enqueue_scripts', 'tb_header_transparent_style_inline' );
}

/* Fonts */
if (!function_exists('tb_add_style_inline')) {
	function tb_add_style_inline() {
		global $tb_options;
		$custom_style = null;
		if (isset($tb_options['custom_css_code']) && $tb_options['custom_css_code'])  {
			$tb_options['custom_css_code'] = wp_filter_nohtml_kses(esc_attr($tb_options['custom_css_code'])); 
			$custom_style .= "{$tb_options['custom_css_code']}";
		}
		wp_enqueue_style('wp_custom_style', TB_URI_PATH . '/assets/css/wp_custom_style.css',array('style'));
		wp_add_inline_style( 'wp_custom_style', $custom_style );
		/*End Font*/
	}
	add_action( 'wp_enqueue_scripts', 'tb_add_style_inline' );
}