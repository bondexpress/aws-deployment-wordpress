<?php
	global $post;
	$tb_options = $GLOBALS['tb_options'];
	$tb_header_fixed = isset($post->ID) ? get_post_meta($post->ID, 'tb_header_fixed', true):'';
	$tb_custom_menu =  isset($post->ID) ? get_post_meta($post->ID, 'tb_custom_menu', true):'';
	$tb_display_widget_top = isset($post->ID) ? get_post_meta($post->ID, 'tb_display_widget_top', true): '';
	if(is_single() || is_archive() || is_category() ||  is_search()) {
		$tb_header_fixed = $tb_display_widget_top = 1;
	}
	$tb_header_class = array();
    if($tb_header_fixed) $tb_header_class[] = 'tb-header-fixed';
    if($tb_options['tb_stick_header']) $tb_header_class[] = 'tb-header-stick';
?>
<div class="tb-header-wrap tb-header-v2 <?php echo esc_attr(implode(' ', $tb_header_class)); ?>">
	<!-- Start Header Sidebar -->
	<?php if($tb_display_widget_top) { ?>
		<div class="tb-header-top">
			<div class="container">
				<div class="row">
					<!-- Start Sidebar Top Left -->
					<?php if(is_active_sidebar( 'tbtheme-header-top-widget-1' )){ ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="tb-sidebar tb-sidebar-left"><?php dynamic_sidebar("tbtheme-header-top-widget-1");?></div>
						</div>
					<?php } ?>
					<!-- End Sidebar Top Left -->
					<!-- Start Sidebar Top Right -->
					<?php if(is_active_sidebar( 'tbtheme-header-top-widget-2' )){ ?>
						<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
							<div class="tb-sidebar tb-sidebar-right"><?php dynamic_sidebar("tbtheme-header-top-widget-2");?></div>
						</div>
					<?php } ?>
					<!-- End Sidebar Top Right -->
				</div>
			</div>
		</div>
	<?php } ?>
	<!-- End Header Sidebar -->
	<div class="tb-logo-v2">
		<div class="tb-logo">
			<a href="<?php echo esc_url(home_url()); ?>">
				<?php tb_theme_logo(); ?>
			</a>
		</div>
	</div>
	<!-- Start Header Menu -->
	<div class="tb-header-menu">
		<div class="container">
			<div class="tb-header-menu-inner">
				<div class="col-logo">
					<div class="tb-logo">
						<a href="<?php echo esc_url(home_url()); ?>">
							<?php tb_theme_logo(); ?>
						</a>
					</div>
				</div>
				<div class="col-menu">
					<div class="tb-menu">
						<?php
						$arr = array(
							'theme_location' => 'main_navigation',
							'menu_id' => 'nav',
							'container_class' => 'tb-menu-list',
							'menu_class'      => 'tb-menu-list-inner',
							'echo'            => true,
							'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
							'depth'           => 0,
						);
						if($tb_custom_menu){
							$arr['menu'] = $tb_custom_menu;
						}
						if (has_nav_menu('main_navigation')) {
							wp_nav_menu( $arr );
						}else{ ?>
						<div class="menu-list-default">
							<?php wp_page_menu();?>
						</div>    
						<?php } ?>
						
						<?php if(is_active_sidebar('tbtheme-woo-sidebar')) { ?> 
							<div class="tb-menu-sidebar">
								<?php dynamic_sidebar('tbtheme-woo-sidebar');?>
							</div>
						<?php } ?>
						
						<div class="tb-menu-control-mobi">
							<a href="javascript:void(0)"><i class="fa fa-bars"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- End Header Menu -->