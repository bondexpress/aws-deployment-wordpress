<?php
	global $post;
	$tb_options = $GLOBALS['tb_options'];
	$tb_header_fixed = isset($post->ID) ? get_post_meta($post->ID, 'tb_header_fixed', true):'';
	$tb_custom_menu =  isset($post->ID) ? get_post_meta($post->ID, 'tb_custom_menu', true):'';
	$tb_display_widget_top = isset($post->ID) ? get_post_meta($post->ID, 'tb_display_widget_top', true): '';
	if(is_single() || is_archive() || is_category() ||  is_search()) {
		$tb_header_fixed = $tb_display_widget_top = 1;
	}
	$tb_header_class = array();
    if($tb_header_fixed) $tb_header_class[] = 'tb-header-fixed';
    if($tb_options['tb_stick_header']) $tb_header_class[] = 'tb-header-stick';
?>
<div class="clearfix">
<div class="tb-header-wrap ct-inc-megamenu tb-header-v4 <?php echo esc_attr(implode(' ', $tb_header_class)); ?>">
	<!-- Start Header Sidebar -->
	<div class="mobile-header">
		<div class="tb-logo">
			<a href="<?php echo esc_url(home_url()); ?>">
				<?php tb_theme_logo(); ?>
			</a>
		</div>
		<a id="mobile-bar-v4" href="javascript:;" class="fa fa-bars"></a>
	</div>
	<div class="mobile-leftbar text-center" tabindex="0">
		<?php if($tb_display_widget_top) { ?>
			<div class="tb-header-top">
				<!-- Start Sidebar Top Left -->
				<?php if(is_active_sidebar( 'tbtheme-header-top-widget-1' )){ ?>
					<div class="tb-sidebar clearfix"><?php dynamic_sidebar("tbtheme-header-top-widget-1");?></div>
				<?php } ?>
				<!-- End Sidebar Top Left -->
				<!-- Start Sidebar Top Right -->
				<?php if(is_active_sidebar( 'tbtheme-header-top-widget-2' )){ ?>
					<div class="tb-sidebar"><?php dynamic_sidebar("tbtheme-header-top-widget-2");?></div>
				<?php } ?>
				<!-- End Sidebar Top Right -->
			</div>
		<?php } ?>
		<div class="tb-logo">
			<a href="<?php echo esc_url(home_url()); ?>">
				<?php tb_theme_logo(); ?>
			</a>
		</div>
		<a class="br-button hiden-md hidden-lg fa fa-close" href="javascript:;"></a>
		<?php
		$arr = array(
			'theme_location'  => 'main_navigation',
			'menu'            => '',
			'container'       => '',
			'container_class' => '',
			'container_id'    => '',
			'menu_class'      => 'mobile-nav',
			'menu_id'         => '',
			'echo'            => true,
			'fallback_cb'     => 'wp_page_menu',
			'before'          => '',
			'after'           => '',
			'link_before'     => '',
			'link_after'      => '',
			'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
			'depth'           => 0,
			'walker'          => ''
		);
		if (has_nav_menu('main_navigation')) {
			wp_nav_menu( $arr );
		} else {
			wp_page_menu();
		}
		?>
		<!-- Start Sidebar Top Right -->
		
		<?php if(is_active_sidebar('tbtheme-woo-sidebar')) { ?> 
			<div class="tb-menu-sidebar">
				<?php dynamic_sidebar('tbtheme-woo-sidebar');?>
			</div>
		<?php } ?>


		<?php if (is_active_sidebar('tbtheme-footer-sidebar') ) { ?>
			<div class="bottom-section tb_footer">
				<?php dynamic_sidebar('tbtheme-footer-sidebar'); ?>
			</div>
		<?php } ?>
	</div>
</div>
<!-- End Header Menu -->
<div class="col-lg-offset-27 col-lg-93">