<?php
    /**
     * ReduxFramework Theme Config File
     * For full documentation, please visit: https://docs.reduxframework.com
     * */

    if ( ! class_exists( 'Redux_Framework_theme_config' ) ) {

        class Redux_Framework_theme_config {

            public $args = array();
            public $sections = array();
            public $theme;
            public $ReduxFramework;

            public function __construct() {

                if ( ! class_exists( 'ReduxFramework' ) ) {
                    return;
                }

                // This is needed. Bah WordPress bugs.  ;)
                if ( true == Redux_Helpers::isTheme( __FILE__ ) ) {
                    $this->initSettings();
                } else {
                    add_action( 'plugins_loaded', array( $this, 'initSettings' ), 10 );
                }
				add_action( 'admin_enqueue_scripts', array( $this, 'tbtheme_add_scripts' ));

            }
			public function tbtheme_add_scripts(){
				wp_enqueue_script( 'action', TB_URI_PATH_ADMIN.'/assets/js/action.js', false );
				wp_enqueue_style( 'style_admin', TB_URI_PATH_ADMIN.'/assets/css/style_admin.css', false );
			}
            public function initSettings() {

                // Just for demo purposes. Not needed per say.
                $this->theme = wp_get_theme();

                // Set the default arguments
                $this->setArguments();

                // Set a few help tabs so you can see how it's done
                //$this->setHelpTabs();

                // Create the sections and fields
                $this->setSections();

                if ( ! isset( $this->args['opt_name'] ) ) { // No errors please
                    return;
                }

                // If Redux is running as a plugin, this will remove the demo notice and links
                //add_action( 'redux/loaded', array( $this, 'remove_demo' ) );

                // Function to test the compiler hook and demo CSS output.
                // Above 10 is a priority, but 2 in necessary to include the dynamically generated CSS to be sent to the function.
                //add_filter('redux/options/'.$this->args['opt_name'].'/compiler', array( $this, 'compiler_action' ), 10, 3);

                // Change the arguments after they've been declared, but before the panel is created
                //add_filter('redux/options/'.$this->args['opt_name'].'/args', array( $this, 'change_arguments' ) );

                // Change the default value of a field after it's been set, but before it's been useds
                //add_filter('redux/options/'.$this->args['opt_name'].'/defaults', array( $this,'change_defaults' ) );

                // Dynamically add a section. Can be also used to modify sections/fields
                //add_filter('redux/options/' . $this->args['opt_name'] . '/sections', array($this, 'dynamic_section'));

                $this->ReduxFramework = new ReduxFramework( $this->sections, $this->args );
            }

            /**
             * This is a test function that will let you see when the compiler hook occurs.
             * It only runs if a field    set with compiler=>true is changed.
             * */
            function compiler_action( $options, $css, $changed_values ) {
                echo '<h1>The compiler hook has run!</h1>';
                echo "<pre>";
                print_r( $changed_values ); // Values that have changed since the last save
                echo "</pre>";
            }

            /**
             * Custom function for filtering the sections array. Good for child themes to override or add to the sections.
             * Simply include this function in the child themes functions.php file.
             * NOTE: the defined constants for URLs, and directories will NOT be available at this point in a child theme,
             * so you must use get_template_directory_uri() if you want to use any of the built in icons
             * */
            function dynamic_section( $sections ) {
                //$sections = array();
                $sections[] = array(
                    'title'  => __( 'Section via hook', 'cayto' ),
                    'desc'   => __( '<p class="description">This is a section created by adding a filter to the sections array. Can be used by child themes to add/remove sections from the options.</p>', 'cayto' ),
                    'icon'   => 'el-icon-paper-clip',
                    // Leave this as a blank section, no options just some intro text set above.
                    'fields' => array()
                );

                return $sections;
            }

            /**
             * Filter hook for filtering the args. Good for child themes to override or add to the args array. Can also be used in other functions.
             * */
            function change_arguments( $args ) {
                //$args['dev_mode'] = true;

                return $args;
            }

            /**
             * Filter hook for filtering the default value of any given field. Very useful in development mode.
             * */
            function change_defaults( $defaults ) {
                $defaults['str_replace'] = 'Testing filter hook!';

                return $defaults;
            }

            // Remove the demo link and the notice of integrated demo from the redux-framework plugin
            function remove_demo() {

                // Used to hide the demo mode link from the plugin page. Only used when Redux is a plugin.
                if ( class_exists( 'ReduxFrameworkPlugin' ) ) {
                    remove_filter( 'plugin_row_meta', array(
                        ReduxFrameworkPlugin::instance(),
                        'plugin_metalinks'
                    ), null, 2 );

                    // Used to hide the activation notice informing users of the demo panel. Only used when Redux is a plugin.
                    remove_action( 'admin_notices', array( ReduxFrameworkPlugin::instance(), 'admin_notices' ) );
                }
            }

            public function setSections() {

                /**
                 * Used within different fields. Simply examples. Search for ACTUAL DECLARATION for field examples
                 * */
                // Background Patterns Reader
                $sample_patterns_path = ReduxFramework::$_dir . '../sample/patterns/';
                $sample_patterns_url  = ReduxFramework::$_url . '../sample/patterns/';
                $sample_patterns      = array();

                if ( is_dir( $sample_patterns_path ) ) :

                    if ( $sample_patterns_dir = opendir( $sample_patterns_path ) ) :
                        $sample_patterns = array();

                        while ( ( $sample_patterns_file = readdir( $sample_patterns_dir ) ) !== false ) {

                            if ( stristr( $sample_patterns_file, '.png' ) !== false || stristr( $sample_patterns_file, '.jpg' ) !== false ) {
                                $name              = explode( '.', $sample_patterns_file );
                                $name              = str_replace( '.' . end( $name ), '', $sample_patterns_file );
                                $sample_patterns[] = array(
                                    'alt' => $name,
                                    'img' => $sample_patterns_url . $sample_patterns_file
                                );
                            }
                        }
                    endif;
                endif;

                ob_start();

                $ct          = wp_get_theme();
                $this->theme = $ct;
                $item_name   = $this->theme->get( 'Name' );
                $tags        = $this->theme->Tags;
                $screenshot  = $this->theme->get_screenshot();
                $class       = $screenshot ? 'has-screenshot' : '';

                $customize_title = sprintf( __( 'Customize &#8220;%s&#8221;', 'cayto' ), $this->theme->display( 'Name' ) );

                ?>
                <div id="current-theme" class="<?php echo esc_attr( $class ); ?>">
                    <?php if ( $screenshot ) : ?>
                        <?php if ( current_user_can( 'edit_theme_options' ) ) : ?>
                            <a href="<?php echo wp_customize_url(); ?>" class="load-customize hide-if-no-customize"
                               title="<?php echo esc_attr( $customize_title ); ?>">
                                <img src="<?php echo esc_url( $screenshot ); ?>"
                                     alt="<?php esc_attr_e( 'Current theme preview', 'cayto' ); ?>"/>
                            </a>
                        <?php endif; ?>
                        <img class="hide-if-customize" src="<?php echo esc_url( $screenshot ); ?>"
                             alt="<?php esc_attr_e( 'Current theme preview', 'cayto' ); ?>"/>
                    <?php endif; ?>

                    <h4><?php echo esc_attr($this->theme->display( 'Name' )); ?></h4>

                    <div>
                        <ul class="theme-info">
                            <li><?php printf( __( 'By %s', 'cayto' ), $this->theme->display( 'Author' ) ); ?></li>
                            <li><?php printf( __( 'Version %s', 'cayto' ), $this->theme->display( 'Version' ) ); ?></li>
                            <li><?php echo '<strong>' . __( 'Tags', 'cayto' ) . ':</strong> '; ?><?php printf( $this->theme->display( 'Tags' ) ); ?></li>
                        </ul>
                        <p class="theme-description"><?php echo esc_attr($this->theme->display( 'Description' )); ?></p>
                        <?php
                            if ( $this->theme->parent() ) {
                                printf( ' <p class="howto">' . __( 'This <a href="%1$s">child theme</a> requires its parent theme, %2$s.', 'cayto' ) . '</p>', __( 'http://codex.wordpress.org/Child_Themes', 'cayto' ), $this->theme->parent()->display( 'Name' ) );
                            }
                        ?>

                    </div>
                </div>

                <?php
                $item_info = ob_get_contents();

                ob_end_clean();

                $sampleHTML = '';
                if ( file_exists( dirname( __FILE__ ) . '/info-html.html' ) ) {
                    Redux_Functions::initWpFilesystem();

                    global $wp_filesystem;

                    $sampleHTML = $wp_filesystem->get_contents( dirname( __FILE__ ) . '/info-html.html' );
                }
				
				$of_options_fontsize = array("8px" => "8px", "9px" => "9px", "10px" => "10px", "11px" => "11px", "12px" => "12px", "13px" => "13px", "14px" => "14px", "15px" => "15px", "16px" => "16px", "17px" => "17px", "18px" => "18px", "19px" => "19px", "20px" => "20px", "21px" => "21px", "22px" => "22px", "23px" => "23px", "24px" => "24px", "25px" => "25px", "26px" => "26px", "27px" => "27px", "28px" => "28px", "29px" => "29px", "30px" => "30px", "31px" => "31px", "32px" => "32px", "33px" => "33px", "34px" => "34px", "35px" => "35px", "36px" => "36px", "37px" => "37px", "38px" => "38px", "39px" => "39px", "40px" => "40px");
				$of_options_fontweight = array("100" => "100", "200" => "200", "300" => "300", "400" => "400", "500" => "500", "600" => "600", "700" => "700");
				$of_options_font = array("1" => "Google Font", "2" => "Standard Font", "3" => "Custom Font");
				//Google font API
				$of_options_google_font = array();
				if (is_admin()) {
					$results = '';
					//$whitelist = array('127.0.0.1','::1');
					//if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
						$results = wp_remote_get('https://www.googleapis.com/webfonts/v1/webfonts?sort=alpha&key=AIzaSyDnf-ujK_DUCihfvzqdlBokan6zbnrJbi0');
						if (!is_wp_error($results)) {
								$results = json_decode($results['body']);
								if(isset($results->items)){
									foreach ($results->items as $font) {
										$of_options_google_font[$font->family] = $font->family;
									}
								}
						}
					//}
				}
				//Standard Fonts
				$of_options_standard_fonts = array(
					'Arial, Helvetica, sans-serif' => 'Arial, Helvetica, sans-serif',
					"'Arial Black', Gadget, sans-serif" => "'Arial Black', Gadget, sans-serif",
					"'Bookman Old Style', serif" => "'Bookman Old Style', serif",
					"'Comic Sans MS', cursive" => "'Comic Sans MS', cursive",
					"Courier, monospace" => "Courier, monospace",
					"Garamond, serif" => "Garamond, serif",
					"Georgia, serif" => "Georgia, serif",
					"Impact, Charcoal, sans-serif" => "Impact, Charcoal, sans-serif",
					"'Lucida Console', Monaco, monospace" => "'Lucida Console', Monaco, monospace",
					"'Lucida Sans Unicode', 'Lucida Grande', sans-serif" => "'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
					"'MS Sans Serif', Geneva, sans-serif" => "'MS Sans Serif', Geneva, sans-serif",
					"'MS Serif', 'New York', sans-serif" => "'MS Serif', 'New York', sans-serif",
					"'Palatino Linotype', 'Book Antiqua', Palatino, serif" => "'Palatino Linotype', 'Book Antiqua', Palatino, serif",
					"Tahoma, Geneva, sans-serif" => "Tahoma, Geneva, sans-serif",
					"'Times New Roman', Times, serif" => "'Times New Roman', Times, serif",
					"'Trebuchet MS', Helvetica, sans-serif" => "'Trebuchet MS', Helvetica, sans-serif",
					"Verdana, Geneva, sans-serif" => "Verdana, Geneva, sans-serif"
				);
				// Custom Font
				$fonts = array();
				$of_options_custom_fonts = array();
				$font_path = get_template_directory() . "/fonts";
				if (!$handle = opendir($font_path)) {
					$fonts = array();
				} else {
					while (false !== ($file = readdir($handle))) {
						if (strpos($file, ".ttf") !== false ||
							strpos($file, ".eot") !== false ||
							strpos($file, ".svg") !== false ||
							strpos($file, ".woff") !== false
						) {
							$fonts[] = $file;
						}
					}
				}
				closedir($handle);

				foreach ($fonts as $font) {
					$font_name = str_replace(array('.ttf', '.eot', '.svg', '.woff'), '', $font);
					$of_options_custom_fonts[$font_name] = $font_name;
				}
				/* remove dup item */
				$of_options_custom_fonts = array_unique($of_options_custom_fonts);

                //lists page
                $lists_page = array();
                $args_page = array(
                    'sort_order' => 'asc',
                    'sort_column' => 'post_title',
                    'hierarchical' => 1,
                    'exclude' => '',
                    'include' => '',
                    'meta_key' => '',
                    'meta_value' => '',
                    'authors' => '',
                    'child_of' => 0,
                    'parent' => -1,
                    'exclude_tree' => '',
                    'number' => '',
                    'offset' => 0,
                    'post_type' => 'page',
                    'post_status' => 'publish'
                );
                $pages = get_pages( $args_page );

                foreach( $pages as $p ){
                    $lists_page[ $p->ID ] = esc_attr( $p->post_title );
                }
				
				/*General Setting*/
				$this->sections[] = array(
                    'title'  => __( 'General Setting', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-cogs',
                    'fields' => array(
						array(
                            'id'       => 'tb_less',
                            'type'     => 'switch',
                            'title'    => __( 'Less Design', 'cayto' ),
                            'subtitle' => __( 'Use the less design features.', 'cayto' ),
							'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_box_style',
                            'type'     => 'switch',
                            'title'    => __( 'Show Box Style', 'cayto' ),
                            'subtitle' => __( 'Show Box style options', 'cayto' ),
                            'default'  => false,
                        ),
                        array( 
                            'id'       => 'tb_body_layout',
                            'type'     => 'select',
                                'title'    => __('Choose Body Layout', 'cayto'),
                                'subtitle' => __('Select body layout.', 'cayto'),
                                'options'  => array(
                                    'wide'=>__('Wide', 'cayto'),
                                    'boxed'=>__('Boxed', 'cayto')
                                ),
                                'default'  => 'wide'
                        ),
						array(
							'id'       => 'tb_background',
							'type'     => 'background',
							'title'    => __('Body Background', 'cayto'),
							'subtitle' => __('Body background with image, color, etc.', 'cayto'),
							'default'  => array(
								'background-color' => '#ffffff',
							),
							'output' => array('body'),
						),
					)
					
				);
				/*Logo*/
				$this->sections[] = array(
                    'title'  => __( 'Logo', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-viadeo',
                    'fields' => array(
						array(
							'id'       => 'tb_favicon_image',
							'type'     => 'media',
							'url'      => true,
							'title'    => __('Favicon Image', 'cayto'),
							'subtitle' => __('Select an image file for your favicon.', 'cayto'),
							'default'  => array(
								'url'	=> TB_URI_PATH.'/favicon.ico'
							),
						),
                        array(
                            'id'       => 'tb_logo_text',
                            'type'     => 'text',
                            'title'    => __('Logo Text', 'cayto'),
                            'subtitle' => __('Enter Logo text', 'cayto'),
                            'default'  => __( 'Cay<span class="primary_color">.</span>To<span class="logo-text-v4 hidden">Ecommerce</span>', 'cayto')
                        ),
						array(
							'id'       => 'tb_logo_image',
							'type'     => 'media',
							'url'      => true,
							'title'    => __('Logo Image', 'cayto'),
							'subtitle' => __('Select an image file for your logo instead of text.', 'cayto'),
							'default'  => array(
								'url'	=> TB_URI_PATH.'/assets/images/logo.png'
							),
						)
					)
				);
				
				/* Header Options */
				$this->sections[] = array(
                    'title'  => __( 'Header', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-file-edit',
                    'fields' => array(
						array( 
							'id'       => 'tb_header_layout',
							'type'     => 'image_select',
							'title'    => __('Header Layout', 'cayto'),
							'subtitle' => __('Select header layout in your site.', 'cayto'),
							'options'  => array(
                                'v1'    => array(
                                    'alt'   => 'Header 1',
                                    'img'   => TB_URI_PATH.'/assets/images/headers/header-v1.jpg'
                                ),
								'v2'	=> array(
									'alt'   => 'Header 2',
									'img'   => TB_URI_PATH.'/assets/images/headers/header-v2.jpg'
								),
								'v3'	=> array(
									'alt'   => 'Header 3',
									'img'   => TB_URI_PATH.'/assets/images/headers/header-v3.jpg'
								),
								'v4'	=> array(
									'alt'   => 'Header 4',
									'img'   => TB_URI_PATH.'/assets/images/headers/header-v4.jpg'
								),
								'v5'	=> array(
									'alt'   => 'Header 5',
									'img'   => TB_URI_PATH.'/assets/images/headers/header-v5.jpg'
								),
								'v6'	=> array(
									'alt'   => 'Header 6',
									'img'   => TB_URI_PATH.'/assets/images/headers/header-v6.jpg'
								),
                                'v7'    => array(
                                    'alt'   => 'Header 7',
                                    'img'   => TB_URI_PATH.'/assets/images/headers/header-v7.jpg'
                                )
							),
							'default' => 'v1'
						),
						/* Header Sticky */
						array(
                            'id'       => 'tb_stick_header',
                            'type'     => 'switch',
                            'title'    => __( 'Sticky Header', 'cayto' ),
                            'subtitle' => __( 'Enable a fixed header when scrolling.', 'cayto' ),
							'default'  => true,
                        ),
						/* Header Sticky */
					)
				);
				/* Header Options */
				
				/*Main Menu*/
				$this->sections[] = array(
                    'title'  => __( 'Main Menu', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-list',
                    'fields' => array(
						array(
							'id'          => 'tb_menu_font_size_firts_level',
							'type'        => 'typography', 
							'title'       => __('Typography', 'cayto'),
							'color'      => false, 
							'font-weight' => false, 
							'subsets' => false,
							'font-backup' => false,
							'line-height' => false,
							'subtitle' => __('Typography option with firts level item in menu. Default: 14px, ex: 14px.', 'cayto'),
							'default'     => array(
								'font-size'   => '15px',
								'font-family' => 'Monda'
							),
							'output' => array('#nav > li > a, a.icon_search_wrap, a.icon_cart_wrap, .header-menu-item-icon a'),
						),
						array(
							'id'          => 'tb_menu_font_size_sub_level',
							'type'        => 'typography', 
							'title'       => __('Typography', 'cayto'),
							'color'      => false, 
							'font-weight' => false, 
							'subsets' => false,
							'font-backup' => false,
							'subtitle' => __('Typography option with sub level item in menu.', 'cayto'),
							'default'     => array(
								'font-size'   => '13px',
								'line-height' => '13px',
							),
							'output' => array('#nav > li > ul li a,'),
						),
						array(
							'id' => 'tb_menu_padding',
							'title' => 'Menu Padding',
							'subtitle' => __('Please, Enter padding For Menu.', 'cayto'),
							'type' => 'spacing',
							'units' => array('px'),
							'output' => array('#nav > li > a'),
							'default' => array(
								'padding-top'     => '0', 
								'padding-right'   => '20px', 
								'padding-bottom'  => '0', 
								'padding-left'    => '20px',
								'units'          => 'px', 
							)
						),
					)
					
				);
/*Footer*/
                $this->sections[] = array(
                    'title'  => __( '404 Page', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el el-error',
                    'fields' => array(
                        array(
                            'id'       => 'tb_error404_page_id',
                            'type'     => 'select',
                            'title'    => __('Page 404 Template', 'cayto'),
                            'subtitle' => __('Select 404 page.', 'cayto'),
                            'options'  => $lists_page
                        ),
                        array(
                            'id'       => 'tb_error404_display_header',
                            'type'     => 'switch',
                            'title'    => __( 'Display Header', 'cayto' ),
                            'subtitle' => __( 'Display header.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_error404_display_top_sidebar',
                            'type'     => 'switch',
                            'title'    => __( 'Display Top Sidebar', 'cayto' ),
                            'subtitle' => __( 'Display Top Sidebar.', 'cayto' ),
                            'default'  => false
                        ),
                        array(
                            'id'       => 'tb_error404_display_title',
                            'type'     => 'switch',
                            'title'    => __( 'Display Page title', 'cayto' ),
                            'subtitle' => __( 'Display Page title.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_error404_display_footer',
                            'type'     => 'switch',
                            'title'    => __( 'Display Footer', 'cayto' ),
                            'subtitle' => __( 'Display Footer.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_error404_bg',
                            'type'     => 'background',
                            'title'    => __('404 Page Background', 'cayto'),
                            'subtitle' => __('404 page background with image, color, etc.', 'cayto'),
                            'default'  => array(
                                'background-image' => TB_URI_PATH.'/assets/images/404-page/background.jpg',
                            ),
                            'output' => array('.tb-error404-wrap'),
                        )
                        
                    )
                    
                );
				/*Footer*/
				$this->sections[] = array(
                    'title'  => __( 'Footer', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-file-edit',
                    'fields' => array(
						array(
                            'id'       => 'tb_display_footer',
                            'type'     => 'switch',
                            'title'    => __( 'Display Footer', 'cayto' ),
                            'subtitle' => __( 'Display footer.', 'cayto' ),
							'default'  => true,
                        ),
                        array( 
                            'id'       => 'tb_footer_layout',
                            'type'     => 'image_select',
                            'title'    => __('Footer Layout', 'cayto'),
                            'subtitle' => __('Select footer layout in your site.', 'cayto'),
                            'options'  => array(
                                'v1'    => array(
                                    'alt'   => 'Footer 1',
                                    'img'   => TB_URI_PATH.'/assets/images/footers/footer-v1.jpg'
                                ),
                                'v2'    => array(
                                    'alt'   => 'Footer 2',
                                    'img'   => TB_URI_PATH.'/assets/images/footers/footer-v2.jpg'
                                ),
                                'v3'    => array(
                                    'alt'   => 'Footer 3',
                                    'img'   => TB_URI_PATH.'/assets/images/footers/footer-v3.jpg'
                                )
                            ),
                            'default' => 'v1'
                        ),
						array(
							'id' => 'tb_footer_margin',
							'title' => 'Footer Margin',
							'subtitle' => __('Please, Enter margin of Footer.', 'cayto'),
							'type' => 'spacing',
							'mode' => 'margin',
							'units' => array('px'),
							'output' => array('.tb_footer'),
							'default' => array(
								'margin-top'     => '0', 
								'margin-right'   => '0', 
								'margin-bottom'  => '0', 
								'margin-left'    => '0',
								'units'          => 'px', 
							)
						),
						array(
							'id' => 'tb_footer_padding',
							'title' => 'Footer Padding',
							'subtitle' => __('Please, Enter padding of Footer.', 'cayto'),
							'type' => 'spacing',
							'units' => array('px'),
							'output' => array('.tb_footer'),
							'default' => array(
								'padding-top'     => '0', 
								'padding-right'   => '0', 
								'padding-bottom'  => '0', 
								'padding-left'    => '0',
								'units'          => 'px', 
							)
						),
						
					)
					
				);
				$this->sections[] = array(
                    'title'  => __( 'Footer Top', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-file-edit',
					'subsection' => true,
                    'fields' => array(
						array(
							'id'       => 'tb_footer_top_bg',
							'type'     => 'background',
							'title'    => __('Footer Background', 'cayto'),
							'subtitle' => __('Footer background with image, color, etc.', 'cayto'),
							'default'  => array(
								'background-color' => '#252525',
							),
							'output' => array('.tb_footer .footer-top'),
						),
						array(
							'id' => 'tb_footer_top_margin',
							'title' => 'Footer Top Margin',
							'subtitle' => __('Please, Enter margin of Footer Top.', 'cayto'),
							'type' => 'spacing',
							'mode' => 'margin',
							'units' => array('px'),
							'output'  => array('.tb_footer .footer-top'),
							'default' => array(
								'margin-top'     => '0', 
								'margin-right'   => '0', 
								'margin-bottom'  => '0', 
								'margin-left'    => '0',
								'units'          => 'px', 
							)
						),
						array(
							'id' => 'tb_footer_top_padding',
							'title' => 'Footer Top Padding',
							'subtitle' => __('Please, Enter padding of Footer Top.', 'cayto'),
							'type' => 'spacing',
							'units' => array('px'),
							'output'  => array('.tb_footer .footer-top'),
							'default' => array(
								'padding-top'     => '55px', 
								'padding-right'   => '0', 
								'padding-bottom'  => '40px', 
								'padding-left'    => '0',
								'units'          => 'px', 
							)
						),
					)
					
				);
				$this->sections[] = array(
                    'title'  => __( 'Footer Bottom', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-file-edit',
					'subsection' => true,
                    'fields' => array(
						array(
							'id'       => 'tb_footer_bottom_bg',
							'type'     => 'background',
							'title'    => __('Footer Background', 'cayto'),
							'subtitle' => __('Footer background with image, color, etc.', 'cayto'),
							'default'  => array(
								'background-color' => '#171717',
							),
							'output' => array('.tb_footer .footer-bottom'),
						),
						array(
							'id' => 'tb_footer_bottom_margin',
							'title' => 'Footer Bottom Margin',
							'subtitle' => __('Please, Enter margin of Footer Bottom.', 'cayto'),
							'type' => 'spacing',
							'mode' => 'margin',
							'units' => array('px'),
							'output'  => array('.tb_footer .footer-bottom'),
							'default' => array(
								'margin-top'     => '0', 
								'margin-right'   => '0', 
								'margin-bottom'  => '0', 
								'margin-left'    => '0',
								'units'          => 'px', 
							)
						),
						array(
							'id' => 'tb_footer_bottom_padding',
							'title' => 'Footer Bottom Padding',
							'subtitle' => __('Please, Enter padding of Footer Bottom.', 'cayto'),
							'type' => 'spacing',
							'units' => array('px'),
							'output'  => array('.tb_footer .footer-bottom'),
							'default' => array(
								'padding-top'     => '20px', 
								'padding-right'   => '0', 
								'padding-bottom'  => '20px', 
								'padding-left'    => '0',
								'units'          => 'px', 
							)
						),
					)
				);
				/*Styling Setting*/
				$this->sections[] = array(
                    'title'  => __( 'Styling Options', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-tint',
                    'fields' => array(
						array(
							'id'       => 'tb_primary_color',
							'type'     => 'color',
							'title'    => __('Primary Color', 'cayto'),
							'subtitle' => __('Controls several items, ex: link hovers, highlights, and more. (default: #19c4aa).', 'cayto'),
							'default'  => '#19c4aa',
							'validate' => 'color',
						),
					)
				);
				
				/* Typography Setting */
				$this->sections[] = array(
                    'title'  => __( 'Typography', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-font',
                    'fields' => array(
						/*Body font*/
						array(
							'id'          => 'tb_body_font',
							'type'        => 'typography', 
							'title'       => __('Body Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'output'      => array('body'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#747474',
								'font-family' => 'Monda',
								'font-size'   => '13px', 
								'line-height' => '20px'
							),
						),
						array(
							'id'          => 'tb_h1_font',
							'type'        => 'typography', 
							'title'       => __('H1 Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'letter-spacing' => true,
							'output'      => array('body h1'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#fff',
								'font-family' => 'Monda',
								'font-size'   => '40px', 
								'line-height' => '40px'
							),
						),
						array(
							'id'          => 'tb_h2_font',
							'type'        => 'typography', 
							'title'       => __('H2 Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'letter-spacing' => true,
							'output'      => array('body h2'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#333', 
								'font-family' => 'Monda', 
								'font-size'   => '25px', 
								'line-height' => '30px'
							),
						),
						array(
							'id'          => 'tb_h3_font',
							'type'        => 'typography', 
							'title'       => __('H3 Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'letter-spacing' => true,
							'output'      => array('body h3'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#272727', 
								'font-family' => 'Monda',
								'font-size'   => '24px', 
								'line-height' => '29px'
							),
						),
						array(
							'id'          => 'tb_h4_font',
							'type'        => 'typography', 
							'title'       => __('H4 Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'letter-spacing' => true,
							'output'      => array('body h4'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#272727',
								'font-family' => 'Monda',
								'font-size'   => '18px', 
								'line-height' => '22px'
							),
						),
						array(
							'id'          => 'tb_h5_font',
							'type'        => 'typography', 
							'title'       => __('H5 Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'letter-spacing' => true,
							'output'      => array('body h5'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#272727',
								'font-family' => 'Monda', 
								'font-size'   => '16px', 
								'line-height' => '19px'
							),
						),
						array(
							'id'          => 'tb_h6_font',
							'type'        => 'typography', 
							'title'       => __('H6 Font Options', 'cayto'),
							'google'      => true, 
							'font-backup' => true,
							'letter-spacing' => true,
							'output'      => array('body h6'),
							'units'       =>'px',
							'subtitle'    => __('Typography option with each property can be called individually.', 'cayto'),
							'default'     => array(
								'google'      => true,
								'color'       => '#272727',
								'font-family' => 'Monda',
								'font-size'   => '14pxpx',
								'line-height' => '17px'
							),
						),
					)
				);
				$this->sections[] = array(
					'title' => __('Extra Fonts', 'cayto'),
					'icon' => 'el el-fontsize',
					'subsection' => true,
					'fields' => array(
						array(
							'id' => 'google-font-1',
							'type' => 'typography',
							'subtitle' => __('Set font family for content... extend class "font-cayto-1"', 'cayto' ),
							'title' => __('Font 1', 'cayto'),
							'google' => true,
							'font-backup' => false,
							'font-style' => false,
							'color' => false,
							'text-align'=> false,
							'line-height'=>false,
							'font-size'=> false,
							'subsets'=> false,
							'output'=> array('.font-cayto-1'),
							'default' => array(
								'font-family' => 'Oswald',
								'font-weight'=> '400',
							)
						),
						array(
							'id' => 'google-font-2',
							'type' => 'typography',
							'subtitle' => __('Set font family for heading... extend class "font-cayto-2. Font Oswald Lighter"', 'cayto' ),
							'title' => __('Font 2', 'cayto'),
							'google' => true,
							'font-backup' => false,
							'font-style' => false,
							'color' => false,
							'text-align'=> false,
							'line-height'=>false,
							'font-size'=> false,
							'subsets'=> false,
							'output'=> array('.font-cayto-2'),
							'default' => array(
								'font-family' => 'Oswald',
								'font-weight'=> '300',
							)
						),
						array(
							'id' => 'google-font-3',
							'type' => 'typography',
							'subtitle' => __('Set font family for heading... extend class "font-cayto-3. Font Oswald Normal"', 'cayto' ),
							'title' => __('Font 3', 'cayto'),
							'google' => true,
							'font-backup' => false,
							'font-style' => false,
							'color' => false,
							'text-align'=> false,
							'line-height'=>false,
							'font-size'=> false,
							'subsets'=> false,
							'output'=> array('.font-cayto-3'),
							'default' => array(
								'font-family' => 'Oswald',
								'font-weight'=> '400',
							)
						),
						array(
							'id' => 'google-font-4',
							'type' => 'typography',
							'subtitle' => __('Set font family for heading... extend class "font-cayto-3. Font Oswald Bold"', 'cayto' ),
							'title' => __('Font 4', 'cayto'),
							'google' => true,
							'font-backup' => false,
							'font-style' => false,
							'color' => false,
							'text-align'=> false,
							'line-height'=>false,
							'font-size'=> false,
							'subsets'=> false,
							'output'=> array('.font-cayto-4'),
							'default' => array(
								'font-family' => 'Oswald',
								'font-weight'=> '700',
							)
						),
					)
				);
				/* Typography Setting */
				
				/*Title Bar Setting*/
				$this->sections[] = array(
                    'title'  => __( 'Title Bar', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-livejournal',
                    'fields' => array(
						array(
                            'id'       => 'tb_display_page_title',
                            'type'     => 'switch',
                            'title'    => __( 'Show page title', 'cayto' ),
                            'subtitle' => __( 'Show page title', 'cayto' ),
                            'default'  => true,
                        ),
						array(
							'id' => 'tb_title_bar_typography',
							'type' => 'typography',
							'title' => __('Typography', 'cayto'),
							'google' => true,
							'font-backup' => true,
							'all_styles' => true,
							'output'  => array('.title-bar .page-title'),
							'units' => 'px',
							'subtitle' => __('Typography option with title text.', 'cayto'),
							'default' => array(
								'color' => '#fff',
								'font-style' => 'normal',
								'font-weight' => '400',
								'font-family' => 'Monda',
								'google' => true,
								'font-size' => '40px',
								'line-height' => '40px',
								'text-align' => 'center'
							)
						),
						array(
							'id'       => 'tb_title_bar_bg',
							'type'     => 'background',
							'title'    => __('Background', 'cayto'),
							'subtitle' => __('background with image, color, etc.', 'cayto'),
							'default'  => array(
								'background-color' => 'transparent',
								'background-image' => TB_URI_PATH.'/assets/images/title_bars/bg-pagetitle.jpg',
								'background-repeat'=>'no-repeat',
								'background-position'=>'center top'
							),
							'output' => array('.title-bar, .title-bar-shop'),
						),
						array(
							'id' => 'tb_title_bar_margin',
							'title' => 'Margin',
							'subtitle' => __('Please, Enter margin of title bar.', 'cayto'),
							'type' => 'spacing',
							'mode' => 'margin',
							'units' => array('px'),
							'output' => array('.title-bar, .title-bar-shop'),
							'default' => array(
								'margin-top'     => '0', 
								'margin-right'   => '0', 
								'margin-bottom'  => '0', 
								'margin-left'    => '0',
								'units'          => 'px', 
							)
						),
						array(
							'id' => 'tb_title_bar_padding',
							'title' => 'Padding',
							'subtitle' => __('Please, Enter padding of title bar.', 'cayto'),
							'type' => 'spacing',
							'units' => array('px'),
							'output' => array('.title-bar, .title-bar-shop'),
							'default' => array(
								'padding-top'     => '135px', 
								'padding-right'   => '0', 
								'padding-bottom'  => '135px', 
								'padding-left'    => '0',
								'units'          => 'px', 
							)
						),
						array(
							'id'       => 'tb_page_breadcrumb_delimiter',
							'type'     => 'text',
							'title'    => __('Delimiter', 'cayto'),
							'subtitle' => __('Please, Enter Delimiter of page breadcrumb in title bar.', 'cayto'),
							'default'  => '|'
						)
					)
				);
				/* Breadcrumb */
				$this->sections[] = array(
					'title' => __('Breadcrumb', 'cayto'),
					'icon' => 'el-icon-livejournal',
					// 'subsection' => true,
					'fields' => array(
                        array(
                            'id'       => 'tb_display_breadcrumb',
                            'type'     => 'switch',
                            'title'    => __( 'Show Breadcrumb', 'cayto' ),
                            'subtitle' => __( 'Show Breadcrumb', 'cayto' ),
                            'default'  => true,
                        ),
						array(
							'id' => 'tb_breadcrumb_typography',
							'type' => 'typography',
							'title' => __('Typography', 'cayto'),
							'google' => true,
							'font-backup' => true,
							'all_styles' => true,
							'output'  => array('.tb-breadcrumb'),
							'units' => 'px',
							'subtitle' => __('Typography option with title text.', 'cayto'),
							'default' => array(
								'color' => '#b3b3b3',
								'font-style' => 'normal',
								'font-weight' => '400',
								'font-family' => 'Monda',
								'google' => true,
								'font-size' => '14px',
								'line-height' => '14px',
								'text-align' => 'left'
							)
						),
                        array(
                            'id' => 'tb_breadcrumb_margin',
                            'title' => 'Margin',
                            'subtitle' => __('Please, Enter margin of breadcrumb.', 'cayto'),
                            'type' => 'spacing',
                            'mode' => 'margin',
                            'units' => array('px'),
                            'output' => array('.tb-breadcrumb'),
                            'default' => array(
                                'margin-top'     => '0', 
                                'margin-right'   => '0', 
                                'margin-bottom'  => '50px', 
                                'margin-left'    => '-15px',
                                'units'          => 'px', 
                            )
                        ),
                        array( 
                            'id'       => 'breadcrumb-border',
                            'type'     => 'border',
                            'title'    => __('Breadcrumb Border Option', 'cayto'),
                            'output'   => array('.tb-breadcrumb'),
                            'desc'     => __('Setting border for breadcrumb.', 'cayto'),
                            'default'  => array(
                                'border-color'  => '#ebebeb', 
                                'border-style'  => 'solid', 
                                'border-top'    => '1px', 
                                'border-right'  => '0', 
                                'border-bottom' => '1px', 
                                'border-left'   => '0'
                            )
                        )
					)
				);
				
				/* Page Setting */
				$this->sections[] = array(
					'title'  => __( 'Page Setting', 'cayto' ),
					'desc'   => __( '', 'cayto' ),
					'icon'   => 'el-icon-file-edit',
					'fields' => array(
						array(
                            'id'       => 'tb_show_page_comment',
                            'type'     => 'switch',
                            'title'    => __( 'Show Page Comment', 'cayto' ),
							'default'  => true,
                        ),
					)
				);
				
				/*Post Setting*/
				$this->sections[] = array(
					'title'  => __( 'Post Setting', 'cayto' ),
					'desc'   => __( '', 'cayto' ),
					'icon'   => 'el-icon-file-edit',
					'fields' => array()
				);
				$this->sections[] = array(
                    'title'  => __( 'Archive Post', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => '',
					'subsection' => true,
                    'fields' => array(
						array( 
							'id'       => 'tb_blog_layout',
							'type'     => 'image_select',
							'title'    => __('Select Layout', 'cayto'),
							'subtitle' => __('Select layout of archive post.', 'cayto'),
							'options'  => array(
								'1col'	=> array(
										'alt'   => '1col',
										'img'   => TB_URI_PATH_ADMIN.'/assets/images/1col.png'
									),
								'2cl'	=> array(
											'alt'   => '2cl',
											'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cl.png'
										),
								'2cr'	=> array(
											'alt'   => '2cr',
											'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cr.png'
										),
								'3cm'	=> array(
											'alt'   => '3cm',
											'img'   => TB_URI_PATH_ADMIN.'/assets/images/3cm.png'
										)
							),
							'default' => '1col'
						),
						array(
							'id'       => 'tb_blog_image_default',
							'type'     => 'media',
							'url'      => true,
							'title'    => __('Image Default', 'cayto'),
							'subtitle' => __('Select an image file for image feature post.', 'cayto'),
							'default'  => array(
								'url'	=> ''
							),
						),
						array(
                            'id'       => 'tb_blog_crop_image',
                            'type'     => 'switch',
                            'title'    => __( 'Crop Image', 'cayto' ),
                            'subtitle' => __( 'Crop or not crop image of post on your archive post.', 'cayto' ),
							'default'  => true,
                        ),
						array(
							'id'       => 'tb_blog_image_width',
							'type'     => 'text',
							'title'    => __('Image Width', 'cayto'),
							'subtitle' => __('Please, Enter the width of image on your archive post. Default: 600.', 'cayto'),
							'indent'   => true,
                            'required' => array( 'tb_blog_crop_image', "=", 1 ),
							'default'  => '870'
						),
						array(
							'id'       => 'tb_blog_image_height',
							'type'     => 'text',
							'title'    => __('Image Height', 'cayto'),
							'subtitle' => __('Please, Enter the height of image on your archive post. Default: 400.', 'cayto'),
							'indent'   => true,
                            'required' => array( 'tb_blog_crop_image', "=", 1 ),
							'default'  => '430'
						),
						array(
                            'id'       => 'tb_blog_show_post_title',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Title', 'cayto' ),
                            'subtitle' => __( 'Show or not title of post on your archive post.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_blog_show_post_info',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Info', 'cayto' ),
                            'subtitle' => __( 'Show or not info of post on your archive post.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_blog_show_post_desc',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Description', 'cayto' ),
                            'subtitle' => __( 'Show or not description of post on your archive post.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_blog_post_excerpt_leng',
                            'type'     => 'text',
                            'title'    => __( 'Excerpt Leng', 'cayto' ),
                            'subtitle' => __( 'Insert the number of words you want to show in the post excerpts.', 'cayto' ),
							'default'  => '50',
                        ),
						array(
                            'id'       => 'tb_blog_post_excerpt_more',
                            'type'     => 'text',
                            'title'    => __( 'Excerpt More', 'cayto' ),
                            'subtitle' => __( 'Insert the character of words you want to show in the post excerpts.', 'cayto' ),
							'default'  => '...',
                        ),
                        array(
                            'id'       => 'tb_blog_post_readmore',
                            'type'     => 'text',
                            'title'    => __( 'Read More Link', 'cayto' ),
                            'subtitle' => __( 'Enter name of readmore link', 'cayto' ),
                            'default'  => __( 'Read More >>', 'cayto' ),
                        ),
					)
				);
				$this->sections[] = array(
                    'title'  => __( 'Single Post', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => '',
					'subsection' => true,
                    'fields' => array(
						array( 
							'id'       => 'tb_post_layout',
							'type'     => 'image_select',
							'title'    => __('Select Layout', 'cayto'),
							'subtitle' => __('Select layout of single blog.', 'cayto'),
							'options'  => array(
								'1col'	=> array(
										'alt'   => '1col',
										'img'   => TB_URI_PATH_ADMIN.'/assets/images/1col.png'
									),
								'2cl'	=> array(
											'alt'   => '2cl',
											'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cl.png'
										),
								'2cr'	=> array(
											'alt'   => '2cr',
											'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cr.png'
										),
								'3cm'	=> array(
											'alt'   => '3cm',
											'img'   => TB_URI_PATH_ADMIN.'/assets/images/3cm.png'
										)
							),
							'default' => '2cr'
						),
						array(
                            'id'       => 'tb_post_crop_image',
                            'type'     => 'switch',
                            'title'    => __( 'Crop Image', 'cayto' ),
                            'subtitle' => __( 'Crop or not crop image of post on your single blog.', 'cayto' ),
							'default'  => false,
                        ),
						array(
							'id'       => 'tb_post_image_width',
							'type'     => 'text',
							'title'    => __('Image Width', 'cayto'),
							'subtitle' => __('Please, Enter the width of image on your single blog. Default: 800.', 'cayto'),
							'indent'   => true,
                            'required' => array( 'tb_post_crop_image', "=", 1 ),
							'default'  => '870'
						),
						array(
							'id'       => 'tb_post_image_height',
							'type'     => 'text',
							'title'    => __('Image Height', 'cayto'),
							'subtitle' => __('Please, Enter the height of image on your single blog. Default: 800.', 'cayto'),
							'indent'   => true,
                            'required' => array( 'tb_post_crop_image', "=", 1 ),
							'default'  => '430'
						),
						array(
                            'id'       => 'tb_post_show_post_title',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Title', 'cayto' ),
                            'subtitle' => __( 'Show or not title of post on your single blog.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_post_show_social_share',
                            'type'     => 'switch',
                            'title'    => __( 'Show Social Share', 'cayto' ),
                            'subtitle' => __( 'Show or not social share of post on your single blog.', 'cayto' ),
							'default'  => false,
                        ),
						array(
                            'id'       => 'tb_post_show_post_info',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Info', 'cayto' ),
                            'subtitle' => __( 'Show or not info of post on your single blog.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_post_show_post_nav',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Navigation', 'cayto' ),
                            'subtitle' => __( 'Show or not post navigation on your single blog.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_post_show_post_tags',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Tags', 'cayto' ),
                            'subtitle' => __( 'Show or not post tags on your single blog.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_post_show_post_author',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Author', 'cayto' ),
                            'subtitle' => __( 'Show or not post author on your single blog.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_post_show_post_comment',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Comment', 'cayto' ),
                            'subtitle' => __( 'Show or not post comment on your single blog.', 'cayto' ),
							'default'  => true,
                        ),
						array(
                            'id'       => 'tb_post_show_post_related',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Related', 'cayto' ),
                            'subtitle' => __( 'Show or not post related on your single blog.', 'cayto' ),
							'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_post_no_post_related',
                            'type'     => 'text',
                            'title'    => __( 'Number Post on Related', 'cayto' ),
                            'subtitle' => __( 'Enter number post related on your single blog.', 'cayto' ),
                            'default'  => 3,
                        )
					)
				);

                $this->sections[] = array(
                    'title'  => __( 'Archive Portfolio', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => '',
                    'subsection' => true,
                    'fields' => array(
                        array( 
                            'id'       => 'tb_blog_layout',
                            'type'     => 'image_select',
                            'title'    => __('Select Layout', 'cayto'),
                            'subtitle' => __('Select layout of archive post.', 'cayto'),
                            'options'  => array(
                                '1col'  => array(
                                        'alt'   => '1col',
                                        'img'   => TB_URI_PATH_ADMIN.'/assets/images/1col.png'
                                    ),
                                '2cl'   => array(
                                            'alt'   => '2cl',
                                            'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cl.png'
                                        ),
                                '2cr'   => array(
                                            'alt'   => '2cr',
                                            'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cr.png'
                                        ),
                                '3cm'   => array(
                                            'alt'   => '3cm',
                                            'img'   => TB_URI_PATH_ADMIN.'/assets/images/3cm.png'
                                        )
                            ),
                            'default' => '1col'
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_template',
                            'type'     => 'select',
                            'title'    => __('Portfolio Template', 'cayto'),
                            'options'  => array(
                                "tpl1" => __("Template 1 ( Overlay effect )",'cayto'),
                                "tpl2" => __("Template 2 ( Overlay effect With Icon )",'cayto'),
                                "tpl" => __("Default",'cayto')
                            ),
                            'default'  => 'tpl',
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_show_filter',
                            'type'     => 'switch',
                            'title'    => __( 'Show Filter', 'cayto' ),
                            'subtitle' => __( 'Show or not filter.', 'cayto' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_show_page',
                            'type'     => 'switch',
                            'title'    => __( 'Show Pagination', 'cayto' ),
                            'subtitle' => __( 'Show or not pagination.', 'cayto' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_no_pading',
                            'type'     => 'switch',
                            'title'    => __( 'No Padding?', 'cayto' ),
                            'subtitle' => __( 'No padding in each items.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_column',
                            'type'     => 'select',
                            'title'    => __('Columns', 'cayto'),
                            'options'  => array(
                                "4" => __("4 Columns",'cayto'),
                                "3" => __("3 Columns",'cayto'),

                                "2" => __("2 Columns",'cayto'),

                                "1" => __("1 Column",'cayto'),
                            ),
                            'default'  => '3',
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_count',
                            'type'     => 'text',
                            'title'    => __('Count', 'cayto'),
                            'subtitle' => __('The number of posts to display on each page. Set to "-1" for display all posts on the page.', 'cayto')
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_view_now',
                            'type'     => 'switch',
                            'title'    => __( 'Show View Now', 'cayto' ),
                            'subtitle' => __( 'Show or not View Now of post in this element.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_archive_portfolio_view_more',
                            'type'     => 'switch',
                            'title'    => __( 'Show View More', 'cayto' ),
                            'subtitle' => __( 'Show or not View more of archive in this element.', 'cayto' ),
                            'default'  => true,
                        )
                        
                    )
                );

                $this->sections[] = array(
                    'title'  => __( 'Single Porfolio', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => '',
                    'subsection' => true,
                    'fields' => array(
                        array( 
                            'id'       => 'tb_portfolio_layout',
                            'type'     => 'image_select',
                            'title'    => __('Select Layout', 'cayto'),
                            'subtitle' => __('Select layout of single porfolio.', 'cayto'),
                            'options'  => array(
                                '1col'  => array(
                                        'alt'   => '1col',
                                        'img'   => TB_URI_PATH_ADMIN.'/assets/images/1col.png'
                                    ),
                                '2cl'   => array(
                                            'alt'   => '2cl',
                                            'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cl.png'
                                        ),
                                '2cr'   => array(
                                            'alt'   => '2cr',
                                            'img'   => TB_URI_PATH_ADMIN.'/assets/images/2cr.png'
                                        ),
                                '3cm'   => array(
                                            'alt'   => '3cm',
                                            'img'   => TB_URI_PATH_ADMIN.'/assets/images/3cm.png'
                                        )
                            ),
                            'default' => '1col'
                        ),
                        array(
                            'id'       => 'tb_portfolio_show_post_title',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Title', 'cayto' ),
                            'subtitle' => __( 'Show or not title of post on your single porfolio.', 'cayto' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'tb_portfolio_show_social_share',
                            'type'     => 'switch',
                            'title'    => __( 'Show Social Share', 'cayto' ),
                            'subtitle' => __( 'Show or not social share of post on your single porfolio.', 'cayto' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'tb_portfolio_show_post_nav',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Navigation', 'cayto' ),
                            'subtitle' => __( 'Show or not post navigation on your single porfolio.', 'cayto' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'tb_portfolio_show_post_author',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Author', 'cayto' ),
                            'subtitle' => __( 'Show or not post author on your single porfolio.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_portfolio_show_post_comment',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Comment', 'cayto' ),
                            'subtitle' => __( 'Show or not post comment on your single porfolio.', 'cayto' ),
                            'default'  => false,
                        ),
                        array(
                            'id'       => 'tb_portfolio_show_post_related',
                            'type'     => 'switch',
                            'title'    => __( 'Show Post Related', 'cayto' ),
                            'subtitle' => __( 'Show or not post related on your single porfolio.', 'cayto' ),
                            'default'  => true,
                        ),
                        array(
                            'id'       => 'tb_portfolio_no_post_related',
                            'type'     => 'text',
                            'title'    => __( 'Number Porfolio items on Related', 'cayto' ),
                            'subtitle' => __( 'Enter number porfolio item relate on your single porfolio.', 'cayto' ),
                            'default'  => 3
                        )
                    )
                );
				
				/*Shop Setting*/
				if (class_exists ( 'Woocommerce' )) {
					$this->sections[] = array(
						'title'  => __( 'Shop Setting', 'cayto' ),
						'desc'   => __( '', 'cayto' ),
						'icon'   => 'el-icon-shopping-cart',
						'fields' => array(
							
						)
						
					);
					$this->sections[] = array(
						'title'  => __( 'Archive Products', 'cayto' ),
						'desc'   => __( '', 'cayto' ),
						'icon'   => '',
						'subsection' => true,
						'fields' => array(
							array(
                                'id'       => 'tb_archive_sidebar_pos_shop',
                                'type'     => 'select',
                                'title'    => __('Sidebar Position (Shop layout)', 'cayto'),
                                'subtitle' => __('Select sidebar position in page archive products.', 'cayto'),
                                'options'  => array(
                                    'tb-sidebar-left' => 'Left',
                                    'tb-sidebar-right' => 'Right',
                                    'tb-sidebar-hidden' =>'Hide sidebar (Shop fullwidth)'
                                ),
                                'default'  => 'tb-sidebar-left',
                            ),
                            array(
                                'id'       => 'tb_archive_shop_column',
                                'type'     => 'select',
                                'title'    => __('Products Per Row', 'cayto'),
                                'subtitle' => __('Change products number display per row for the Shop page'),
                                'options'  => array(
                                    "4" => __("4 Products",'cayto'),
                                    "3" => __("3 Products",'cayto'),

                                    "2" => __("2 Products",'cayto'),

                                    "1" => __("1 Column",'cayto'),
                                ),
                                'default'  => '3',
                            ),
                            array(
                                'id'       => 'tb_archive_shop_ful_column',
                                'type'     => 'select',
                                'title'    => __('Products Per Row For Layout Fullwidth', 'cayto'),
                                'subtitle' => __('Change products number display per row for the Shop page( fullwidth layout )'),
                                'options'  => array(
                                    "4" => __("4 Products",'cayto'),
                                    "3" => __("3 Products",'cayto'),
                                    "2" => __("2 Products",'cayto'),
                                    "1" => __("1 Column",'cayto'),
                                ),
                                'default'  => '4',
                            ),
                            array(
                                'id'       => 'tb_archive_shop_per_page',
                                'type'     => 'text',
                                'title'    => __( 'Products Per Page', 'cayto' ),
                                'subtitle' => __( 'Enter number products per page.', 'cayto' ),
                                'default'  => 9,
                            ),
							array(
								'id'       => 'tb_archive_show_result_count',
								'type'     => 'switch',
								'title'    => __( 'Show Result Count', 'cayto' ),
								'subtitle' => __( 'Show result count in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_catalog_ordering',
								'type'     => 'switch',
								'title'    => __( 'Show Catalog Ordering', 'cayto' ),
								'subtitle' => __( 'Show catalog ordering in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_pagination_shop',
								'type'     => 'switch',
								'title'    => __( 'Show Pagination', 'cayto' ),
								'subtitle' => __( 'Show pagination in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_title_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Title', 'cayto' ),
								'subtitle' => __( 'Show product title in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_price_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Price', 'cayto' ),
								'subtitle' => __( 'Show product price in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_rating_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Rating', 'cayto' ),
								'subtitle' => __( 'Show product rating in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_sale_flash_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Sale Flash', 'cayto' ),
								'subtitle' => __( 'Show product sale flash in page archive products.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_archive_show_add_to_cart_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Add To Cart', 'cayto' ),
								'subtitle' => __( 'Show product add to cart in page archive products.', 'cayto' ),
								'default'  => true,
							),
                            array(
                                'id'       => 'tb_archive_show_quick_view_product',
                                'type'     => 'switch',
                                'title'    => __( 'Show Product Quick View', 'cayto' ),
                                'subtitle' => __( 'Show product quick view in page archive products.', 'cayto' ),
                                'default'  => true,
                            ),
                            array(
                                'id'       => 'tb_archive_show_whishlist_product',
                                'type'     => 'switch',
                                'title'    => __( 'Show Product Wish List', 'cayto' ),
                                'subtitle' => __( 'Show product wish lish in page archive products.', 'cayto' ),
                                'default'  => true,
                            ),
                            array(
                                'id'       => 'tb_archive_show_compare_product',
                                'type'     => 'switch',
                                'title'    => __( 'Show Product Compare', 'cayto' ),
                                'subtitle' => __( 'Show product compare in page archive products.', 'cayto' ),
                                'default'  => true,
                            ),
                             array(
                                'id'       => 'tb_archive_show_color_attribute',
                                'type'     => 'switch',
                                'title'    => __( 'Show Product Compare', 'cayto' ),
                                'subtitle' => __( 'Show color attribute in page archive products.', 'cayto' ),
                                'default'  => true,
                            ),
						)
					);
					$this->sections[] = array(
						'title'  => __( 'Single Product', 'cayto' ),
						'desc'   => __( '', 'cayto' ),
						'icon'   => '',
						'subsection' => true,
						'fields' => array(
							array(
								'id'       => 'tb_single_sidebar_pos_shop',
								'type'     => 'select',
								'title'    => __('Sidebar Position', 'cayto'),
								'subtitle' => __('Select sidebar position in page single product.', 'cayto'),
								'options'  => array(
									'tb-sidebar-left' => 'Left',
									'tb-sidebar-right' => 'Right',
                                    'tb-sidebar-hidden' =>'Hide sidebar (single fullwidth)'
								),
								'default'  => 'tb-sidebar-right',
							),
							array(
								'id'       => 'tb_single_show_title_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Title', 'cayto' ),
								'subtitle' => __( 'Show product title in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_price_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Price', 'cayto' ),
								'subtitle' => __( 'Show product price in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_rating_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Rating', 'cayto' ),
								'subtitle' => __( 'Show product rating in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_sale_flash_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Sale Flash', 'cayto' ),
								'subtitle' => __( 'Show product sale flash in page single product.', 'cayto' ),
								'default'  => true,
							),
                            array( 
                                'id'       => 'tb_video_tab',
                                'type'     => 'select',
                                    'title'    => __('How to display video tab?', 'cayto'),
                                    'options'  => array(
                                        'none'=>__('Hidden', 'cayto'),
                                        'on_tabs'=>__('Show in Woocommerce tabs', 'cayto'),
                                        'on_thumbnail'=>__('Show on product thumbnails', 'cayto')
                                    ),
                                    'default'  => 'on_thumbnail'
                            ),
							array(
								'id'       => 'tb_single_show_excerpt',
								'type'     => 'switch',
								'title'    => __( 'Show Product Excerpt', 'cayto' ),
								'subtitle' => __( 'Show product excerpt in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_add_to_cart_product',
								'type'     => 'switch',
								'title'    => __( 'Show Product Add To Cart', 'cayto' ),
								'subtitle' => __( 'Show product add to cart in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_meta',
								'type'     => 'switch',
								'title'    => __( 'Show Product Meta', 'cayto' ),
								'subtitle' => __( 'Show product meta in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_data_tabs',
								'type'     => 'switch',
								'title'    => __( 'Show Product Data Tabs', 'cayto' ),
								'subtitle' => __( 'Show product data tabs in page single product.', 'cayto' ),
								'default'  => true,
							),
							array(
								'id'       => 'tb_single_show_related_products',
								'type'     => 'switch',
								'title'    => __( 'Show Product Related Products', 'cayto' ),
								'subtitle' => __( 'Show product related products in page single product.', 'cayto' ),
								'default'  => true,
							),
						)
					);
				}
				/*Custom CSS*/
				$this->sections[] = array(
                    'title'  => __( 'Custom CSS', 'cayto' ),
                    'desc'   => __( '', 'cayto' ),
                    'icon'   => 'el-icon-css',
                    'fields' => array(
						array(
							'id'       => 'custom_css_code',
							'type'     => 'ace_editor',
							'title'    => __('Custom CSS Code', 'cayto'),
							'subtitle' => __('Quickly add some CSS to your theme by adding it to this block..', 'cayto'),
							'mode'     => 'css',
							'theme'    => 'monokai',
							'default'  => ''
						)
					)
					
				);
				/*Import / Export*/
				$this->sections[] = array(
                    'title'  => __( 'Import / Export', 'cayto' ),
                    'desc'   => __( 'Import and Export your Redux Framework settings from file, text or URL.', 'cayto' ),
                    'icon'   => 'el-icon-refresh',
                    'fields' => array(
                        array(
                            'id'         => 'tb_import_export',
                            'type'       => 'import_export',
                            'title'      => 'Import Export',
                            'subtitle'   => __('Save and restore your Redux options','cayto'),
                            'full_width' => false,
                        ),
						array (
							'id'            => 'tb_import',
							'type'          => 'js_button',
							'title'         => 'One Click Demo Import.',
						    'subtitle'   => __('Tools > Demo Content Install. <a href="https://www.youtube.com/watch?v=TjiPvlew3KU" target="_blank">Detail Video</a>','cayto'),
						),
                    ),
                );
				
            }

            public function setHelpTabs() {

                // Custom page help tabs, displayed using the help API. Tabs are shown in order of definition.
                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-1',
                    'title'   => __( 'Theme Information 1', 'cayto' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'cayto' )
                );

                $this->args['help_tabs'][] = array(
                    'id'      => 'redux-help-tab-2',
                    'title'   => __( 'Theme Information 2', 'cayto' ),
                    'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'cayto' )
                );

                // Set the help sidebar
                $this->args['help_sidebar'] = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'cayto' );
            }

            /**
             * All the possible arguments for Redux.
             * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
             * */
            public function setArguments() {

                $theme = wp_get_theme(); // For use with some settings. Not necessary.

                $this->args = array(
                    // TYPICAL -> Change these values as you need/desire
                    'opt_name'             => 'tb_options',
                    // This is where your data is stored in the database and also becomes your global variable name.
                    'display_name'         => $theme->get( 'Name' ),
                    // Name that appears at the top of your panel
                    'display_version'      => $theme->get( 'Version' ),
                    // Version that appears at the top of your panel
                    'menu_type'            => 'menu',
                    //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
                    'allow_sub_menu'       => true,
                    // Show the sections below the admin menu item or not
                    'menu_title'           => __( 'Theme Options', 'cayto' ),
                    'page_title'           => __( 'Theme Options', 'cayto' ),
                    // You will need to generate a Google API key to use this feature.
                    // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
                    'google_api_key'       => '',
                    // Set it you want google fonts to update weekly. A google_api_key value is required.
                    'google_update_weekly' => false,
                    // Must be defined to add google fonts to the typography module
                    'async_typography'     => true,
                    // Use a asynchronous font on the front end or font string
                    //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
                    'admin_bar'            => true,
                    // Show the panel pages on the admin bar
                    'admin_bar_icon'     => 'dashicons-portfolio',
                    // Choose an icon for the admin bar menu
                    'admin_bar_priority' => 50,
                    // Choose an priority for the admin bar menu
                    'global_variable'      => '',
                    // Set a different name for your global variable other than the opt_name
                    'dev_mode'             => false,
                    // Show the time the page took to load, etc
                    'update_notice'        => false,
                    // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
                    'customizer'           => true,
                    // Enable basic customizer support
                    //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
                    //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

                    // OPTIONAL -> Give you extra features
                    'page_priority'        => null,
                    // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
                    'page_parent'          => 'themes.php',
                    // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
                    'page_permissions'     => 'manage_options',
                    // Permissions needed to access the options panel.
                    'menu_icon'            => '',
                    // Specify a custom URL to an icon
                    'last_tab'             => '',
                    // Force your panel to always open to a specific tab (by id)
                    'page_icon'            => 'icon-themes',
                    // Icon displayed in the admin panel next to your menu_title
                    'page_slug'            => '_options',
                    // Page slug used to denote the panel
                    'save_defaults'        => true,
                    // On load save the defaults to DB before user clicks save or not
                    'default_show'         => false,
                    // If true, shows the default value next to each field that is not the default value.
                    'default_mark'         => '',
                    // What to print by the field's title if the value shown is default. Suggested: *
                    'show_import_export'   => true,
                    // Shows the Import/Export panel when not used as a field.

                    // CAREFUL -> These options are for advanced use only
                    'transient_time'       => 60 * MINUTE_IN_SECONDS,
                    'output'               => true,
                    // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
                    'output_tag'           => true,
                    // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
                    // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

                    // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
                    'database'             => '',
                    // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!
                    'system_info'          => false,
                    // REMOVE

                    // HINTS
                    'hints'                => array(
                        'icon'          => 'icon-question-sign',
                        'icon_position' => 'right',
                        'icon_color'    => 'lightgray',
                        'icon_size'     => 'normal',
                        'tip_style'     => array(
                            'color'   => 'light',
                            'shadow'  => true,
                            'rounded' => false,
                            'style'   => '',
                        ),
                        'tip_position'  => array(
                            'my' => 'top left',
                            'at' => 'bottom right',
                        ),
                        'tip_effect'    => array(
                            'show' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'mouseover',
                            ),
                            'hide' => array(
                                'effect'   => 'slide',
                                'duration' => '500',
                                'event'    => 'click mouseleave',
                            ),
                        ),
                    )
                );
				
                // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
                $this->args['share_icons'][] = array(
                    'url'   => '#',
                    'title' => 'Visit us on GitHub',
                    'icon'  => 'el-icon-github'
                    //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
                );
                $this->args['share_icons'][] = array(
                    'url'   => '#',
                    'title' => 'Like us on Facebook',
                    'icon'  => 'el-icon-facebook'
                );
                $this->args['share_icons'][] = array(
                    'url'   => '#',
                    'title' => 'Follow us on Twitter',
                    'icon'  => 'el-icon-twitter'
                );
                $this->args['share_icons'][] = array(
                    'url'   => '#',
                    'title' => 'Find us on LinkedIn',
                    'icon'  => 'el-icon-linkedin'
                );
            }

            public function validate_callback_function( $field, $value, $existing_value ) {
                $error = true;
                $value = 'just testing';

                /*
              do your validation

              if(something) {
                $value = $value;
              } elseif(something else) {
                $error = true;
                $value = $existing_value;
                
              }
             */

                $return['value'] = $value;
                $field['msg']    = 'your custom error message';
                if ( $error == true ) {
                    $return['error'] = $field;
                }

                return $return;
            }

            public function class_field_callback( $field, $value ) {
                print_r( $field );
                echo '<br/>CLASS CALLBACK';
                print_r( $value );
            }

        }

        global $reduxConfig;
        $reduxConfig = new Redux_Framework_theme_config();
    } else {
        echo "The class named Redux_Framework_theme_config has already been called. <strong>Developers, you need to prefix this class with your company name or you'll run into problems!</strong>";
    }

    /**
     * Custom function for the callback referenced above
     */
    if ( ! function_exists( 'redux_my_custom_field' ) ):
        function redux_my_custom_field( $field, $value ) {
            print_r( $field );
            echo '<br/>';
            print_r( $value );
        }
    endif;

    /**
     * Custom function for the callback validation referenced above
     * */
    if ( ! function_exists( 'redux_validate_callback_function' ) ):
        function redux_validate_callback_function( $field, $value, $existing_value ) {
            $error = true;
            $value = 'just testing';

            /*
          do your validation

          if(something) {
            $value = $value;
          } elseif(something else) {
            $error = true;
            $value = $existing_value;
            
          }
         */

            $return['value'] = $value;
            $field['msg']    = 'your custom error message';
            if ( $error == true ) {
                $return['error'] = $field;
            }

            return $return;
        }
    endif;

if( ! function_exists('tb_get_option') ){
    function tb_get_option($name, $default=false){
        global $tb_options;
        return isset( $tb_options[ $default ] ) ? $tb_options[ $default ] : $default;
    }
}

if( ! function_exists('tb_update_option') ){
    function tb_update_option($name, $value){
        global $tb_options;
        $tb_options[ $name ] = $value;
    }
}

