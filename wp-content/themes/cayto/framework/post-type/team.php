<?php

// Register Custom Post Type

function tb_add_post_type_Team() {


    //Register post type Team

    $labels = array(

            'name'                => _x( 'Team', 'Post Type General Name', 'cayto' ),

            'singular_name'       => _x( 'Team Item', 'Post Type Singular Name', 'cayto' ),

            'menu_name'           => __( 'Team', 'cayto' ),

            'parent_item_colon'   => __( 'Parent Item:', 'cayto' ),

            'all_items'           => __( 'All Items', 'cayto' ),

            'view_item'           => __( 'View Item', 'cayto' ),

            'add_new_item'        => __( 'Add New Item', 'cayto' ),

            'add_new'             => __( 'Add New', 'cayto' ),

            'edit_item'           => __( 'Edit Item', 'cayto' ),

            'update_item'         => __( 'Update Item', 'cayto' ),

            'search_items'        => __( 'Search Item', 'cayto' ),

            'not_found'           => __( 'Not found', 'cayto' ),

            'not_found_in_trash'  => __( 'Not found in Trash', 'cayto' ),

    );

    $args = array(

            'label'               => __( 'Team', 'cayto' ),

            'description'         => __( 'Team Description', 'cayto' ),

            'labels'              => $labels,

            'supports'            => array( 'title', 'editor', 'author', 'thumbnail', 'revisions', ),

            'hierarchical'        => true,

            'public'              => true,

            'show_ui'             => true,

            'show_in_menu'        => true,

            'show_in_nav_menus'   => true,

            'show_in_admin_bar'   => true,

            'menu_position'       => 5,

            'menu_icon'           => 'dashicons-groups',

            'can_export'          => true,

            'has_archive'         => true,

            'exclude_from_search' => false,

            'publicly_queryable'  => true,

            'capability_type'     => 'page',

    );

    

    if(function_exists('custom_reg_post_type')) {

        custom_reg_post_type( 'Team', $args );

    }

    

}



// Hook into the 'init' action

add_action( 'init', 'tb_add_post_type_Team', 0 );

