<?php

// Register Custom Post Type

function tb_add_post_type_portfolio() {

    // Register taxonomy

    $labels = array(

            'name'              => _x( 'Portfolio Category', 'taxonomy general name', 'cayto' ),

            'singular_name'     => _x( 'Portfolio Category', 'taxonomy singular name', 'cayto' ),

            'search_items'      => __( 'Search Portfolio Category', 'cayto' ),

            'all_items'         => __( 'All Portfolio Category', 'cayto' ),

            'parent_item'       => __( 'Parent Portfolio Category', 'cayto' ),

            'parent_item_colon' => __( 'Parent Portfolio Category:', 'cayto' ),

            'edit_item'         => __( 'Edit Portfolio Category', 'cayto' ),

            'update_item'       => __( 'Update Portfolio Category', 'cayto' ),

            'add_new_item'      => __( 'Add New Portfolio Category', 'cayto' ),

            'new_item_name'     => __( 'New Portfolio Category Name', 'cayto' ),

            'menu_name'         => __( 'Portfolio Category', 'cayto' ),

    );



    $args = array(

            'hierarchical'      => true,

            'labels'            => $labels,

            'show_ui'           => true,

            'show_admin_column' => true,

            'query_var'         => true,

            'rewrite'           => array( 'slug' => 'portfolio_category' ),

    );

    if(function_exists('custom_reg_taxonomy')) {

        custom_reg_taxonomy( 'portfolio_category', array( 'portfolio' ), $args );

    }
  

    //Register post type Portfolio

    $labels = array(

            'name'                => _x( 'Portfolio', 'Post Type General Name', 'cayto' ),

            'singular_name'       => _x( 'Portfolio Item', 'Post Type Singular Name', 'cayto' ),

            'menu_name'           => __( 'Portfolio', 'cayto' ),

            'parent_item_colon'   => __( 'Parent Item:', 'cayto' ),

            'all_items'           => __( 'All Items', 'cayto' ),

            'view_item'           => __( 'View Item', 'cayto' ),

            'add_new_item'        => __( 'Add New Item', 'cayto' ),

            'add_new'             => __( 'Add New', 'cayto' ),

            'edit_item'           => __( 'Edit Item', 'cayto' ),

            'update_item'         => __( 'Update Item', 'cayto' ),

            'search_items'        => __( 'Search Item', 'cayto' ),

            'not_found'           => __( 'Not found', 'cayto' ),

            'not_found_in_trash'  => __( 'Not found in Trash', 'cayto' ),

    );

    $args = array(

            'label'               => __( 'Portfolio', 'cayto' ),

            'description'         => __( 'Portfolio Description', 'cayto' ),

            'labels'              => $labels,

            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', ),

            'taxonomies'          => array( 'portfolio_category', 'portfolio_tag' ),

            'hierarchical'        => true,

            'public'              => true,

            'show_ui'             => true,

            'show_in_menu'        => true,

            'show_in_nav_menus'   => true,

            'show_in_admin_bar'   => true,

            'menu_position'       => 5,

            'menu_icon'           => 'dashicons-pressthis',

            'can_export'          => true,

            'has_archive'         => true,

            'exclude_from_search' => false,

            'publicly_queryable'  => true,

            'capability_type'     => 'page',

    );

    

    if(function_exists('custom_reg_post_type')) {

        custom_reg_post_type( 'portfolio', $args );

    }

    

}



// Hook into the 'init' action

add_action( 'init', 'tb_add_post_type_portfolio', 0 );

