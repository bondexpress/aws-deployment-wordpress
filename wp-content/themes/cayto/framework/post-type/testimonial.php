<?php
// Register Custom Post Type
function tb_add_post_type_testimonial() {
    // Register taxonomy
    $labels = array(
            'name'              => _x( 'Testimonial Category', 'taxonomy general name', 'cayto' ),
            'singular_name'     => _x( 'Testimonial Category', 'taxonomy singular name', 'cayto' ),
            'search_items'      => __( 'Search Testimonial Category', 'cayto' ),
            'all_items'         => __( 'All Testimonial Category', 'cayto' ),
            'parent_item'       => __( 'Parent Testimonial Category', 'cayto' ),
            'parent_item_colon' => __( 'Parent Testimonial Category:', 'cayto' ),
            'edit_item'         => __( 'Edit Testimonial Category', 'cayto' ),
            'update_item'       => __( 'Update Testimonial Category', 'cayto' ),
            'add_new_item'      => __( 'Add New Testimonial Category', 'cayto' ),
            'new_item_name'     => __( 'New Testimonial Category Name', 'cayto' ),
            'menu_name'         => __( 'Testimonial Category', 'cayto' ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'testimonial_category' ),
    );
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy( 'testimonial_category', array( 'testimonial' ), $args );
    }
    //Register tags
    $labels = array(
            'name'              => _x( 'Testimonial Tag', 'taxonomy general name', 'cayto' ),
            'singular_name'     => _x( 'Testimonial Tag', 'taxonomy singular name', 'cayto' ),
            'search_items'      => __( 'Search Testimonial Tag', 'cayto' ),
            'all_items'         => __( 'All Testimonial Tag', 'cayto' ),
            'parent_item'       => __( 'Parent Testimonial Tag', 'cayto' ),
            'parent_item_colon' => __( 'Parent Testimonial Tag:', 'cayto' ),
            'edit_item'         => __( 'Edit Testimonial Tag', 'cayto' ),
            'update_item'       => __( 'Update Testimonial Tag', 'cayto' ),
            'add_new_item'      => __( 'Add New Testimonial Tag', 'cayto' ),
            'new_item_name'     => __( 'New Testimonial Tag Name', 'cayto' ),
            'menu_name'         => __( 'Testimonial Tag', 'cayto' ),
    );

    $args = array(
            'hierarchical'      => true,
            'labels'            => $labels,
            'show_ui'           => true,
            'show_admin_column' => true,
            'query_var'         => true,
            'rewrite'           => array( 'slug' => 'testimonial_tag' ),
    );
    
    if(function_exists('custom_reg_taxonomy')) {
        custom_reg_taxonomy( 'testimonial_tag', array( 'testimonial' ), $args );
    }
    
    //Register post type Testimonial
    $labels = array(
            'name'                => _x( 'Testimonial', 'Post Type General Name', 'cayto' ),
            'singular_name'       => _x( 'Testimonial Item', 'Post Type Singular Name', 'cayto' ),
            'menu_name'           => __( 'Testimonial', 'cayto' ),
            'parent_item_colon'   => __( 'Parent Item:', 'cayto' ),
            'all_items'           => __( 'All Items', 'cayto' ),
            'view_item'           => __( 'View Item', 'cayto' ),
            'add_new_item'        => __( 'Add New Item', 'cayto' ),
            'add_new'             => __( 'Add New', 'cayto' ),
            'edit_item'           => __( 'Edit Item', 'cayto' ),
            'update_item'         => __( 'Update Item', 'cayto' ),
            'search_items'        => __( 'Search Item', 'cayto' ),
            'not_found'           => __( 'Not found', 'cayto' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'cayto' ),
    );
    $args = array(
            'label'               => __( 'Testimonial', 'cayto' ),
            'description'         => __( 'Testimonial Description', 'cayto' ),
            'labels'              => $labels,
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
            'taxonomies'          => array( 'testimonial_category', 'testimonial_tag' ),
            'hierarchical'        => true,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'menu_icon'           => 'dashicons-yes',
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'page',
    );
    
    if(function_exists('custom_reg_post_type')) {
        custom_reg_post_type( 'testimonial', $args );
    }
    
}

// Hook into the 'init' action
add_action( 'init', 'tb_add_post_type_testimonial', 0 );
