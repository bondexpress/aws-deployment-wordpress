<?php
if ( ! isset( $content_width ) ) $content_width = 900;
if ( is_singular() ) wp_enqueue_script( "comment-reply" );
if ( ! function_exists( 'tb_theme_setup' ) ) {
function tb_theme_setup() {
	load_theme_textdomain( 'cayto', get_template_directory() . '/languages' );
	// Add Custom Header.
	add_theme_support('custom-header');
	// Add RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );
	// Enable support for Post Thumbnails, and declare two sizes.
	add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'main_navigation'   => esc_html__( 'Primary Menu','cayto' ),
	) );
	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
	) );
	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'audio', 'quote', 'link', 'gallery','status'
	) );
	// This theme allows users to set a custom background.
	add_theme_support( 'custom-background', apply_filters( 'tbtheme_custom_background_args', array(
		'default-color' => 'f5f5f5',
	) ) );
	// Add support for featured content.
	add_theme_support( 'featured-content', array(
		'featured_content_filter' => 'tbtheme_get_featured_posts',
		'max_posts' => 6,
	) );
	add_theme_support( "title-tag" );
	// This theme uses its own gallery styles.
	add_filter( 'use_default_gallery_style', '__return_false' );
	// Register a new image size
	add_image_size( 'cayto-blog-large-hard-crop', 870, 430, true );
	add_image_size( 'cayto-blog-medium-hard-crop', 370, 207, true );
	add_image_size( 'cayto-portfolio-thumb', 370, 231, true );
	add_image_size( 'cayto-blog-grid', 370, 309, true );
	add_image_size( 'cayto-colection-thumb', 270, 330, true );
	add_image_size( 'cayto-blog-grid-medium', 270, 200, true );
	add_image_size( 'cayto-thumb-side-post', 100, 110, true );
}
}
add_action( 'after_setup_theme', 'tb_theme_setup' );
add_filter( 'post-format-status', 'tb_avia_status_content_filter', 10, 1 );
function tb_page_title(){
global $tb_options, $post;
$class = array();
$class[] = 'title-bar';
if(isset($post->ID)){
	$show_on_page = get_post_meta($post->ID, 'tb_page_title_bar', true);
	if($show_on_page){
		/* Title bar for page */
		tb_get_content_title_bar($class);
	}else if (is_archive()){
		/* Title bar for page archive */
		tb_get_content_title_bar($class);
	}else if(is_single()) {
		/* Title bar for single post */
		tb_get_content_title_bar($class);
	}
}else{		
	/* Title bar for blog, archives, search, 404... */
	tb_get_content_title_bar($class);
}
}
function tb_get_content_title_bar($class){
global $tb_options;
	if( $tb_options['tb_display_page_title'] ):
?>
	<div class="<?php echo esc_attr(implode(' ', $class)); ?>">
		<div class="container">
			<div class="text-center">
				<h1 class="page-title"><?php echo tb_getPageTitle(); ?></h1>
			</div>
		</div>
	</div>
<?php
	endif;
	if( $tb_options['tb_display_breadcrumb'] ):
		$delimiter = isset($tb_options['tb_page_breadcrumb_delimiter']) ? $tb_options['tb_page_breadcrumb_delimiter'] :  '/';
	?>
	<!-- Breadcumb -->
	<div class="tb-breadcrumb">
		<div class="container">
			<?php  echo tb_theme_page_breadcrumb( $delimiter ); ?>
		</div>
	</div>
<?php endif;
}
function tb_getPageTitle(){
if (!is_archive()){
	/* page. */
	if(is_page()) :
		the_title();
	/* search */
	elseif (is_search()):
		printf( esc_html__( 'Search Results for: %s', 'cayto' ), '<span>' . get_search_query() . '</span>' );
	/* 404 */ 
	elseif (is_404()):
		_e( '404', 'cayto');
	/* Blog */ 
	elseif (is_home()):
		_e( 'Blog', 'cayto');
	/* other */
	else :
		the_title();
	endif;
} else {
	/* category. */
	if ( is_category() || ( function_exists('is_product_category') && is_product_category() ) ) :
		single_cat_title();
	elseif ( is_tag() || ( function_exists('is_product_tag') && is_product_tag() ) ) :
	/* tag. */
		single_tag_title();
	/* author. */
	elseif ( is_author() ) :
		printf( esc_html__( 'Author: %s', 'cayto' ), '<span class="vcard">' . get_the_author() . '</span>' );
	/* date */
	elseif ( is_day() ) :
		printf( esc_html__( 'Day: %s', 'cayto' ), '<span>' . get_the_date() . '</span>' );
	elseif ( is_month() ) :
		printf( esc_html__( 'Month: %s', 'cayto' ), '<span>' . get_the_date('F Y') . '</span>' );
	elseif ( is_year() ) :
		printf( esc_html__( 'Year: %s', 'cayto' ), '<span>' . get_the_date('Y') . '</span>' );
	/* post format */
	elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
		_e( 'Asides', 'cayto' );
	elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
		_e( 'Galleries', 'cayto');
	elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
		_e( 'Images', 'cayto');
	elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
		_e( 'Videos', 'cayto' );
	elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
		_e( 'Quotes', 'cayto' );
	elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
		_e( 'Links', 'cayto' );
	elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
		_e( 'Statuses', 'cayto' );
	elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
		_e( 'Audios', 'cayto' );
	elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
		_e( 'Chats', 'cayto' );
	else :
	/* other */
		the_title();
	endif;
}
}
/* Header */
function tb_header() {
global $tb_options,$post;
$header_layout = isset( $tb_options["tb_header_layout"] ) ? $tb_options["tb_header_layout"] : 'v1';
if($post && isset($post->ID)){
	$tb_header = get_post_meta($post->ID, 'tb_header', true) ? get_post_meta($post->ID, 'tb_header', true) : 'global';
	$header_layout = ($tb_header=='global') ? $header_layout : $tb_header;
	$tb_options['tb_header_layout'] = $header_layout;
}
switch ($header_layout) {
	case 'v1':
		get_template_part('framework/headers/header', 'v1');
		break;
	case 'v2':
		get_template_part('framework/headers/header', 'v2');
		break;
	case 'v3':
		get_template_part('framework/headers/header', 'v3');
		break;
	case 'v4':
		get_template_part('framework/headers/header', 'v4');
		break;
	case 'v5':
		get_template_part('framework/headers/header', 'v5');
		break;
	case 'v6':
		get_template_part('framework/headers/header', 'v6');
		break;
	case 'v7':
		get_template_part('framework/headers/header', 'v7');
		break;
	default :
		get_template_part('framework/headers/header', 'v7');
		break;
}
}
/* Main Logo */
if (!function_exists('tb_theme_logo')) {
function tb_theme_logo() {
	global $tb_options,$post;
	$postid = isset($post->ID) ? $post->ID : 0;
	$meta_logo = get_post_meta( $postid, 'tb_logo_image', true );
	if( !empty( $meta_logo ) || ( isset($tb_options['tb_logo_image']['url']) && !empty( $tb_options['tb_logo_image']['url']) ) ){
		$logo = ! empty($meta_logo) ? $meta_logo : $tb_options['tb_logo_image']['url'];			
		$logo = '<img src="'.esc_url($logo).'" class="main-logo" alt="' . __('Main Logo', 'cayto') .  '"/>';
	}else{
		$logo = isset( $tb_options['tb_logo_text'] ) ? strip_tags( $tb_options['tb_logo_text'],'<span><br><em><strong>' ) : wp_kses( __('Cay<span class="primary_color">.</span>To','cayto'), array(
			'br' => array(),
			'em' => array(),
			'strong' => array(),
			'span' => array()
		   ) );
	}
	echo $logo;
}
}
/* Page title */
if (!function_exists('tb_theme_page_title')) {
function tb_theme_page_title() { 
		ob_start();
		if( is_home() ){
			_e('Home', 'cayto');
		}elseif(is_search()){
			esc_html_e('Search Keyword: ', 'cayto');
			echo '<span class="keywork">'. get_search_query(). '</span>';
		}elseif (!is_archive()) {
			the_title();
		} else { 
			if (is_category()){
				single_cat_title();
			}elseif(get_post_type() == 'recipe' || get_post_type() == 'portfolio' || get_post_type() == 'produce' || get_post_type() == 'team' || get_post_type() == 'testimonial' || get_post_type() == 'myclients' || get_post_type() == 'product'){
				single_term_title();
			}elseif (is_tag()){
				single_tag_title();
			}elseif (is_author()){
				printf(__('Author: %s', 'cayto'), '<span class="vcard">' . get_the_author() . '</span>');
			}elseif (is_day()){
				printf(__('Day: %s', 'cayto'), '<span>' . get_the_date() . '</span>');
			}elseif (is_month()){
				printf(__('Month: %s', 'cayto'), '<span>' . get_the_date(_e('F Y', 'cayto')) . '</span>');
			}elseif (is_year()){
				printf(__('Year: %s', 'cayto'), '<span>' . get_the_date(_e('Y', 'cayto')) . '</span>');
			}elseif (is_tax('post_format', 'post-format-aside')){
				esc_html_e('Asides', 'cayto');
			}elseif (is_tax('post_format', 'post-format-gallery')){
				esc_html_e('Galleries', 'cayto');
			}elseif (is_tax('post_format', 'post-format-image')){
				esc_html_e('Images', 'cayto');
			}elseif (is_tax('post_format', 'post-format-video')){
				esc_html_e('Videos', 'cayto');
			}elseif (is_tax('post_format', 'post-format-quote')){
				esc_html_e('Quotes', 'cayto');
			}elseif (is_tax('post_format', 'post-format-link')){
				esc_html_e('Links', 'cayto');
			}elseif (is_tax('post_format', 'post-format-status')){
				esc_html_e('Statuses', 'cayto');
			}elseif (is_tax('post_format', 'post-format-audio')){
				esc_html_e('Audios', 'cayto');
			}elseif (is_tax('post_format', 'post-format-chat')){
				esc_html_e('Chats', 'cayto');
			}else{
				esc_html_e('Archives', 'cayto');
			}
		}
		return ob_get_clean();
}
}
/* Page breadcrumb */
if (!function_exists('tb_theme_page_breadcrumb')) {
function tb_theme_page_breadcrumb($delimiter) {
		ob_start();
		$delimiter = esc_attr($delimiter);
		$home = esc_html__('Home', 'cayto');
		$before = '<span class="current">'; // tag before the current crumb
		$after = '</span>'; // tag after the current crumb
		global $post;
		$homeLink = home_url('/');
		if( is_home() ){
			_e('Home', 'cayto');
		}else{
			echo '<a href="' . esc_url($homeLink) . '">' . $home . '</a> ' . $delimiter . ' ';
		}
		if ( is_category() ) {
			$thisCat = get_category(get_query_var('cat'), false);
			if ($thisCat->parent != 0) echo get_category_parents($thisCat->parent, TRUE, ' ' . $delimiter . ' ');
			echo wp_kses_post($before . esc_html__('Archive by category: ', 'cayto') . single_cat_title('', false) . $after);
		} elseif ( is_search() ) {
			echo wp_kses_post($before . esc_html__('Search results for: ', 'cayto') . get_search_query() . $after);
		} elseif ( is_day() ) {
			echo '<a href="' . get_month_link(get_the_time('Y'),get_the_time('m')) . '">' . get_the_time('F').' '. get_the_time('Y') . '</a> ' . $delimiter . ' ';
			echo wp_kses_post($before . get_the_time('d') . $after);
		} elseif ( is_month() ) {
			echo wp_kses_post($before . get_the_time('F'). ' '. get_the_time('Y') . $after);
		} elseif ( is_single() && !is_attachment() ) {
		if ( get_post_type() != 'post' ) {
			if(get_post_type() == 'portfolio'){
				$terms = get_the_terms(get_the_ID(), 'portfolio_category', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'portfolio_category', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}elseif(get_post_type() == 'recipe'){
				$terms = get_the_terms(get_the_ID(), 'recipe_category', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'recipe_category', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}elseif(get_post_type() == 'produce'){
				$terms = get_the_terms(get_the_ID(), 'produce_category', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'produce_category', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}elseif(get_post_type() == 'team'){
				$terms = get_the_terms(get_the_ID(), 'team_category', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'team_category', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}elseif(get_post_type() == 'testimonial'){
				$terms = get_the_terms(get_the_ID(), 'testimonial_category', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'testimonial_category', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}elseif(get_post_type() == 'myclients'){
				$terms = get_the_terms(get_the_ID(), 'clientscategory', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'clientscategory', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}elseif(get_post_type() == 'product'){
				$terms = get_the_terms(get_the_ID(), 'product_cat', '' , '' );
				if($terms) {
					the_terms(get_the_ID(), 'product_cat', '' , ', ' );
					echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
				}else{
					echo wp_kses_post($before . get_the_title() . $after);
				}
			}else{
				$post_type = get_post_type_object(get_post_type());
				$slug = $post_type->rewrite;
				echo '<a href="' . $homeLink . '/' . $slug['slug'] . '/">' . $post_type->labels->singular_name . '</a>';
				echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
			}
		} else {
			$cat = get_the_category(); $cat = $cat[0];
			$cats = get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo wp_kses_post($cats);
			echo wp_kses_post($before . get_the_title() . $after);
		}
		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object(get_post_type());
			if($post_type) echo wp_kses_post($before . $post_type->labels->singular_name . $after);
		} elseif ( is_attachment() ) {
			$parent = get_post($post->post_parent);
			$cat = get_the_category($parent->ID); $cat = $cat[0];
			echo get_category_parents($cat, TRUE, ' ' . $delimiter . ' ');
			echo '<a href="' . get_permalink($parent) . '">' . $parent->post_title . '</a>';
			echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
		} elseif ( is_page() && !$post->post_parent ) {
			echo wp_kses_post($before . get_the_title() . $after);
		} elseif ( is_page() && $post->post_parent ) {
			$parent_id  = $post->post_parent;
			$breadcrumbs = array();
			while ($parent_id) {
				$page = get_page($parent_id);
				$breadcrumbs[] = '<a href="' . get_permalink($page->ID) . '">' . get_the_title($page->ID) . '</a>';
				$parent_id = $page->post_parent;
			}
			$breadcrumbs = array_reverse($breadcrumbs);
			for ($i = 0; $i < count($breadcrumbs); $i++) {
				echo wp_kses_post($breadcrumbs[$i]);
				if ($i != count($breadcrumbs) - 1)
					echo ' ' . $delimiter . ' ';
			}
			echo ' ' . $delimiter . ' ' . $before . get_the_title() . $after;
		} elseif ( is_tag() ) {
			echo wp_kses_post($before . esc_html__( 'Posts tagged: ', 'cayto' ) . single_tag_title('', false) . $after);
		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata($author);
			echo wp_kses_post( $before . esc_html__( 'Articles posted by ', 'cayto' ) . $userdata->display_name . $after );
		} elseif ( is_404() ) {
			echo wp_kses_post($before . esc_html__( 'Error 404', 'cayto' ) . $after);
		}
		if ( get_query_var('paged') ) {
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ' (';
				echo ' '.$delimiter.' '.__('Page', 'cayto') . ' ' . get_query_var('paged');
			if ( is_category() || is_day() || is_month() || is_year() || is_search() || is_tag() || is_author() ) echo ')';
		}
		return ob_get_clean();
}
}
/* Custom excerpt */
function tb_custom_excerpt($limit, $more) {
$excerpt = explode(' ', get_the_excerpt(), $limit);
if (count($excerpt) >= $limit) {
	array_pop($excerpt);
	$excerpt = implode(" ", $excerpt) . esc_attr( $more );
} else {
	$excerpt = implode(" ", $excerpt);
}
$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
return $excerpt;
}
/* Custom excerpt */
function tb_custom_content($limit,$more) {
$trimContent = get_the_content();
$shortContent = wp_trim_words( $trimContent, $limit, $more);
echo do_shortcode($shortContent); 
}
/* Display navigation to next/previous set of posts */
if ( ! function_exists( 'tb_theme_paging_nav' ) ) {
function tb_theme_paging_nav() {
	if ( $GLOBALS['wp_query']->max_num_pages < 2 ) {
		return;
	}
	$paged        = get_query_var( 'paged' ) ? intval( get_query_var( 'paged' ) ) : 1;
	$pagenum_link = html_entity_decode( get_pagenum_link() );
	$query_args   = array();
	$url_parts    = explode( '?', $pagenum_link );
	if ( isset( $url_parts[1] ) ) {
		wp_parse_str( $url_parts[1], $query_args );
	}
	$pagenum_link = remove_query_arg( array_keys( $query_args ), $pagenum_link );
	$pagenum_link = trailingslashit( $pagenum_link ) . '%_%';
	$format  = $GLOBALS['wp_rewrite']->using_index_permalinks() && ! strpos( $pagenum_link, 'index.php' ) ? 'index.php/' : '';
	$format .= $GLOBALS['wp_rewrite']->using_permalinks() ? user_trailingslashit( 'page/%#%', 'paged' ) : '?paged=%#%';
	// Set up paginated links.
	$links = paginate_links( array(
			'base'     => $pagenum_link,
			'format'   => $format,
			'total'    => $GLOBALS['wp_query']->max_num_pages,
			'current'  => $paged,
			'mid_size' => 1,
			'add_args' => array_map( 'urlencode', $query_args ),
			'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'cayto' ),
			'next_text' => __( '<i class="fa fa-angle-right"></i>', 'cayto' ),
	) );
	if ( $links ) {
	?>
	<div class="col-xs-12">
		<nav class="pagination text-right" role="navigation">
			<?php echo wp_kses_post($links); ?>
		</nav>
	</div>
	<?php
	}
}
}
/* Display navigation to next/previous post */
if ( ! function_exists( 'tb_theme_post_nav' ) ) {
function tb_theme_post_nav() {
	$previous = ( is_attachment() ) ? get_post( get_post()->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );
	if ( ! $next && ! $previous ) {
		return;
	}
	?>
	<nav class="navigation post-navigation clearfix" role="navigation">
		<div class="nav-links">
			<?php
				$previous = get_previous_post_link( '<div class="nav-previous pull-left">%link</div>', wp_kses( __( '<span class="btn text-left btn-default"><i class="fa fa-caret-left"></i>&nbsp; Previous</span>', 'cayto' ),array( 'span' => array( 'class' => array() ), 'i' => array( 'class' => array() ))) );
				$next = get_next_post_link(     '<div class="nav-next pull-right">%link</div>',     wp_kses( __( '<span class="text-right btn btn-default">Next &nbsp;<i class="fa fa-caret-right"></i></span>', 'cayto' ),array( 'span' => array( 'class' => array() ), 'i' => array( 'class' => array() ))) );
				if( $previous ){
					echo $previous;
				}else{
					?>
					<div class="nav-previous pull-left"><a href="#previous" rel="prev"><span class="btn btn-default disabled text-left"><i class="fa fa-caret-left"></i>&nbsp; Previous</span></a></div>
					<?php
				}
				if( $next ){
					echo $next;
				}else{
					?>
					<div class="nav-next pull-right"><a href="#next" rel="next"><span class="btn btn-default disabled text-right">Next &nbsp;<i class="fa fa-caret-right"></i></span></a></div>
					<?php
				}
			?>
		</div>
	</nav>
	<?php
}
}
/* Title Bar */
if ( ! function_exists( 'tb_theme_title_bar' ) ) {
function tb_theme_title_bar($tb_show_page_title, $tb_show_page_breadcrumb) {
	global $tb_options;
	$class = array();
	$class[] = 'title-bar';
	if($tb_show_page_title || $tb_show_page_breadcrumb){ 
		tb_get_content_title_bar( $class );
	}
}
}
/*Custom comment list*/
function tb_custom_comment($comment, $args, $depth) {
$GLOBALS['comment'] = $comment;
extract($args, EXTR_SKIP);
if ( 'div' == $args['style'] ) {
	$tag = 'div';
	$add_below = 'comment';
} else {
	$tag = 'li';
	$add_below = 'div-comment';
}
?>
<<?php echo wp_kses_post($tag) ?> <?php comment_class( empty( $args['has_children'] ) ? '' : 'parent' ) ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
		<div class="comment-avatar">
			<?php if ( $args['avatar_size'] != 0 ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
		</div>
		<div class="comment-info">
			<div class="comment-header-info">
				<div class="comment-author vcard">
					<?php printf( esc_html__( '%s','cayto' ), get_comment_author_link() ); ?>
				</div>
				<div class="comment-date">
					<a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ); ?>">
						<?php
						/* translators: 1: date, 2: time */
						printf( esc_html__('%1$s %2$s','cayto'), get_comment_date('M, d, Y'),  get_comment_time('g:i A') ); ?>
					</a>
				</div>
				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div>
			</div>
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'cayto' ); ?></em>
				<br />
			<?php endif; ?>
			<?php comment_text(); ?>
		</div>
<?php if ( 'div' != $args['style'] ) : ?>
	</div>
<?php endif; ?>
<?php
}
function tb_addURLParameter($url, $paramName, $paramValue) {
 $url_data = parse_url($url);
 if(!isset($url_data["query"]))
	 $url_data["query"]="";
 $params = array();
 parse_str($url_data['query'], $params);
 $params[$paramName] = $paramValue;
 if( $paramName == 'product_count' ) {
	$params['paged'] = '1';
 }
 $url_data['query'] = http_build_query($params);
 return tb_build_url($url_data);
}
function tb_build_url($url_data) {
 $url="";
 if(isset($url_data['host']))
 {
	 $url .= $url_data['scheme'] . '://';
	 if (isset($url_data['user'])) {
		 $url .= $url_data['user'];
			 if (isset($url_data['pass'])) {
				 $url .= ':' . $url_data['pass'];
			 }
		 $url .= '@';
	 }
	 $url .= $url_data['host'];
	 if (isset($url_data['port'])) {
		 $url .= ':' . $url_data['port'];
	 }
 }
 if (isset($url_data['path'])) {
	$url .= $url_data['path'];
 }
 if (isset($url_data['query'])) {
	 $url .= '?' . $url_data['query'];
 }
 if (isset($url_data['fragment'])) {
	 $url .= '#' . $url_data['fragment'];
 }
 return $url;
}
// Hooks add play btn for editor
add_action('admin_init', 'tb_add_button_play');
function tb_add_button_play() {
add_filter('mce_external_plugins', 'tb_add_plugin_play');
add_filter('mce_buttons', 'tb_register_button_play');
}
function tb_register_button_play($buttons) {
   array_push($buttons, "btnplay");
   return $buttons;
}
function tb_add_plugin_play($plugin_array) {
$plugin_array['btnplay'] = TB_URI_PATH .'/assets/js/mce.btn.play.js';
return $plugin_array;
}
/**
* Count product view.
*
* @since 1.0.0
*/
function tb_set_count_view(){
global $post;
if(is_single() && !empty($post->ID) && !isset($_COOKIE['_tb_count_view_'. $post->ID])){
	$views = get_post_meta($post->ID , '_tb_count_view', true);
	$views = $views ? $views : 0 ;
	$views++;
	update_post_meta($post->ID, '_tb_count_view' , $views);
	/* set cookie. */
	setcookie('_tb_count_view_'. $post->ID, $post->ID, time() * 20, '/');
}
}
add_action( 'wp', 'tb_set_count_view' );
function tb_get_count_view() {
global $post;
$views = get_post_meta($post->ID , '_tb_count_view', true);
$views = $views ? $views : 0 ;
return $views;
}
/* Woocommerce - Add meta tag -  data tabs */
add_filter( 'woocommerce_product_tabs', 'tb_add_meta_tag_to_data_tabs' );
function tb_add_meta_tag_to_data_tabs( $tabs ) {
global$tb_options;
// Adds the new tab
$tabs['tag'] = array(
	'title' 	=> esc_html__( 'Tags', 'cayto' ),
	'priority' 	=> 50,
	'callback' 	=> 'tb_add_meta_tag_to_data_tabs_content'
);
if( isset( $tb_options['tb_video_tab'] ) && $tb_options['tb_video_tab'] =='on_tabs' ){
	$tabs['video'] = array(
		'title' 	=> esc_html__( 'Video', 'cayto' ),
		'priority' 	=> 51,
		'callback' 	=> 'tb_add_meta_tag_to_data_video_content'
	);
}
return $tabs;
}
function tb_add_meta_tag_to_data_tabs_content() {
global $post, $product;
$tag_count = sizeof( get_the_terms( $post->ID, 'product_tag' ) );
echo wp_kses_post(wc_get_product_tag_list ($product->get_id() ,', ', '<span class="tagged_as">' . _n( 'Tag:', 'Tags:', $tag_count, 'cayto' ) . ' ', '</span>' ));
}
function tb_add_meta_tag_to_data_video_content() {
global $post;
if(isset($post->ID)){
	$video = get_post_meta($post->ID, 'tb_product_video_youtube', true);
	$video = str_replace( 'https','http', $video );
	echo wp_oembed_get( $video, array('width'=>560,'height'=>315));
}
}
// update porfolio
add_action( 'wp_ajax_tb_update_porfolio', 'tb_update_porfolio' );
add_action( 'wp_ajax_nopriv_tb_update_porfolio', 'tb_update_porfolio' );
function tb_update_porfolio(){
global $wpdb, $pagenow;
$data = (array) $_POST['data'];
if( empty( $data ) ){
	global $tb_options;
	$args = array(
		'posts_per_page' => $tb_options['tb_archive_portfolio_count'],
		'post_type' => 'portfolio',
	);
}else{
	$args = isset( $data['args'] ) ? (array) $data['args'] : array();
	$options = isset( $data['options'] ) ? (array) $data['options'] : array();
	if( empty( $args ) || empty( $options ) ){
		return;
		wp_die();
	}
	extract( $options );
}
$p = new WP_Query($args);
if( $p->have_posts() ){
	switch ($columns) {
		case 1:
			$class_columns[] = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
			break;
		case 2:
			$class_columns[] = 'col-xs-12 col-sm-6 col-md-6 col-lg-6';
			break;
		case 3:
			$class_columns[] = 'col-xs-12 col-sm-6 col-md-4 col-lg-4';
			break;
		case 4:
			$class_columns[] = 'col-xs-12 col-sm-6 col-md-3 col-lg-3';
			break;
		default:
			$class_columns[] = 'col-xs-12 col-sm-6 col-md-3 col-lg-3';
			break;
	}
	ob_start();
	while( $p->have_posts() ){ $p->the_post();
		if( $tpl == 'tpl2' ){
			include( locate_template( 'framework/templates/portfolio/portfolio-lightbox.php' ) );
		}else{
			include( locate_template( 'framework/templates/portfolio/porfolio.php' ) );
		}
	}
	$response = array('pagination'=>'');
	$response['content'] = ob_get_clean();
	if( $show_pagination ){
		$big = 999999999; // need an unlikely integer
		$response['pagination'] = paginate_links( array(
			'base' => @add_query_arg( 'paged', '%#%', esc_url( $_POST['link'] ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, intval( $args['paged'] ) ),
			'total' => $p->max_num_pages,
			'prev_text' => __( '<i class="fa fa-angle-left"></i>', 'cayto' ),
			'next_text' => __( '<i class="fa fa-angle-right"></i>', 'cayto' ),
		) );
	}
	$response['paged'] = $args['paged'] < $p->max_num_pages ? ( $args['paged'] + 1 ) : 0;
	echo json_encode( $response );
}
wp_die();
}
function tb_get_page_id( $id, $meta_id=null ){
global $tb_options;
if( is_archive() || is_search() ){
	if( isset( $tb_options[ $id ] ) ){
		return $tb_options[ $id ];
	}else{
		$post_id = get_option('page_on_front');
	}
}else{
	$post_id = get_queried_object_id();
}
$id = is_null( $meta_id ) ? $id : $meta_id;
return get_post_meta( $post_id , $id, true );
}
function tb_get_object_id( $id, $meta_id=null ){
if( function_exists('is_woocommerce') && is_woocommerce() )
	return tb_get_shop_id( $id, $meta_id );
else
	return tb_get_page_id( $id, $meta_id );
}
function tb_portfolio_archive_paged( $query ) {
if ( $query->is_post_type_archive('portfolio') && $query->is_main_query() ) {
	global $tb_options;
	$count = intval( $tb_options['tb_archive_portfolio_count'] );
	$count = $count > 0 ? $count : -1;
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$query->set('posts_per_page', $count);
	$query->set('paged', $paged);
}
}
add_action( 'pre_get_posts', 'tb_portfolio_archive_paged' );