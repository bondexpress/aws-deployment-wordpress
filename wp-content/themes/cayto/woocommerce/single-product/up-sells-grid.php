<?php
/**
 * Single Product Up-Sells
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

$upsells = $product->get_upsell_ids();

if ( sizeof( $upsells ) == 0 ) {
	return;
}

$meta_query = WC()->query->get_meta_query();

$args = array(
	'post_type'           => 'product',
	'ignore_sticky_posts' => 1,
	'no_found_rows'       => 1,
	'posts_per_page'      => $posts_per_page,
	'orderby'             => $orderby,
	'post__in'            => $upsells,
	'post__not_in'        => array( $product->get_id() ),
	'meta_query'          => $meta_query
);

$products = new WP_Query( $args );

$tb_cols = $posts_per_page === 4 ? 'col-md-3 col-sm-6 col-xs-12' : 'col-md-4 col-sm-6 col-xs-12';

if ( $products->have_posts() ) : ?>

	<div class="upsellss products">
		<div class="cayto-title-separator-wrap text-center">
            <h3 class="cayto-title-separator cayto-title text-center"><span><?php _e( 'UpSell Products', 'woocommerce' ) ?></span></h3>
        </div>
		

		<div class="row">

			<?php while ( $products->have_posts() ) : $products->the_post(); ?>
				<div class="<?php echo $tb_cols; ?>">
					<?php wc_get_template('loop/loop-content.php'); ?>
				</div>
			<?php endwhile; // end of the loop. ?>

		</div>

	</div>

<?php endif;

wp_reset_postdata();
