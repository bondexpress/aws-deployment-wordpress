<?php
/**
 * Related Products
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product, $woocommerce_loop;

if ( empty( $product ) || ! $product->exists() ) {
	return;
}

$related = wc_get_related_products($product->get_id() , $posts_per_page );

if ( sizeof( $related ) == 0 ) return;

$args = apply_filters( 'woocommerce_related_products_args', array(
	'post_type'            => 'product',
	'ignore_sticky_posts'  => 1,
	'no_found_rows'        => 1,
	'posts_per_page'       => $posts_per_page,
	'orderby'              => $orderby,
	'post__in'             => $related,
	'post__not_in'         => array( $product->get_id() )
) );

$products = new WP_Query( $args );

$tb_cols = $args['posts_per_page'] === 4 ? 'col-md-3 col-sm-6 col-xs-12' : 'col-md-4 col-sm-6 col-xs-12';

if ( $products->have_posts() ) : ?>

	<div class="products ro-product-relate">
		<div class="ro-title text-center"><h4><?php _e( 'Related Products', 'cayto' ); ?></h4></div>
		
		<div class="tb-product-items">
			<!--
			<div id="carousel-related" class="tb-carousel" data-paginationspeed="700" data-items="4" data-paginationnumbers="false" data-pagination="true" data-scrollperpage="false" data-navigation="true" data-stoponhover="true" data-autoplay="false" data-itemsscaleup="false" data-singleitem="true" >
			-->
			<div class="row">
				<?php while ( $products->have_posts() ) : $products->the_post(); ?>
					<div class="<?php echo $tb_cols; ?>">
						<?php wc_get_template('loop/loop-content.php'); ?>
					</div>
				<?php endwhile; // end of the loop. ?>
			
			</div>
			
		</div>
	</div>

<?php endif;

wp_reset_postdata();
