<?php
/**
 * Show options for ordering
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<form class="woocommerce-ordering" method="get">
	<?php if( ! isset( $_GET['columns'] ) ){?>
		<input type="hidden" disabled name="columns" value="1">
	<?php } ?>
	<div class="tb-woo-short-by hidden-sm hidden-xs">
		<span><?php _e('Short by: ','cayto');?></span>
		<select name="orderby" class="orderby">
			<?php foreach ( $catalog_orderby_options as $id => $name ) : ?>
				<option value="<?php echo esc_attr( $id ); ?>" <?php selected( $orderby, $id ); ?>><?php echo esc_html( $name ); ?></option>
			<?php endforeach; ?>
		</select>
	</div>
	<div class="tb-woo-sort-by  hidden-sm hidden-xs">
		<span><?php _e('Show: ','cayto');?></span>
		<select name="tb_sortby" id="tb_sortby" class="tb_sortby" onchange="this.form.submit()">
			<?php		 
			//Get products on page reload
			if  (isset($_GET['tb_sortby'])) :
				$numberOfProductsPerPage = $_GET['tb_sortby'];
			else:
				$numberOfProductsPerPage = apply_filters('tb_woocommerce_no_page', 9);
			endif;
			$shopCatalog_orderby = apply_filters('tb_woocommerce_sortby_page', array(
				'3' 		=> __('3', 'raymond'),
				'6' 		=> __('6', 'raymond'),
				'9' 		=> __('9', 'raymond'),
				'12' 		=> __('12', 'raymond'),
				'15' 		=> __('15', 'raymond'),
				'18' 		=> __('18', 'raymond'),
				'-1' 		=> __('All', 'raymond')
			));

			foreach ( $shopCatalog_orderby as $sort_id => $sort_name )
				echo '<option value="' . $sort_id . '" ' . selected( $numberOfProductsPerPage, $sort_id, true ) . ' >' . $sort_name . '</option>';
			?>
		</select>
	</div>
	<?php
		// Keep query string vars intact
		foreach ( $_GET as $key => $val ) {
			if ( 'orderby' === $key || 'submit' === $key  || 'tb_sortby' === $key ) {
				continue;
			}
			if ( is_array( $val ) ) {
				foreach( $val as $innerVal ) {
					echo '<input type="hidden" name="' . esc_attr( $key ) . '[]" value="' . esc_attr( $innerVal ) . '" />';
				}
			} else {
				echo '<input type="hidden" name="' . esc_attr( $key ) . '" value="' . esc_attr( $val ) . '" />';
			}
		}
	?>
</form>
