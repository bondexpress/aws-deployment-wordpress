<?php global $product;
	$tb_options = $GLOBALS['tb_options'];
	$show_sale_flash = (int) isset( $tb_options['tb_archive_show_sale_flash_product'] ) ? $tb_options['tb_archive_show_sale_flash_product'] : 1;
	$show_quick_view = (int) isset( $tb_options['tb_archive_show_quick_view_product'] ) ? $tb_options['tb_archive_show_quick_view_product'] : 1;
	$show_rating = (int) isset( $tb_options['tb_archive_show_rating_product'] ) ? $tb_options['tb_archive_show_rating_product'] : 1;
	$show_add_to_cart = (int) isset( $tb_options['tb_archive_show_add_to_cart_product'] ) ? $tb_options['tb_archive_show_add_to_cart_product'] : 1;
	$show_whishlist = (int) isset( $tb_options['tb_archive_show_whishlist_product'] ) ? $tb_options['tb_archive_show_whishlist_product'] : 1;
	$show_compare = (int) isset( $tb_options['tb_archive_show_compare_product'] ) ? $tb_options['tb_archive_show_compare_product'] : 1;
	$show_title = (int) isset( $tb_options['tb_archive_show_title_product'] ) ? $tb_options['tb_archive_show_title_product'] : 1;
	$show_price = (int) isset( $tb_options['tb_archive_show_price_product'] ) ? $tb_options['tb_archive_show_price_product'] : 1;
	$show_color_attr = (int) isset( $tb_options['tb_archive_show_color_attribute'] ) ? $tb_options['tb_archive_show_color_attribute'] : 1;
?>
<div <?php post_class(); ?>>
							
	<div class="tb-product-item-inner">
		
		<div class="tb-image">
			<?php if( $show_sale_flash ) do_action( 'woocommerce_show_product_loop_sale_flash' ); ?>
			<?php
				$postDate = strtotime( get_the_date('j F Y') );
				$todaysDate = time() - (7 * 86400);// publish < current time 1 week
				if( $postDate >= $todaysDate) echo '<span class="new">'. esc_html__('New', 'cayto') .'</span>';
				$att_ids = $product->get_gallery_image_ids();
				$exist_thumb = ! empty( $att_ids );
				if( $exist_thumb ){
					?>
					<a href="<?php the_permalink(); ?>" class="tb-thumb-effect" data-tb-thumb="true">
						<?php do_action( 'woocommerce_template_loop_product_thumbnail' ); ?>
						<span>
							<img src="<?php $src = wp_get_attachment_image_src( array_shift( $att_ids ),'shop_catalog' ); if( $src ) echo esc_url( $src[0] );?>" alt="<?php the_title();?>">
						</span>
						<?php if( $show_color_attr ):
						$color_atts = wc_get_product_terms( $product->get_id(), 'pa_color', array( 'fields' => 'names' ) );
						if( ! empty( $color_atts ) ):
						?>
						<ul class="list-inline text-center tb-color-attribute">
							<?php foreach( $color_atts as $color ):
								$color = esc_attr( strtolower( $color ) );
							?>
								<li class="tb-attribute-<?php echo $color;?>"><?php echo $color;?></li>
							<?php endforeach; ?>
						</ul>
						<?php endif; endif ?>
					</a>
					<?php
				}else{
					?>
					<a class="tb-normal-effect" href="<?php the_permalink(); ?>">
						<?php do_action( 'woocommerce_template_loop_product_thumbnail' ); ?>
					</a>
					<?php
				}
			?>
			<div class="tb-action">
				<?php if( $show_quick_view ): ?>
				<div class="tb-btn-prod tb-btn-quickview">
					<div data-toggle="tooltip" data-placement="right" title="<?php _e('Quick view','cayto');?>">
						<?php tb_add_quick_view_button(); ?>
					</div>
				</div>
				<?php
					endif;
					if( $show_whishlist ):
						if( function_exists('YITH_WCWL') ):
							$wishlist_text = YITH_WCWL()->is_product_in_wishlist( get_the_ID() ) ? __(' Browse Wishlist','cayto') : __('Add To Wishlist', 'cayto');
				 ?>
				<div class="tb-btn-prod tb-btn-wishlist">
					<div data-toggle="tooltip" data-placement="right" title="<?php echo $wishlist_text;?>">
						<?php echo do_shortcode('[yith_wcwl_add_to_wishlist]');?>
					</div>
				</div>
				<?php
						endif;
					endif;
					if( $show_compare ):
				?>
				<div class="tb-btn-prod tb-btn-compare">
					<div data-toggle="tooltip" data-placement="right" title="<?php _e('Add To Compare','cayto');?>">
						<?php tb_add_compare_link();?>
					</div>
				</div>
				<?php
					endif;
					if( $show_add_to_cart ):
				?>

				<div class="tb-btn-prod tb-btn-tocart">
					<div data-toggle="tooltip" data-placement="right" title="<?php echo esc_html( $product->add_to_cart_text()); ?>">
							<?php do_action( 'woocommerce_template_loop_add_to_cart' ); ?>
					</div>
				</div>
				<?php endif; ?>
			</div>
		</div>
		
		<div class="tb-content">
			<?php if( $show_title ): ?>
			<div class="tb-title text-ellipsis"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
			<?php endif; ?>
			<div class="tb-price-rating">
				<?php

					if( $show_rating ) do_action( 'woocommerce_template_loop_rating' );
					if( $show_price ) do_action( 'woocommerce_template_loop_price' );
				?>
			</div>
			
		</div>
	</div>
	
</div>