<?php $tb_options = $GLOBALS['tb_options']; ?>
<div <?php post_class(); ?>>
							
	<div class="tb-product-item-inner">
		
		<div class="tb-item-image tb-image">
			<?php if($tb_options['tb_archive_show_sale_flash_product']) do_action( 'woocommerce_show_product_loop_sale_flash' ); ?>
			<?php
				$postDate = strtotime( get_the_date('j F Y') );
				$todaysDate = time() - (7 * 86400);// publish < current time 1 week
				if ( $postDate >= $todaysDate) echo '<span class="new">'.esc_html__('New', 'cayto').'</span>';
			?>
			<a href="<?php the_permalink(); ?>">
				<?php do_action( 'woocommerce_template_loop_product_thumbnail' ); ?>
			</a>
		</div>
		
		<div class="tb-item-content-info">
			<div class="tb-title text-ellipsis"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
			<?php if($tb_options['tb_archive_show_price_product']):?>
				<div class="tb-product-wrap-price-rating">
					<?php
						do_action( 'woocommerce_template_single_rating' );
						do_action( 'woocommerce_template_loop_price' );
					?>
				</div>
			<?php endif;?>
			<!-- Content -->
			<div class="tb-product-content">

				<?php tb_custom_content(40,'');?>

			</div>
			<?php if($tb_options['tb_archive_show_add_to_cart_product']):?>
				<div class="tb-product-btn">
					<?php
						do_action( 'woocommerce_template_loop_add_to_cart' );

						echo do_shortcode('[yith_wcwl_add_to_wishlist]');

						tb_add_compare_link();

						tb_add_quick_view_button();
					?>

				</div>
			<?php endif;?>

		</div>
	</div>
	
</div>