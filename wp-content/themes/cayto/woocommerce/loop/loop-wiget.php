<?php
	$show_rating = isset( $show_rating ) ? $show_rating : 1;
	$show_add_to_cart = isset( $show_add_to_cart ) ? $show_add_to_cart : 1;
	$show_title = isset( $show_title ) ? $show_title : 1;
	$show_price = isset( $show_price ) ? $show_price : 1;
 ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				
	<div class="tb-product-item">
		
		<div class="tb-image">
			<?php do_action( 'woocommerce_template_loop_product_thumbnail' ); ?>
		</div>
		
		<div class="tb-content">
			<div class="tb-title text-ellipsis">
			<?php if( $show_title ): ?>
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			<?php endif; ?>
			</div>
			<div class="tb-price-rating">
				<?php
					if( $show_rating ) do_action( 'woocommerce_template_loop_rating' );
					if( $show_price ) do_action( 'woocommerce_template_loop_price' ); 
				?>
			</div>
			
		</div>
	</div>
	
</article>