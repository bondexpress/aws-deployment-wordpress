<?php
add_theme_support( 'woocommerce' );

/** Template pages ********************************************************/

if (!function_exists('tb_woocommerce_content')) {
    
    function tb_woocommerce_content() {

        if (is_singular('product')) {
            wc_get_template_part('single', 'product');
        } else {
            wc_get_template_part('archive', 'product');
        }
    }

}
/**
* Change number of related products on product page
* Set your own value for 'posts_per_page'
*/ 
add_filter( 'woocommerce_output_related_products_args', 'tb_related_products_args' );
function tb_related_products_args( $args ) {
    $args['posts_per_page'] = 4; // 4 related products
    $args['columns'] = 4; // arranged in 4 columns
    return $args;
}

function woocommerce_related_products_args( $args ){
	global $tb_options;
	if( (isset( $_GET['layout']) && $_GET['layout'] ==='fullwidth' ) || ($tb_options['tb_single_sidebar_pos_shop'] ==='tb-sidebar-hidden' ) ){
		 $args['posts_per_page'] = $args['columns'] = 4; 
	}
	return $args;
}
/**
* Change number of upsell display products on product page
* Set your own value for 'posts_per_page'
*/ 
function woocommerce_upsell_display( $posts_per_page = 4, $columns = 4, $orderby = 'rand' ) {
	global $tb_options;
	$fullwidth = ( (isset( $_GET['layout']) && $_GET['layout'] ==='fullwidth' ) || ($tb_options['tb_single_sidebar_pos_shop'] ==='tb-sidebar-hidden' ) );
    if (is_active_sidebar('tbtheme-woo-single-sidebar'))
        $columns = 3;
    $template = 'single-product/up-sells.php';
	if( $fullwidth ){
		$columns = $posts_per_page = 4;
		$template = 'single-product/up-sells-grid.php';
	}
	
	woocommerce_get_template( $template, array(
		'posts_per_page' => $posts_per_page,
		'orderby' => $orderby,
		'columns' => $columns
	) );
}

if ( ! function_exists( 'tb_woocommerce_breadcrumb_defaults' ) ) {

	/**
	 * Output the WooCommerce Breadcrumb
	 *
	 * @access public
	 * @return void
	 */
	function tb_woocommerce_breadcrumb_defaults( $args = array() ) {
		global $tb_options;
		$delimiter = isset($tb_options['tb_page_breadcrumb_delimiter']) ? $tb_options['tb_page_breadcrumb_delimiter'] : '/';
		$args['delimiter']   = '<span>'.$delimiter.'</span>';
		return $args;
	}
}
if ( ! function_exists( 'tb_woocommerce_sharing' ) ) {

	function tb_woocommerce_sharing() {
		global $product;
		//$permalink = $product->post->guid;
		//$title = $product->post->post_title;
		
		$content = '<!-- Go to www.addthis.com/dashboard to customize your tools -->
					<div class="addthis_sharing_toolbox addthis_default_style"></div>';

		echo $content;
	}
}

/* hook layout woo */
add_action( 'wp_ajax_ct_hook_woo_columns', 'tb_hook_woo_columns' );
add_action( 'wp_ajax_nopriv_ct_hook_woo_columns', 'tb_hook_woo_columns' );

function tb_hook_woo_columns() {
	global $woocommerce_loop;
	$column = $_POST['column'];
	$woocommerce_loop['columns'] = $column;
	delete_option('loop_shop_columns');
	add_option('loop_shop_columns',$column);
	die($column);
}


function tb_sort_by_page($count) {
	global $tb_options;

 $count = 9;
 if (isset($_GET['tb_sortby'])) {
  $count = $_GET['tb_sortby'];
 }elseif( isset( $tb_options['tb_archive_shop_per_page'] ) ){
 	$count = intval( $tb_options['tb_archive_shop_per_page']);
 }
 // else normal page load and no cookie
 return $count;
}
add_filter('loop_shop_per_page','tb_sort_by_page', 15);

/**
 *  Add the link to compare
 */
function tb_add_compare_link( $product_id = false, $args = array() ) {
	extract( $args );

	if ( ! $product_id ) { 
		global $product;
		$product_id = isset( $product->id ) && $product->exists() ? $product->get_id() : 0;
	}

	// return if product doesn't exist
	if ( empty( $product_id ) ) return;
	
	$action_add ='yith-woocompare-add-product';
	$url_args = array(
					'action' => 'yith-woocompare-add-product',
					'id' => $product_id
				);
	$add_product_url = wp_nonce_url( add_query_arg( $url_args ), $action_add );
	printf( '<div class="woocommerce product compare-button"><a href="%s" class="%s" data-product_id="%d" title="%s">%s</a></div>', $add_product_url, 'compare', $product_id, 'Compare', 'Compare' );
}

/**
 * Add quick view button in wc product loop
 */
function tb_add_quick_view_button() {

	global $product;

	echo '<a href="#" class="button yith-wcqv-button" data-product_id="' . $product->get_id() . '"></a>';
}

// rename for tags tab
function tb_woo_rename_tabs( $tabs ) {

	$tabs['tag']['title'] = __( 'Tags', 'cayto' );

	return $tabs;
}

// change related products args
function tb_woo_related_products_args( $args ) {
	$args['posts_per_page'] = 3;
	$args['columns'] = 3;
	return $args;
}

// change rating output

// function tb_woo_get_rating_html( $rating_html, $rating ){
// 	$rating_html = str_replace( '<div>', '</div>', $rating_html );
// 	return $rating_html;
// }
// Add term page
add_action( 'product_cat_add_form_fields', 'tb_taxonomy_add_new_meta_field', 10, 2 );
function tb_taxonomy_add_new_meta_field() {
  ?>
  <div class="form-field">
  		<label for="term_meta[tb_full_width]"><?php _e( 'Show as full width', 'cayto' ); ?></label>
    	<select name="term_meta[tb_full_width]" id="term_meta[tb_full_width]">
			<option value="0"><?php esc_html_e('No','cayto');?></option>
		   <option value="1"><?php esc_html_e('Yes','cayto');?></option>
   		</select>
  </div>
  <?php
}


// Edit term page
add_action( 'product_cat_edit_form_fields', 'tb_taxonomy_edit_meta_field', 10, 2 );
function tb_taxonomy_edit_meta_field($term) {
 
  // put the term ID into a variable
  $t_id = $term->term_id;
 
  // retrieve the existing value(s) for this meta field. This returns an array
  $term_meta = get_option( "taxonomy_$t_id" );
  $tb_full_wid = $term_meta['tb_full_width'] ? intval( $term_meta['tb_full_width'] ) : 0;
  ?>
  <tr class="form-field">
  <th scope="row" valign="top"><label for="term_meta[tb_full_width]"><?php _e( 'Full width', 'cayto' ); ?></label></th>
    <td>
    	<select name="term_meta[tb_full_width]" id="term_meta[tb_full_width]">
			<option <?php selected( $tb_full_wid, 0 );?> value="0"><?php esc_html_e('No','cayto');?></option>
		   <option <?php selected( $tb_full_wid, 1 );?> value="1"><?php esc_html_e('Yes','cayto');?></option>
   		</select>
    </td>
  </tr>
<?php
}

// Save extra taxonomy fields callback function
add_action( 'edited_product_cat', 'tb_save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_product_cat', 'tb_save_taxonomy_custom_meta', 10, 2 );
function tb_save_taxonomy_custom_meta( $term_id ) {
  if ( isset( $_POST['term_meta'] ) ) {
    $t_id = $term_id;
    $term_meta = get_option( "taxonomy_$t_id" );
    $cat_keys = array_keys( $_POST['term_meta'] );
    foreach ( $cat_keys as $key ) {
      if ( isset ( $_POST['term_meta'][$key] ) ) {
        $term_meta[$key] = wp_kses_post( stripslashes($_POST['term_meta'][$key]) );
      }
    }
    // Save the option array.
    update_option( "taxonomy_$t_id", $term_meta );
  }
}

function tb_get_op_full_wid(){
	global $tb_options;
	if( isset( $_GET['layout'] ) && $_GET['layout']=='fullwidth' ) return true;
	$t_id = get_queried_object_id();
	$term_meta = get_option( "taxonomy_$t_id" );
	if( isset(  $term_meta['tb_full_width'] ) )
		return $term_meta['tb_full_width'];
	elseif( $tb_options['tb_archive_sidebar_pos_shop'] == 'tb-sidebar-hidden' )
		return true;
}

add_filter('woocommerce_product_single_add_to_cart_text', 'tb_variable_single_add_to_cart_text');
function tb_variable_single_add_to_cart_text( $text ){
	global $product;
	if( $product->is_type( 'variable' ) ){
		$text = __('Select options', 'cayto');
	}
	return $text;
}

add_action('wp_head','hook_css');

function hook_css() {
	$colors = get_terms('pa_color', array('hide_empty'=>0));
	if( is_wp_error( $colors ) || empty( $colors ) ) return;
	ob_start();
	?>
	<style>
	<?php foreach( $colors as $color ){
		$c = esc_attr( strtolower( $color->name ) );
		?>
		.tb-attribute-<?php echo $c;?>{
			background-color:<?php echo $c;?>;
		}
		<?php
	}
	?>
	</style>
	<?php

	echo ob_get_clean();
}

function tb_get_shop_id( $id, $meta_id=null ){
	global $tb_options;

	if( is_archive() || is_search() || is_product() ){
		if( isset( $tb_options[ $id ] ) ){
			return $tb_options[ $id ];
		}else{
			$post_id = get_option( 'woocommerce_shop_page_id' );
		}
	}else{
		$post_id = get_queried_object_id();
	}
	$id = is_null( $meta_id ) ? $id : $meta_id;
	return get_post_meta( $post_id , $id, true );
}

function tb_product_featured_video(){
	global $tb_options, $post;
	if( isset( $tb_options['tb_video_tab'] ) && $tb_options['tb_video_tab']=='on_thumbnail' ){
		$video = get_post_meta($post->ID, 'tb_product_video_youtube', true);
		if( $video ){
			echo '<a class="product-video-popup"  data-toggle="tooltip" data-placement="top" title="'. __("View video","cayto") .'" href="'. esc_url($video) .'"><span class="fa fa-play"></span>'. __("Play Video","cayto") .'</a>';
		}
	}
}


function tb_woocommerce_sortby_page( $options ){
	global $tb_options;
	$no_page = (int)$tb_options['tb_archive_shop_per_page'];
	if( isset( $tb_options['tb_archive_shop_per_page']) && $no_page && !isset( $options[ $no_page ])){
		$options[ $no_page ] = $no_page;
	}

	return $options;
}

function tb_woocommerce_no_page( $page ){
	global $tb_options;
	if( isset( $tb_options['tb_archive_shop_per_page']) && $no_page = (int)$tb_options['tb_archive_shop_per_page'] ){
		$page = $no_page;
	}
	return $page;
}