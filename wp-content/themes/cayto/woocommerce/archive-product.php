<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.3.0
 */
if (!defined('ABSPATH')) exit; // Exit if accessed directly
global $woocommerce_loop;
$tb_options = $GLOBALS['tb_options'];

// Store column count for displaying the grid
if( isset( $_GET['columns'] ) ){
	$woocommerce_loop['columns'] = intval( $_GET['columns'] );
}elseif ( empty( $woocommerce_loop['columns'] ) ) {
	$columns = isset( $tb_options['tb_archive_shop_column'] ) ?  $tb_options['tb_archive_shop_column'] : 4;
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', $columns );
}

get_header();
?>
<?php
	// get option fullwidth
	$is_full_wid = tb_get_op_full_wid();

	$cl_content = 'col-xs-12 col-sm-12 col-md-12 col-lg-12';
	$cl_sidebar = '';

	if ( ! $is_full_wid && is_active_sidebar('tbtheme-woo-sidebar') ) {
		
		$cl_content = 'col-xs-12 col-sm-8 col-md-8 col-lg-9 tb-content';
		$cl_sidebar = 'col-xs-12 col-sm-4 col-md-4 col-lg-3 sidebar-area';
	}

	$is_one_col = $woocommerce_loop['columns'] == 1;

	if( isset( $_GET['sidebar'] ) ){
		$tb_sidebar_pos =  trim( $_GET['sidebar'] );
		$tb_sidebar_pos = $tb_sidebar_pos =='tb-sidebar-left' ? $tb_sidebar_pos : 'tb-sidebar-right'; 
	}else{
		$tb_sidebar_pos = ! empty($tb_options['tb_archive_sidebar_pos_shop'])?$tb_options['tb_archive_sidebar_pos_shop']:'tb-sidebar-right';
	}
	


?>
<div class="archive-products<?php if( $is_full_wid ) echo ' grid-full-width';?>">
	<div class="container">
		<div class="row <?php echo esc_attr($tb_sidebar_pos); ?>">

			<?php if ( ! $is_full_wid && $tb_sidebar_pos == 'tb-sidebar-left') { ?>
				<div class="<?php echo esc_attr($cl_sidebar); ?>">
					<div id="secondary" class="widget-area" role="complementary">
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php
								if(is_active_sidebar('tbtheme-woo-shop-sidebar')){
									dynamic_sidebar( 'tbtheme-woo-shop-sidebar' ); 
								}
							?>
						</div>
					</div>
				</div>
			<?php } ?>
			
			<div class="<?php echo esc_attr($cl_content); ?>">
				
				<?php do_action('woocommerce_archive_description'); ?>

				<?php if (have_posts()) : ?>
					
					<div class="tb-start-shop-loop">
						<!-- View as -->
						<div class="tb-view-as">
							<p class="woocommerce-result-count">
								<span><?php _e('View as:', 'cayto')?></span>
								<a href="javascript:void(0);" title="Layout Grid" data-column="3" class="tb_action_grid tb_action_layout tb_action <?php if( ! $is_one_col ){ echo 'active'; }?>"><i class="fa fa-th"></i></a>
								<a href="javascript:void(0);" title="Layout List" data-column="1" class="tb_action_list tb_action_layout tb_action <?php if( $is_one_col ){ echo 'active'; }?>"><i class="fa fa-list"></i></a>
							</p>
						</div>
						
						<!-- Catalog Ordering-->
						<div class="tb-shop-catalog-ordering ">
							<?php if($tb_options['tb_archive_show_catalog_ordering']) woocommerce_catalog_ordering(); ?>
						</div>



						<!-- pagination -->
						<?php if( ! $is_full_wid && $tb_options['tb_archive_show_pagination_shop'] ): ?>
							<!-- Shop Pagination -->
							<div class="tb-shop-pagination pull-right">
								<?php 
								/**
								 * woocommerce_after_shop_loop hook
								 *
								 * @hooked woocommerce_pagination - 10 
								 */
								 do_action('woocommerce_after_shop_loop');
								?>
							</div>
						<?php else: ?>
							<div class="pull-right tb-shop-attribute">
								<?php if( is_active_sidebar('tbtheme-woo-archive-attr-sidebar') ) dynamic_sidebar( 'tbtheme-woo-archive-attr-sidebar' ); ?>
							</div>
						<?php endif; ?>

					</div>
					
					<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>

					<?php while (have_posts()) : the_post(); ?>

						<?php wc_get_template_part('content', 'product'); ?>

					<?php endwhile; ?>

					<?php woocommerce_product_loop_end(); ?>
					
					<div class="tb-after-shop-loop">
					
						<!-- View as -->
						<div class="tb-view-as">
							<p class="woocommerce-result-count">
								<span><?php _e('View as:', 'cayto')?></span>
								<a href="#" title="Layout Grid" data-column="3" class="tb_action_grid tb_action <?php if( ! $is_one_col ){ echo 'active'; }?>"><i class="fa fa-th"></i></a>
								<a href="#" title="Layout List" data-column="1" class="tb_action_list tb_action <?php if( $is_one_col ){ echo 'active'; }?>"><i class="fa fa-list"></i></a>
							</p>
						</div>
						
						<!-- Pagination -->
						<div class="tb-shop-pagination pull-right">
							<?php
							/**
							 * woocommerce_after_shop_loop hook
							 *
							 * @hooked woocommerce_pagination - 10
							 */
							if($tb_options['tb_archive_show_pagination_shop']) do_action('woocommerce_after_shop_loop');
							?>
						</div>
					</div>

				<?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>

					<?php wc_get_template('loop/no-products-found.php'); ?>

				<?php endif; ?>

			</div>
			<?php if ( ! $is_full_wid && $tb_sidebar_pos == 'tb-sidebar-right') { ?>
				<div class="<?php echo esc_attr($cl_sidebar); ?>">
					<div id="secondary" class="widget-area" role="complementary">
						<div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
							<?php
								if(is_active_sidebar('tbtheme-woo-shop-sidebar')){
									dynamic_sidebar( 'tbtheme-woo-shop-sidebar' ); 
								}
							?>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<?php get_footer('shop'); ?>
