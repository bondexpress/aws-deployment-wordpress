<?php
	/*
		Template Name: 404 Template
	*/
	get_header();
?>
<div class="main-content">
	<div class="tb-error404-wrap text-center" >
		<?php
			?>
			<h2><?php esc_html_e('Page Not Found','perfomy');?></h2>
			<div class="page-wrapper container">
				<div class="page-content">
					<h3><?php echo esc_html_e('This is somewhat embarrassing, isn�t it?','perfomy');?></h3>
					<p><?php echo esc_html_e('It looks like nothing was found at this location. Maybe try a search?','perfomy');?></p>
					<?php get_search_form(); ?>
				</div><!-- .page-content -->
			</div>
			<?php
			
		?>
	</div>
</div>
<?php //get_footer(); ?>