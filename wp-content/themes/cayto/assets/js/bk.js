revapi9.bind("revolution.slide.onloaded",function (e) {
	var _tb_header_v4 = jQuery('.tb-header-v4'),
			_home_slide_v4 = jQuery('#home-slider-v4');
		if( _tb_header_v4.length && _home_slide_v4.length ){
			function tb_change_hei_slider(){
				var real_hei = _home_slide_v4.innerHeight();
				if( _home_slide_v4.innerHeight() < jQuery( window ).height() ){
					real_hei = '100vh';
					_tb_header_v4.find('.mobile-leftbar').css('position', 'fixed' );
				}
				_tb_header_v4.find('.mobile-leftbar').css('height', real_hei );
			}
			tb_change_hei_slider();
			jQuery(window).on('resize', function(){
				tb_change_hei_slider();
			});

		}
		
});